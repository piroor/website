---
layout: default
main_title: YAPC::Kyoto 2023に『Perlと全文検索エンジンGroongaでMySQLのデータを高速に全文検索する』と題し堀本泰弘が登壇
sub_title:
type: press
keywords: perl, event, Groonga, 全文検索, full text search
---

<div class="press-release-signature">
  <p class="date">2023年3月16日</p>
  <p>株式会社クリアコード
</div>

株式会社クリアコード（本社：埼玉県所沢市、代表取締役：須藤功平）は、2022年3月19日(日)に京都リサーチパークにて開催される「YAPC::Kyoto 2023」に『Perlと全文検索エンジンGroongaでMySQLのデータを高速に全文検索する』というタイトルで堀本泰弘が登壇することをお知らせします。

今回の発表では、国産全文検索エンジンGroongaを使ってMySQLのデータをPerlアプリケーションから高速に全文検索する方法とその仕組みを紹介します。MySQLのデータを全文検索する既存の方法に課題を感じている方はぜひ、本講演をお聞きください。

資料はおってクリアコードの[ブログ](https://www.clear-code.com/blog/)で公開予定です。


### 講演の概要

* 日時：3月19日（日）15時～15時40分
* 場所：G会議室
* 講演タイトル：Perlと全文検索エンジンGroongaでMySQLのデータを高速に全文検索する
* 講演者プロフィール：堀本　泰弘（Groonga/Mroonga/PGroongaメンテナー)

2017年から全文検索エンジンGroonga及び、MySQLやMariaDBからGroongaを使えるMroongaや、PostgreSQLからGroongaを使えるPGroongaなど関連プロジェクトのメンテナンス・開発に従事。これまで、様々な場所で全文検索エンジンGroongaの機能や運用方法について紹介している。

### YAPC::Kyoto 2023の概要

<blockquote cite="https://yapcjapan.org/2023kyoto/">

YAPCはYet Another Perl Conferenceの略で、Perlを軸としたITに関わる全ての人のためのカンファレンスです。 Perlだけにとどまらない技術者たちが、好きな技術の話をし交流するカンファレンスで、技術者であれば誰でも楽しめるお祭りです！　

今回のYAPC::Japanのテーマは「try/catch」。

</blockquote>

（[公式サイト](https://yapcjapan.org/2023kyoto/)より抜粋）

* 日時： 2023年3月19日（日）9時30分～19時30分
* 会場： 京都リサーチパーク＆オンライン
* 主催：Japan Perl Association
* 公式サイト：https://yapcjapan.org/2023kyoto/

![logo]({% link press-releases/20230316-yapc-kyoto-2023/yapc-logo.png %})

### Groongaについて

[Groonga](https://groonga.org/ja/)は日本で開発された、オープンソースのカラムストア機能付き全文検索エンジンです。Groongaを使うと高性能な全文検索機能をアプリケーションに組み込むことができます。[2023年2月9日に最新のGroonga 13.0.0がリリースされました。]({% link press-releases/20230209-groonga-13-0-0.md %})

よく使われているRDBMSからGroongaを使えるようにするソフトウェアや、GroongaのRubyバインディングも開発されています。2010年に1.0.0をリリースしてから、着実にユーザを増やし、レストラン検索サイト、医療機関検索サイト、不動産物件情報サイト等で採用されています。

### クリアコードについて

クリアコードは、2006年7月にフリーソフトウェア開発者を中心に設立したソフトウェア開発会社です。

Fluentd/Fluent Bit/Apache Arrow/Groonga/組み込みシステム向け日本語入力ソフトウェアなど、多岐にわたるソフトウェアの開発やソースコードレベルでの技術支援を行っています。

10年以上にわたりGroongaプロジェクトのメンバーとして、Groonga本体および関連プロジェクトの開発、リリース、コミュニティーでのサポートに関わっています。Groonga/PGroonga/Mroongaの機能拡張、サポート、導入支援を提供します。新規導入のほか、既存システムの改善などのご相談も対応可能です。

### 当リリースに関するお問い合わせ先

株式会社クリアコード

メール：info@clear-code.com

#### 参考リンク

* 【コーポレートサイト】[https://www.clear-code.com/]({% link index.html %})
* 【関連サービス】[https://www.clear-code.com/services/groonga.html]({% link services/groonga.md %})
* 【プロジェクトページ】[Groonga.org](https://groonga.org/ja/)
