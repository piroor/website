---
layout: default
main_title: オフィス文書内文字列検索・置換ツール「OfficeStringReplacer」の提供を開始
sub_title: Excel,Word,PowerPoint,Access,ショートカットをサポート。フォルダ内のすべてのファイルを対象とした文字列の一括置換を実現。
type: press
keywords: Excel,Word,PowerPoint,Access, 置換, ClearCode, ファイル内検索, 検索, 文字列一括置換
---

<p class='press-release-subtitle-small'>
Excel,Word,PowerPoint,Access,ショートカットをサポート。フォルダ内のすべてのファイルを対象とした文字列の一括置換を実現。
</p>
<div class="press-release-signature">
  <p class="date">2024年2月21日</p>
  <p>株式会社クリアコード
</div>

株式会社クリアコード（本社：埼玉県所沢市、代表取締役：須藤 功平）は、オフィス文書内の文字列を検索・置換するWindowsアプリケーションであるOfficeStringReplacerの提供を開始しました。

Web：[https://www.clear-code.com/services/officestringreplacer.html]({% link services/officestringreplacer.md %})

![]({% link press-releases/20240221-officestringreplacer/OSRimage.png %})

## OfficeStringReplacerについて

OfficeStringReplacer は、ファイル内の文字列を検索・置換するツールです。
Microsoft Office 製品で使用する各種ファイルに含まれる特定文字列を検索、置換します。
また指定したディレクトリ配下のすべてのファイルを検索・置換対象とするため、効率的にファイル内の文字列置換を行うことができます。例えば、1000ファイルを対象とした場合、およそ1時間で、検索・置換処理を行います。

### 想定する利用シーン

* ファイルサーバーの移行によりExcelファイルのファイルパスが変更になった。Excel内にある他のファイルへのリンクを特定ルールに基づいて書き換えたい。
* 会社名や部署名の変更に伴い、旧会社名や旧部署名が記載されている文書を探したい。
* 「ＷＥＢサイト」と「WEBサイト」など表記ゆれがあるとき、適切な表記に統一したい。

### サポートするファイル形式

| ファイル種類 | 拡張子 |
| :---: | :---: |
| Excel | `.xlsx`, `.xlsm`, `.xls` |
| Word | `.docx`, `.docm`, `.dox` |
| PowerPoint | `.pptx`, `.pptm`, `.ppt` |
| Access | `.accdb`, `.mdb` |
| ショートカット | `.lnk` |

## OfficeStringReplacer法人利用パック

クリアコードでは、OfficeStringReplacerを導入する企業様に対する支援として、OfficeStringReplacer法人利用パックを提供します。
OfficeStringReplacer法人利用パックは、OfficeStringReplacerを利用する法人ユーザーを対象に、OfficeStringReplacerのプログラム一式とサポートを提供するものです。
提供するサービスは以下の通りです。

* OfficeStringReplacerのプログラム本体
* OfficeStringReplacer利用マニュアル
* OfficeStringReplacerの問い合わせサポート 

1年毎更新のサブスクリプション契約となります。価格は￥1,200,000から。

## クリアコードについて

クリアコードは、2006年7月にフリーソフトウェア開発者を中心に設立したソフトウェア開発会社です。 創業以来Mozilla Firefox、Thunderbirdの企業導入ならびにサポートを提供しています。さらに企業におけるデスクトップ環境の課題解決を目的として、誤送信防止対策の拡張機能であるFlexConfirmMail（Thunderbird、Outlookをサポート）や、URL毎に起動するブラウザを切り替えるBrowserSelectorといった製品を開発しています。

また、Fluentd/ Apache Arrow/ Groonga/日本語入力など組み込みシステム向けソフトウェアなど多岐にわたるソフトウェアの開発や技術サポートサービスを提供しています。

## 当リリースに関するお問合せ先

株式会社クリアコード
メール：info@clear-code.com

※ Microsoft Office, Access, Excel,Power Point, Wordはマイクロソフトグループの商標です。
