---
layout: default
main_title: PostgreSQL Conference Japan 2023にて、PostgreSQLをApache Arrow Flight SQLに対応させる新プロダクトについて代表取締役 須藤功平が講演
sub_title: 2023年11月24日（金）14:10～
type: press
keywords: 
---
<div class="press-release-signature">
  <p class="date">2023年11月21日</p>
  <p>株式会社クリアコード</p>
</div>
株式会社クリアコード（本社：埼玉県所沢市、代表取締役：須藤 功平）は、2023年11月24日（金）に、特定非営利活動法人 日本PostgreSQLユーザ会 （JPUG）主催のPostgreSQL Conference Japan 2023 （日本語略称： PostgreSQLカンファレンス2023）において、「Apache Arrow Flight SQLでPostgreSQLをもっと速く！」と題し講演します。

最新のApache Arrow Flight SQL adapter for PostgreSQL[^1]の開発を主導したApache Arrowプロジェクト[^2]でコミット数1位のコミッターが、日本のPostgreSQLユーザーに向けて、2023年9月にリリースされたばかりの新プロダクトについて具体的に紹介します。

[^1]:Apache Arrow Flight SQLは、メモリー上でのデータ処理に関わる共通基盤Apache Arrowプロジェクトが開発している新しいネットワークプロトコルです。PostgreSQLとやりとりするデータが多いケースの高速化にも使えます。[詳しい情報はブログで公開しています。]({% post_url 2023-09-14-apache-arrow-flight-sql-postgresql-0.1.0 %})
[^2]:Apache Arrow, the Apache Arrow project logo are either registered trademarks or trademarks of The Apache Software Foundation in the United States and other countries.（Apache Arrowの商標およびロゴはApache Software Foundationの商標です。） Apache Arrowはこれからさらに必要とされている大規模データの処理をより高速にスムーズに行うため、データの処理・交換・変換等を行うモジュールで使えるデータ処理基盤を開発するプロジェクトです。特定のプログラミング言語に特化しておらず、多くのプログラミング言語で使えるようになっているため、複数の言語で実装されたデータ処理システムでも活用できます。

### 講演の概要

PostgreSQLとの接続には独自プロトコルが使われていますが、やりとりするデータが大きくなるとクエリーの処理ではなくこのプロトコルがボトルネックになることが知られています。Apache Arrow Flight SQLプロトコルはこのボトルネックを解消できるプロトコルです。Apache Arrow Flight SQLの詳細、どのくらい速くなるのか、プロトコルを拡張する仕組みのないPostgreSQLでどのように実装したのかといった実装の詳細を紹介します。

* 日時：2023年11月24日（金）14:10-15:00
* 講演タイトル：Apache Arrow Flight SQLでPostgreSQLをもっと速く！
* 講演者プロフィール：
*須藤功平（Apache Arrowプロジェクト管理委員会メンバー）*

2016年よりApache Arrowの開発に参加し、2017年5月にコミッターに就任、2017年9月にプロジェクト管理委員会メンバーに就任。2022年1月から1年間、プロジェクト管理委員会のchairを務める。日本でのApache Arrow普及を目的として、開発のみならず各所でのApache Arrowの紹介を精力的に行っている。またApache Arrowの開発状況を定期的にまとめ、日本語の記事として公開している。

講演資料はブログで公開予定です。 https://www.clear-code.com/blog/


### イベントに関する情報

PostgreSQL Conference Japan 2023は日本PostgreSQLユーザ会（JPUG）が毎年開催している国内最大のPostgreSQLをテーマとしたカンファレンスイベントです。
オンサイトにて実施される1Dayイベントで、日本のユーザ向けに PostgreSQLの導入事例や技術情報を伝える場です。

* 開催期間：2023年11月24日（木）10:00 - 18:10（開場 9:30、18:30から懇親会あり）
* 会場：AP日本橋（東京都） 6F
* 参加料：
  * チュートリアル付きチケット：4000円
  * チュートリアルなしチケット：2500円
  * 懇親会チケット：4000円
  チケット購入サイト：https://eventregist.com/e/pgconjp2023
* 公式ページ：https://www.postgresql.jp/jpug-pgcon2023


## クリアコードについて

クリアコードは2006年にフリーソフトウェア開発者を中心に設立しました。

データ処理基盤Apache Arrow、データ収集ソフトウェアFluentd[^3]、検索エンジンGroonga[^4]といった様々なソフトウェアを通して、データ利用に関する様々な顧客依頼に対し、根本的な課題解決を実現させる受託開発や、ソースコードレベルのサポートを提供してきました。

また、フリーソフトウェアとビジネスの両立を目指し、継続的なメンテナンス・新規機能開発・開発者を増やす取り組みなどにも取り組んでいます。


### 参考URL

【コーポレートサイト】{{ site.url }}{% link index.html %}

【関連サービス】{{ site.url }}{% link services/index.md %}

## 当リリースに関するお問い合わせ先

株式会社クリアコード

TEL：04-2907-4726

メール：info@clear-code.com

[^3]:Groongaはオープンソースのカラムストア機能付き全文検索エンジンです。Groongaを使うことにより、高性能な全文検索機能を備えたアプリケーションを容易に開発することができます。Groonga公式ページ：https://groonga.org/ja/ 
[^4]:Fluentdは、米国及びその他の国におけるThe Linux Foundationの商標または登録商標です。Fluentdは、拡張性の高いログ収集オープンソースソフトウェアで1000以上のプラグインで様々なサービスとのデータ連携を実現します。Cloud Native Computing Foundation (CNCF)により認定されたプロジェクトの一つです。Fluentd公式ページ（英語）：https://www.fluentd.org/
