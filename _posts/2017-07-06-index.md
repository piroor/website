---
tags: 
- embedded
title: Gecko Embeddedプロジェクト
---
### はじめに

クリアコードは、組み込みLinux向けにMozilla Firefoxのブラウザエンジンを移植するプロジェクト[Gecko Embedded](https://github.com/webdino/gecko-embedded/wiki)を[WebDINO Japan（旧Mozilla Japan）](https://www.webdino.org/)様と共同で立ち上げ、開発を進めております。[Yocto](https://www.yoctoproject.org/)を使用してFirefoxをビルドしたりハードウェアアクセラレーションを有効化する際のノウハウを蓄積して公開することで、同じ問題に悩む開発者の助けになることを目指しています。
<!--more-->


### ターゲット

組み込みLinuxと一口に言っても、対象となるハードウェア及びソフトウェア環境は多岐にわたります。
しかし投入できる開発リソースには限りがあるため、当面のターゲットは以下の環境に絞っています。

  * SoC: [Renesas RZ/G](http://elinux.org/RZ-G)シリーズ

  * ビルドシステム/OS: [Yocto](https://www.yoctoproject.org/) [2.0 (Jethro)](https://www.yoctoproject.org/downloads/core/jethro20)

  * ウィンドウシステム: [Wayland](https://wayland.freedesktop.org/) 1.10以降

  * ツールキット: GTK+ 3.20以降

GTK+を使用せずに直接WaylandあるいはDRMで描画するバックエンドを作成した方がより組み込み向けらしいとは言えます。しかし過去の事例から考えても、[Mozillaが正式にサポートするプラットフォーム](https://developer.mozilla.org/ja/docs/Supported_build_configurations)から大きくかけ離れたバックエンドを作成すると、その後のバージョンアップに追従するコストが高くなり、やがてはメンテナンスされなくなっていく未来が見えてしまいます。このため本プロジェクトでは、少なくとも現段階ではGTK+を使用し、成果を本体へフィードバックしていくことで、バージョンアップに追従していくコストを抑えることを目指しています。

### 現在のステータス

#### Wayland対応

Firefoxは正式にはWaylandをサポートしていませんが、Red Hat社のMartin Stransky氏が[Waylandへの移植作業](https://bugzilla.redhat.com/show_bug.cgi?id=1054334)を行っています。
Stransky氏のパッチはFirefoxの最新バージョンを対象としていますが、Gecko Embeddedプロジェクトではこのパッチを52ESRに移植した上で、安定化作業を進めています。
本プロジェクトで発見した問題や、作成したパッチはStransky氏に随時フィードバックしています。Stransky氏のWayland対応パッチは、徐々に[mozilla-central](https://hg.mozilla.org/mozilla-central)に取り込まれていっています。

#### EGL対応

暫定パッチを当てることにより、[EGL](https://www.khronos.org/egl)/[OpenGL ES](https://www.khronos.org/opengles/)を使用して以下のアクセラレーションを有効化できることを確認済みです。

  * Compositor

  * Canvas

  * WebGL1

ただし上記パッチは必ずしもWaylandやGTK+の描画機構に即した形にはなっておらず、いくつかの問題を抱えているため、本体へフィードバックする際には抜本的な見直しが必要です。また、CanvasやWebGLについては安定性の面でさらなる対応が必要です。

#### OpenMAX IL対応

H.264の動画再生については、[OpenMAX IL](https://www.khronos.org/openmax/il/)対応コードを追加することで動作を確認済みです。現在はまだ画像データのゼロコピーを実現できていませんが、これを実装することにより今後の性能改善が期待できます。

なお、一般的に、組み込みLinuxでの動画再生は[GStreamer](https://gstreamer.freedesktop.org/)によって実装されますが、現在のFirefoxでは諸般の理由から[GStreamer対応コードが削除されています](https://bugzilla.mozilla.org/show_bug.cgi?id=gstreamer)。このため本体にコードを取り込んでもらえる可能性は低いですし、独自にGStreamer対応コードを保守するのもコストが嵩みます。一方でOpenMAX ILについてはFirefox OS用の類似のコードが残されており、保守が容易で本体へフィードバックできる可能性も高いため、現時点ではOpenMAX ILを使用する方法を選択しています。

ただし、将来的に他のボードへの対応を行う際には、OpenMAX ILコンポーネントを提供されていないボードもありますので、いずれにしてもGStreamerやその他の方法での対応が必要となるかもしれません。

### ビルド方法

前述のリファレンス環境におけるビルド方法については、以下のページにまとめてあります。

  * https://github.com/mozilla-japan/meta-browser/wiki/Build-RZ-G1E-Yocto2.0

CanvasやWebGLの有効化については、それぞれ以下の設定を追加する必要があります。

~/.mozilla/firefox/xxxxxxxx.default/user.js:

```
user_pref("gfx.canvas.azure.accelerated", true);
user_pref("webgl.force-enabled", true);
```


他のボードでも類似の方法でビルドできるかもしれませんが、相応の対応が必要と思われます。

### デモ動画

[ミラクル・リナックス社](https://www.miraclelinux.com/)がYouTubeでデモ動画を公開して下さっています。

<div class="youtube-4x3">
  <iframe width="425"
          height="350"
          src="https://www.youtube.com/embed/6zOVIZnbwbk"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen></iframe>
</div>


### まとめ

Gecko Embeddedプロジェクトと現在のステータスについて紹介しました。
組み込みLinux上のFirefoxでもハードウェアアクセラレーションを有効化して、実用的なパフォーマンスを発揮できることは確認できていますが、
安定化や、さらなるパフォーマンス改善、他のSoCのサポートなど、やるべきことはまだまだ残されていますので、
興味がある方は協力して頂けるとありがたいです。問題を発見した場合は[Gecko EmbeddedプロジェクトのIssueページ](https://github.com/mozilla-japan/gecko-embedded/issues)に報告して下さい。
