---
tags:
- milter-manager
title: milter manager 1.4.0リリース
---
[milter manager 1.3.1]({% post_url 2009-09-16-index %})にほとんど問題がなかったので、ドキュメントやOpenDKIMへの対応などを加えた[milter manager 1.4.0をリリース](http://milter-manager.sourceforge.net/blog/ja/2009/10/13.html)しました。1.4.0でログまわりを強化する予定もあったのですが、現時点で安定しているので、1.4.0での採用は見送り、次回のリリースにまわすことにしました。
<!--more-->


今のところ、milter managerは新しい安定版をおよそ3ヶ月毎の間隔でリリースしています。そのたびに、新しい機能や最新のmilterへの対応が強化されています。また、バグも修正されているので、古いバージョンを使っている方はアップグレードをおすすめします。インストール・アップグレードの方法は以下のドキュメントを参考にしてください。

  * [インストール](http://milter-manager.sourceforge.net/reference/ja/install.html)
  * [アップグレード](http://milter-manager.sourceforge.net/reference/ja/upgrade.html)

クリアコードでは[milter managerの導入支援やmilterを用いたメールフィルタの開発](/services/milter-manager.html)などサーバサイドの迷惑メール対策サービスから[Mozilla Thunderbird](/services/#mozilla)を用いたクライアントサイドの迷惑メール対策まで、メールシステム全体での迷惑メール対策サービスを提供しています。迷惑メールに困っている方はinfo@clear-code.comまでお問い合わせください。
