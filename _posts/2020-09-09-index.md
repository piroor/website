---
tags:
  - apache-arrow
title: Apache Arrowコンサルティングサポートを開始
---
Apache Arrowの開発に参加している須藤です。
<!--more-->


2020年7月24日にApache Arrowの最初のメジャーバージョン1.0.0がリリースされたので、クリアコードとして正式にApache Arrowのコンサルティングサポートを開始することにしました！詳細はプレスリリース[Apache Arrowコンサルティングサポートを開始](/press-releases/20200909-apache-arrow.html)を参照してください！

前から個別に声をかけてくれた方々には提供していたのですがコンサルティングサポートを提供していることを表明しました。

[2年前からデータ処理ツールの開発事業を立ち上げようと取り組んでいました]({% post_url 2018-07-11-index %})が、それの1つの形になります。感慨深いです。

コンサルティングサポートを利用したい方はもちろん、ビジネスパートナーとして一緒にApache Arrowを活用したい方からの[お問い合わせ](/contact/)をお待ちしています！また、[クリアコードに入社して一緒にコンサルティングサポートに取り組みたい人も募集](/recruitment/)しています！
