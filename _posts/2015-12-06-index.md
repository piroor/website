---
tags:
- groonga
- presentation
title: 'Groonga Meatup 2015：Groonga族2015 #groonga'
---
[Groonga Advent Calendar 2015](http://qiita.com/advent-calendar/2015/groonga)の6日目の記事です。5日目は[@KitaitiMakoto](https://github.com/KitaitiMakoto)さんの[DroongaをインストールするItamaeレシピ](https://kitaitimakoto.github.io/apehuci/2015/12/05.html)でした。
<!--more-->


先日の11月29日（いい肉の日）に[Groonga Meatup 2015](https://groonga.doorkeeper.jp/events/31482)が開催されました。その中で「Groonga族2015」と題してGroonga族の概要・最新情報・今後のことを話しました。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/groonga-meatup-2015/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/groonga-meatup-2015/" title="Groonga族2015">Groonga族2015</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show](https://slide.rabbit-shocker.org/authors/kou/groonga-meatup-2015/)

  * [スライド（SlideShare）](http://www.slideshare.net/kou/groonga-meatup-2015)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-groonga-meatup-2015)

### 内容

内容は大きくわけて次の3つです。

  * Groonga族の概要

  * Groonga族の最新情報

  * Groonga族の今後

なお、Groonga族とは次の条件に当てはまるプロダクトのことです。

  * Groongaそのもの

  * Groongaと他のプロダクトをつなぐプロダクト

  * 名前が「○roonga」なプロダクト

たとえば、次のプロダクトはGroonga族です。

  * [Groonga](http://groonga.org/ja/)（Groongaそのもの）

  * [Mroonga](http://mroonga.org/ja/)（GroongaとMySQLをつなぐプロダクト）

  * [PGroonga](http://pgroonga.github.io/ja/)（GroongaとPostgreSQLをつなぐプロダクト）

  * [Rroonga](http://ranguba.org/ja/#about-rroonga)（GroongaとRubyをつなぐプロダクト）

  * [Droonga](http://droonga.org/ja/)（名前が「○roonga」なプロダクト）

最初の「概要」の部分はプロダクトを紹介するときに使える情報が入っているので、Groonga族を知らない人はこの部分を活用してください。

続く「最新情報」の部分は今年のGroonga族の変更をキャッチアップできていない人にオススメの内容になっています。

最後の「今後」の部分はこれからのGroonga族の動向が気になる人にオススメの内容になっています。

Groonga族に興味のある方はぜひ確認してください。

イベントの他の発表についてはGroonga公式ブログの[Groonga Meatup 2015開催](http://groonga.org/ja/blog/2015/11/29/groonga-meatup-2015.html)を参照してください。

### おしらせ

隔週金曜日に「[Groongaで学ぶ全文検索](http://groonga.org/ja/blog/2015/09/22/learn-full-text-search-with-groonga-announce.html)」というイベントを開催しています。予習・復習なしで全文検索・Groongaの理解を深めることを大事にしているイベントです。全文検索・Groongaに興味のある方はご活用ください。

開催日は[新着イベント一覧ページ](https://groonga.doorkeeper.jp/events/upcoming)で確認できます。次回の開催日は[2015年12月18日](https://groonga.doorkeeper.jp/events/35021)です。定員が埋まっていた場合でもキャンセル待ちに登録していれば定員を増やせないか検討することができるので、参加したい方は定員が埋まっているかどうかに関わらず登録してください。
