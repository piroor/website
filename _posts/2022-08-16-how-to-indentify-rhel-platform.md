---
title: Red Hat Enterprise Linux系OSの種類やバージョンを見分ける方法
author: daipom
tags:
  - unix
---

最近GNU/Linux系のパッケージング周りの仕組みに初めてチャレンジしている福田です。

複数のプラットフォームを想定する場合、動作しているプラットフォームを見分けることが必要になります。

今回は、Red Hat Enterprise Linux系OSの種類とバージョンの見分け方をご紹介します。

<!--more-->

### Red Hat Enterprise Linux系のOSであることを見分ける

2つの方法があります。

* `/etc/redhat-release`というファイルが存在することを確認する
* `/etc/os-release`というファイルに記載されている`ID_LIKE`の値に、`rhel`が含まれていることを確認する

私のケースでは、前者の方法で十分でした。

### バージョンを見分ける

前章でも登場した`/etc/os-release`ファイルには、プラットフォームの情報が詳細に記載されています。

この中の`VERSION_ID`の値がOSのバージョンを表しており、ここを見ることでバージョンを見分けることができます。

### テクニック: `/etc/os-release`ファイルから値を取り出す

ビルドの設定ファイル等で、`/etc/os-release`ファイルからバージョンを読ませたい場合、カッコイイ方法があります。

このファイルは、`CentOS 7`では次のようになっています。

```console
$ cat /etc/os-release
NAME="CentOS Linux"
VERSION="7 (Core)"
ID="centos"
ID_LIKE="rhel fedora"
VERSION_ID="7"
PRETTY_NAME="CentOS Linux 7 (Core)"
ANSI_COLOR="0;31"
CPE_NAME="cpe:/o:centos:centos:7"
HOME_URL="https://www.centos.org/"
BUG_REPORT_URL="https://bugs.centos.org/"

CENTOS_MANTISBT_PROJECT="CentOS-7"
CENTOS_MANTISBT_PROJECT_VERSION="7"
REDHAT_SUPPORT_PRODUCT="centos"
REDHAT_SUPPORT_PRODUCT_VERSION="7"
```

よく見ると、これはシェルスクリプトとしてそのまま実行可能な形式で書かれていますよね！
よって、このファイルを実行すると各左辺の変数に各値が代入されて、それらを利用できるのです！

例えば次のようにすることで、`VERSION_ID`の値を確認できます。

```console
$ . /etc/os-release && echo $VERSION_ID
```

しかしこの方法だと、現在のシェルにどのような影響を与えるか分かりません。
そのため、実際には次のようにサブシェルの中で実行し、必要な値だけを取り出します。

```console
$ echo $(. /etc/os-release && echo $VERSION_ID)
```

`()`で囲うことにより、サブシェル、つまり現在のシェルの中でさらにシェルを立ち上げてコマンドを実行しています。
これにより現在のシェルへの意図しない影響を防ぐことができます。
さらに`$`を付けることで、サブシェル内の標準出力の内容を取得しています。

以上を踏まえると、次のように`/etc/os-release`から`VERSION_ID`の値を変数に取得できます。

```console
$ VERSION_ID=$(. /etc/os-release && echo $VERSION_ID)
```

サブシェルの中の`VERSION_ID`変数を、現在のシェルの`VERSION_ID`変数に代入したわけです！

### テクニック: メジャーバージョンだけ取り出す

前章で取り出した`VERSION_ID`ですが、OSによってはマイナーバージョンが付いていることがあります。

例えば`AlmaLinux 9`では、次のように`"9.0"`という値になっています。

```console
$ cat /etc/os-release 
NAME="AlmaLinux"
VERSION="9.0 (Emerald Puma)"
ID="almalinux"
ID_LIKE="rhel centos fedora"
VERSION_ID="9.0"
PLATFORM_ID="platform:el9"
PRETTY_NAME="AlmaLinux 9.0 (Emerald Puma)"
ANSI_COLOR="0;34"
LOGO="fedora-logo-icon"
CPE_NAME="cpe:/o:almalinux:almalinux:9::baseos"
HOME_URL="https://almalinux.org/"
DOCUMENTATION_URL="https://wiki.almalinux.org/"
BUG_REPORT_URL="https://bugs.almalinux.org/"

ALMALINUX_MANTISBT_PROJECT="AlmaLinux-9"
ALMALINUX_MANTISBT_PROJECT_VERSION="9.0"
REDHAT_SUPPORT_PRODUCT="AlmaLinux"
REDHAT_SUPPORT_PRODUCT_VERSION="9.0"
```

前章のテクニックと合わせて、次のようにメジャーバージョンだけを取り出すことができます。

```console
$ VERSION_ID=$(. /etc/os-release && echo $VERSION_ID | grep -oE '^[0-9]+')
```

前章のコマンドのサブシェル内の末尾に、`grep`コマンドをパイプで繋げています。
この`grep`コマンドにより、メジャーバージョンのみを取り出すことができます。

`AlmaLinux 9`における例

```console
$ VERSION_ID=$(. /etc/os-release && echo $VERSION_ID | grep -oE '^[0-9]+')
$ echo $VERSION_ID
9
```

`CentOS 7`のようにメジャーバージョンだけ記載されている場合にも、これを共通して利用できます。

仕組みを説明します。
`'^[0-9]+'`は、先頭から0から9の数字で始まる部分だけにマッチする正規表現になっています。
`^`は先頭を表し、`[0-9]`は0から9の数字のどれかであることを表し、`+`は1文字以上続くことを表しています。
また、`grep`は **マッチするものがあった行** をデフォルトで抽出するので、`o`オプションを付けることで **マッチした部分のみ** を取り出しています。
さらに`E`オプションを付けることで、先ほどの`+`の正規表現を利用できるようにしています。
マイナーバージョンの前に`.`があるため、これによりメジャーバージョンのみ抽出できています。

### まとめ

本記事では、Red Hat Enterprise Linux系OSの種類とバージョンの見分け方を紹介しました。

クリアコードではこのように業務の成果を公開することを重視しています。業務の成果を公開する職場で働きたい人は[クリアコードの採用情報]({% link recruitment/index.md %})をぜひご覧ください。
