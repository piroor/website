---
tags:
- fluentd
title: Fluentdで古くなったプラグインを検出する試み
---
Fluentd v0.14.xはFluentd v0.10.xのAPIがいくつか廃止されていて古いAPIを使っているプラグインが動作しなくなっています。また、他にも以下の観点でFluentd v0.14では使わない方がよい3rd party製プラグインが見つかっています。
<!--more-->


  * 古くなっていてメンテナンスされていないもの

  * 最新のFluentdに添付されているプラグインでは、その機能が取り込まれているため使う意味がないもの

  * 対象のミドルウェアがメンテナンスされなくなっているもの

  * 対象のウェブサービスがサービス提供を終了しているもの

  * 旧バージョンのRubyでのみ意味があったもの

  * ある問題を解決するためにrubygems.orgで公開されていたが、アップストリームで対応されるなどして問題が解決されたため不要になったもの

Fluentdを使いたいユーザーがプラグインを探すときは[プラグイン一覧](http://www.fluentd.org/plugins/all)を見ると思いますが、ここのリストは機械的に更新されています。ユーザーが使いたいプラグインをより見つけやすくするために、[もう使わない方がよいプラグインをリスト化してメンテナンスしています](https://github.com/fluent/fluentd-website/blob/master/scripts/obsolete-plugins.yml)。

このリストは手動でメンテナンスされていて[fluent/fluentd-website](https://github.com/fluent/fluentd-website)にPull requestを送ると取り込まれて、翌日にはウェブサイト上に反映されるはずです。

記事執筆時点では約40個のプラグインがこのリストに載っています。このリストを見れば、使わない方がよいプラグインを確認することができます。
新しく使用するプラグインを検討するときは、このリストを目視で確認していくとよいのですが、既にインストール済みのプラグインがたくさんある場合は、目視で確認するのはとても大変です。

そこでFluentd起動時に確認できる[fluent-plugin-obsolete-plugins](https://github.com/okkez/fluent-plugin-obsolete-plugins)というプラグインを作ってみました。

このプラグインは、Fluentd起動時に[非推奨プラグインのリスト](https://github.com/fluent/fluentd-website/blob/master/scripts/obsolete-plugins.yml)を読み込んでその環境で利用可能なgemのリストと比較し、非推奨なプラグインがインストールされていたら警告します。このプラグイン自体はFluentd v0.14 APIを使用して実装したレコードを素通しするFilterプラグインです。v0.14 APIを使用して実装したので、Fluentd v0.12以前のバージョンでは利用できません。

Fluentd v0.14の検証のついでに、非推奨なプラグインのチェックも一緒にしてみてはいかがでしょうか。
