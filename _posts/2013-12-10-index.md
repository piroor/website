---
tags:
- ruby
title: パーフェクトRubyのおかげでYARDがよくなってGroongaイベントも開催できた話
---
[パーフェクトRuby Advent Calendar 2013](http://www.adventar.org/calendars/198)の10日目の記事です。
<!--more-->


9日目の記事は[パーフェクトなCRubyを目指して - 1行のバグ修正に潜む苦労 - - I am Cruby!](http://d.hatena.ne.jp/authorNari/20131209/1386583244)です。

### パーフェクトRubyとの関わり

それは[札幌Ruby会議2012に「クリアなコードの作り方」という話をしに行ったとき]({% post_url 2012-09-19-index %})のことです。会場に向かう途中でパーフェクトRubyの著者の1人の[@ryopeko](https://twitter.com/ryopeko)さんとばったり会いました。そのとき、「Rubyの本を書いているんですよー。[YARD](http://yardoc.org/)のことも書いているんですけど、バグを見つけちゃったんですよねぇ。直し方はわかったんですけど、テストが書けなくて…どうしたらいいですかねぇ。」みたいな話を聞きました。それに対してなんと言ったのか具体的には覚えていませんが、「それでもPull Requestすればいいと思うよ！」みたいなことを言った気がします。

その後、@ryopekoさんは[YARDにPull Requestを送り](https://github.com/lsegal/yard/pull/651)[^0]、YARDの問題が1つ直りました。

さらにその後、同じくパーフェクトRubyの著者の1人の[@takkanm](https://twitter.com/takkanm)さんから、「自分が書いたところとかYARDのところとかいくつかの章だけでもレビューしてくれないか」という打診があったのでレビューアーとして参加しました。

これが、パーフェクトRubyとの関わりです。パーフェクトRubyが出版されたときは[紹介記事]({% post_url 2013-08-19-index %})を書きました。

### Groongaイベント

パーフェクトRubyが出版されたあと、レビューアーに誘ってもらえなかったかくたにさんから「パーフェクトRubyのちょっとした打ち上げをやるのでいかがですか」というお誘いをもらいました。もちろん、喜んで参加しました。

その会で、「毎年いい肉の日にやっているGroongaイベントの場所をどうするか悩んでいるんですよねー」と漏らしたところ、@ryopekoさんと[@sunaot](https://twitter.com/sunaot)さんのDeNAコンビが「うちでやりなよ！」と言ってくれました。そのおかげで、[2013年のGroongaイベント](http://atnd.org/events/43461)を成功させることができました[^1]。

### まとめ

パーフェクトRubyはRubyの最新情報を網羅的に提供しているだけでなく、YARDもよくして、Groongaイベントの開催も支援して、すばらしいですね！

[^0]: コミットメッセージでtypoしているのがかわいらしいですね。

[^1]: DeNAさんのイベント支援力、ものすごいですよ！これは何度でも強調するべきことです！ものすごいです！
