---
tags:
- groonga
title: Sphinxの国際化機能を使って複数言語用ドキュメントを用意する方法（使い方）
---
[Sphinxの国際化機能を使って複数言語用ドキュメントを用意する方法（概要）]({% post_url 2011-05-31-index %})で示した複数言語用ドキュメントを用意する仕組みの*使い方*を紹介します。本当は仕組みについて説明するつもりだったのですが、使い方を書いていたら長くなったので分けることにしました。この仕組みは実際に[groonga](http://groonga.org/)で使っているもので、以下のような使い方になります。
<!--more-->


  * ドキュメントを英語で書く。
  * 他の言語への翻訳をPOファイルに書く。
  * それぞれの言語毎にHTMLを生成する。

まずファイル構成を紹介して、その後、実例を示しながら具体的な作業を紹介します。

### ファイル構成

まずファイル構成です。これで概要を掴んでください。

groongaはAutomakeなどのGNUビルドシステムを利用しているため、このような構成になっています。違うビルドシステムを利用している場合は違う構成にした方がよいかもしれません。その場合でも英語を特別扱いせずに`doc/locale/`以下に各言語毎のディレクトリを作るという方法は真似した方がよいでしょう。この方が規則を単純化できるため、ビルドシステムが単純になるはずです。

{% raw %}
```
.
|-- build
|   `-- makefiles （doc/locale/#{言語}/以下で共有するMakefile）
|       |-- LC_MESSAGES.am （doc/locale/#{言語}/LC_MESSAGES/Makefile.amでinclude）
|       |-- gettext-files.am
|       |-- gettext.am
|       |-- locale.am （doc/locale/#{言語}/Makefile.amでinclude）
|       |-- sphinx-build.am
|       `-- sphinx.am
`-- doc
    |-- Makefile.am
    |-- locale （各言語用ディレクトリ置き場）
    |   |-- Makefile.am
    |   |-- en （英語用ディレクトリ）
    |   |   |-- LC_MESSAGES（翻訳テキストなし。単なる置き場所。）
    |   |   |   |-- Makefile.am
    |   |   |   |-- *.po
    |   |   |   `-- *.mo
    |   |   |-- Makefile.am
    |   |   |-- html/ （生成された英語のHTML）
    |   |   |-- html-build-stamp （HTMLが生成されたことを示すだけのファイル）
    |   |   |-- man/ （生成された英語のman）
    |   |   `-- man-build-stamp （manが生成されたことを示すだけのファイル）
    |   `-- ja （日本語用ディレクトリ）
    |       |-- LC_MESSAGES （英語→日本語の翻訳テキスト）
    |       |   |-- Makefile.am
    |       |   |-- *.po
    |       |   `-- *.mo
    |       |-- Makefile.am
    |       |-- html/ （生成された日本語のHTML）
    |       |-- html-build-stamp （HTMLが生成されたことを示すだけのファイル）
    |       |-- man/ （生成された日本語のman）
    |       `-- man-build-stamp （manが生成されたことを示すだけのファイル）
    |-- source/ （ドキュメント本体）
    `-- sphinx/ （最新のSphinx）
```
{% endraw %}

[Sphinxの国際化のドキュメント（英語）](http://sphinx.pocoo.org/latest/intl.html)では`source/`以下に`translated/#{言語}/LC_MESSAGES/`というディレクトリを作って、そこに`*.mo`を置くような例になっています。しかし、上記の構成では`source/`の下ではなく、`source/`と同じディレクトリに`locale/#{言語}/LC_MESSAGES/`を作っています。これは、`source/`以下に`*.mo`など自動生成するファイルを置かないようにするためです。

`source/`以下にファイルがあるとデフォルトで`sphinx-build`の処理対象となってしまいます。そのため、`source/`以下に`translated/#{言語}/LC_MESSAGES/`を置く場合は`conf.py`内で<var>exclude_patterns</var>を使って処理対象でないことを明示する必要があります。明示的に処理対象外とするくらいなら、はじめから`source/`以下ではなく場所に置いた方がよいのではないか、ということで`source/`の下ではなく、同じディレクトリに`locale/#{言語}/LC_MESSAGES/`を置いています。

### 使い方

それではこの仕組みを使ったドキュメントの作成方法です。

ドキュメント作成の流れは以下のようになります。ここでは、`new-document`というドキュメントを追加するという例で話を進めます。

  1. `doc/source/new-document.txt`を作成し、英語でドキュメントを書く。

  1. `doc/locale/ja/LC_MESSAGES/`へ移動する。

  1. `make init`を実行する。

  1. ↑で`new-document.po`ができるので、それをリポジトリへ追加する。

  1. `new-document.po`を翻訳する。

  1. `doc/locale/ja/`へ移動する。

  1. `make html`を実行する。

  1. ↑で`doc/locale/ja/html/`以下に翻訳されたHTMLが生成されるので確認する。


ドキュメントの作成はこの作業を繰り返すことになります。実例を以下に示します。

#### 1. 英語でドキュメントを書く。

まず、英語でドキュメントを作成します。以下の内容のドキュメントにしたとします。

`doc/source/new-document.txt`:

{% raw %}
```
New Document
============

Hi! This is new document.
```
{% endraw %}

次に、既存のページからリンクを張ります。こうしないとどこからも辿れないページになってしまいます。

`doc/source/index.txt`:

{% raw %}
```
...
* :doc:`new-document`
...
```
{% endraw %}

これでオリジナルのドキュメントができました。

#### 2., 3., 4. POファイルの作成

次は、翻訳テキストを書くファイルであるPOファイルを作成します。

{% raw %}
```
[groonga]% cd doc/locale/ja/LC_MESSAGES
[groonga/doc/locale/ja/LC_MESSAGES]% make init
...
ユーザが翻訳に関するフィードバックをあなたに送ることができるように,
新しいメッセージカタログにはあなたの email アドレスを含めてください.
またこれは, 予期せぬ技術的な問題が発生した場合に管理者があなたに連絡が取れる
ようにするという目的もあります.

Is the following your email address?
  kou@clear-code.com
Please confirm by pressing Return, or enter your email address.
kou@clear-code.com ← 入力
http://translationproject.org/team/index.html を検索中... 完了.
A translation team for your language (ja) does not exist yet.
If you want to create a new translation team for ja, please visit
  http://www.iro.umontreal.ca/contrib/po/HTML/teams.html
  http://www.iro.umontreal.ca/contrib/po/HTML/leaders.html
  http://www.iro.umontreal.ca/contrib/po/HTML/index.html

new-document.po を生成.
[groonga/doc/locale/ja/LC_MESSAGES]%
```
{% endraw %}

上記のコマンドで以下のような内容のPOファイルが作成されます。

`doc/locale/ja/LC_MESSAGES/new-document.po`:

{% raw %}
```
# Japanese translations for 1.2.2 package.
# Copyright (C) 2009-2011, Brazil, Inc
# This file is distributed under the same license as the groonga package.
# Kouhei Sutou <kou@clear-code.com>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.2.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-06-19 19:18\n"
"PO-Revision-Date: 2011-06-19 19:18+0900\n"
"Last-Translator: Kouhei Sutou <kou@clear-code.com>\n"
"Language-Team: Japanese\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ja\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../../../source/new-document.txt:2
msgid "New Document"
msgstr ""

#: ../../../source/new-document.txt:4
msgid "Hi! This is new document."
msgstr ""
```
{% endraw %}

翻訳対象のメッセージは`msgid "New Document"`と`msgid "Hi! This is new document."`の部分です。

内容を確認したらリポジトリに登録しましょう。

{% raw %}
```
[groonga/doc/locale/ja/LC_MESSAGES]% git add new-document.pot
[groonga/doc/locale/ja/LC_MESSAGES]% git commit
```
{% endraw %}

（ここで`build/makefiles/Makefile.am`の中にある<var>po_files</var>や<var>mo_files</var>などを更新する必要があるのですが、ここでは割愛します。）

#### 5. POファイルの更新

生成されたPOファイルは`msgstr ""`と`msgstr`の部分が空文字列になっています。ここに翻訳後のテキストを入力します。

`doc/locale/ja/LC_MESSAGES/new-document.po`:

{% raw %}
```
...
#: ../../../source/new-document.txt:2
msgid "New Document"
msgstr "新しいドキュメント"

#: ../../../source/new-document.txt:4
msgid "Hi! This is new document."
msgstr "わーい！新しいドキュメントだよー。"
```
{% endraw %}

#### 6., 7., 8. 翻訳されたドキュメントの確認

以下の手順で翻訳され日本語ドキュメントとなったHTMLを出力できます。

{% raw %}
```
[groonga/doc/locale/ja/LC_MESSAGES]% cd ..
[groonga/doc/locale/ja]% make html
[groonga/doc/locale/ja]% firefox html/new-document.html
```
{% endraw %}

以下のようなHTMLがブラウザで表示されたはずです。

{% raw %}
```
...
<h1>新しいドキュメント...</h1>
<p>わーい！新しいドキュメントだよー。</p>
...
```
{% endraw %}

### まとめ

[groonga](http://groonga.org/)で採用しているSphinxの国際化機能を使って複数言語用ドキュメントを用意する方法の使い方について紹介しました。なお、新しい言語の追加方法については[groongaの翻訳方法のドキュメント](http://groonga.org/ja/docs/contribution/documentation/i18n.html)で説明しています。

次こそはこの仕組みについて説明できるはずです。
