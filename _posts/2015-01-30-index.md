---
tags:
- presentation
title: 'CROSS 2015 全文検索エンジン群雄割拠〜あなたが使うべきはどれだ！〜：Groongaの紹介 #cross2015'
---
[CROSS 2015](http://2015.cross-party.com/)の[全文検索エンジン群雄割拠〜あなたが使うべきはどれだ！〜](http://2015.cross-party.com/program/c4)というセッションで他の全文検索との違いという観点でGroongaの紹介をしました。
<!--more-->


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/cross-2015/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/cross-2015/" title="Groongaの特徴">Groongaの特徴</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/cross-2015/)
  * [スライド（SlideShare）](http://www.slideshare.net/kou/cross-2015)
  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-cross-2015)

### 内容

全文検索エンジンの性能面での特徴では次のようなこともあるのですが、今回は「既存のシステムと連携できる」という観点で紹介しました。

  * 更新をしているときも検索性能が落ちない（参照ロックフリーな実装のため）

連携できるという観点をもっとアピールできるように、この日のために[PostgreSQLと連携できるPGroongaという拡張機能を開発して肉の日リリース](http://groonga.org/ja/blog/2015/01/29/pgroonga-0.2.0.html)しました。まだ最初のリリースで最小限の機能しかなかったりしますが、興味のある方はぜひ試してみてください。

### まとめ

CROSS 2015の「全文検索エンジン群雄割拠〜あなたが使うべきはどれだ！〜」でGroongaを紹介するときに使った資料を紹介しました。ElasticsearchやSolrのユーザーと同じくらい（少し少ないくらい）Groonga関連プロダクトのユーザーがいました。（いるように見えました。）インターネット上の情報はElasticsearchやSolrの方が圧倒的に多いのですが、実際に使っている人は同じくらいいたことがわかったので参加できてよかったです。

全文検索エンジンを検討するときはGroonga関連プロダクトも検討対象に入れてみてください。
