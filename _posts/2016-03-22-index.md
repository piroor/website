---
tags:
- groonga
title: pglogicalとPGroongaを使ったレプリケーション対応の高速日本語全文検索可能なPostgreSQLクラスターの作り方
---
PostgreSQLは標準機能ではインデックスを使った日本語全文検索機能がありません。PostgreSQLでインデックスを使った高速な日本語全文検索を実現する拡張機能には[PGroonga](http://pgroonga.github.io/ja/)（ぴーじーるんが）があります。しかし、PGroongaはPostgreSQL標準のレプリケーション機能を使えません。これは、PostgreSQLが拡張機能で追加したインデックスのレプリケーションをサポートしていないからです。
<!--more-->


レプリケーション機能を提供する拡張機能があり、それを使うとPGroongaでもレプリケーションを実現できます。たとえば、[pg_shardを使う方法]({% post_url 2015-05-18-index %})があります。

ここでは、別の方法として[pglogical](http://2ndquadrant.com/en-us/resources/pglogical/)を使う方法を紹介します。

なお、pglogicalの開発者は[PostgreSQL 9.6へpglogicalを含めることを提案](http://www.postgresql.org/message-id/flat/CAMsr+YGc6AYKjsCj0Zfz=X4Aczonq1SfQx9C=hUYUN4j2pKwHA@mail.gmail.com#CAMsr+YGc6AYKjsCj0Zfz=X4Aczonq1SfQx9C=hUYUN4j2pKwHA@mail.gmail.com)しています。どうなるかはわかりませんが、もしかしたら、将来のPostgreSQLにはpglogicalが含まれているかもしれません。

### 構築方法

pglogicalとPGroongaを使ったレプリケーション対応の高速日本語全文検索可能なPostgreSQLクラスターの構築方法を説明します。

ディストリビューションはCentOS 7を使い、PostgreSQLは9.5を使います。

クラスターには次のノードがあるとします。

<table>
  <thead>
    <tr>
      <th>役割</th>
      <th>ホスト名</th>
      <th>IPアドレス</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>マスターノード</td>
      <td>master</td>
      <td>192.168.0.16</td>
    </tr>
    <tr>
      <td>スレーブノード1</td>
      <td>slave1</td>
      <td>192.168.0.17</td>
    </tr>
    <tr>
      <td>スレーブノード2</td>
      <td>salve2</td>
      <td>192.168.0.18</td>
    </tr>
  </tbody>
</table>


このクラスターでは`blog`データベースをレプリケーションします。

それでは、まずはマスターノードをセットアップし、その後スレーブノードをセットアップします。

#### マスターノードのセットアップ

PostgreSQLをパッケージでインストールします。

```text
% sudo rpm -ivh http://yum.postgresql.org/9.5/redhat/rhel-$(rpm -qf --queryformat="%{VERSION}" /etc/redhat-release)-$(rpm -qf --queryformat="%{ARCH}" /etc/redhat-release)/pgdg-centos95-9.5-2.noarch.rpm
% sudo -H yum install -y postgresql95-server
% sudo -H /usr/pgsql-9.5/bin/postgresql95-setup initdb
% sudo -H systemctl enable postgresql-9.5
```


pglogicalをパッケージでインストールします。詳細は[pglogicalのインストールドキュメント（英語）](http://2ndquadrant.com/en/resources/pglogical/pglogical-installation-instructions/)を参照してください。

```text
% sudo -H yum install -y postgresql95-contrib
% sudo -H yum install -y http://packages.2ndquadrant.com/pglogical/yum-repo-rpms/pglogical-rhel-1.0-1.noarch.rpm
% sudo -H yum install -y postgresql95-pglogical
```


PGroongaをパッケージでインストールします。詳細は[PGroongaのインストールドキュメント](http://pgroonga.github.io/ja/install/centos.html#install-on-7)を参照してください。

```text
% sudo -H yum install -y http://packages.groonga.org/centos/groonga-release-1.1.0-1.noarch.rpm
% sudo -H yum install -y postgresql95-pgroonga
```


これで必要なパッケージはすべてインストールできたので設定をします。

まずは`postgresql.conf`を設定します。

外部からの接続を受け付けるようにするため、`listen_address`を`*`にします。

/var/lib/pgsql/9.5/data/postgresql.conf:

```text
listen_addresses = '*'
```


続いてpglogical用の設定をします。詳細は[pglogicalのドキュメント（英語）](http://2ndquadrant.com/en/resources/pglogical/pglogical-docs/)を参照してください。

次の項目を設定します。

<table>
  <thead>
    <tr>
      <th>項目</th>
      <th>値</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>wal_level</code></td>
      <td><code>'logical'</code></td>
    </tr>
    <tr>
      <td><code>max_replication_slots</code></td>
      <td><code>2</code></td>
    </tr>
    <tr>
      <td><code>max_wal_senders</code></td>
      <td><code>2</code></td>
    </tr>
    <tr>
      <td><code>shared_preload_libraries</code></td>
      <td><code>'pglogical'</code></td>
    </tr>
  </tbody>
</table>


`max_replication_slots`と`max_wal_senders`が`2`なのは、今回の例ではスレーブノードが2台だからです。スレーブノードの数だけこの値を増やす必要があります。

なお、`max_worker_processes`をレプリケーションするデータベースの数以上に設定する必要もありますが、今回はデフォルト（`8`）から変更しません。理由は、今回の例では`blog`データベースだけをレプリケーションするからです。1つなのでデフォルトの`8`で十分だからです。

具体的には次のように設定します。

/var/lib/pgsql/9.5/data/postgresql.conf:

```text
wal_level = 'logical'

max_replication_slots = 2
max_wal_senders       = 2

shared_preload_libraries = 'pglogical'
```


続いて`pg_hba.conf`を設定します。

次の2つの設定を追加します。

  * 後で作成するデータベース操作用のユーザー（`blog_user`）がローカルネットワークから`blog`データベースへパスワード接続することを許可

  * 後で作成するレプリケーション用のユーザー（`blog_replication`）がスレーブノードからのレプリケーションのためにパスワード接続することを許可

/var/lib/pgsql/9.5/data/postgresql.conf:

```text
host blog        blog_user        192.168.0.0/24 md5
host replication blog_replication 192.168.0.0/24 md5
```


これで起動前の設定は完了したのでPostgreSQLを起動します。

```text
% sudo -H systemctl start postgresql-9.5
```


まず、データベースを使用するユーザー`blog_user`を作成します。パスワードは`user_password`にしたとします。

```text
% sudo -u postgres -H createuser blog_user --pwprompt
```


続いて、レプリケーション時にスレーブノードから接続するユーザー`blog_replication`を作成します。パスワードは`replication_password`にしたとします。

```text
% sudo -u postgres -H createuser blog_replication --pwprompt --replication
```


`blog`データベースを作成します。`blog_user`をオーナーにします。

```text
% sudo -u postgres -H createdb --owner blog_user blog
```


`blog`データベースをセットアップします。これはスーパーユーザーで実行する必要があります。

```text
% sudo -u postgres -H psql blog
```


データベースにpglogicalをインストールします。詳細は[pglogicalのドキュメント（英語）](http://2ndquadrant.com/en/resources/pglogical/pglogical-docs/)を参照してください。

```sql
CREATE EXTENSION pglogical;
GRANT USAGE ON SCHEMA pglogical TO blog_user;
GRANT USAGE ON SCHEMA pglogical TO blog_replication;
GRANT SELECT ON ALL TABLES IN SCHEMA pglogical TO blog_replication;
```


PGroongaもインストールします。詳細は[PGroongaのドキュメント](http://pgroonga.github.io/ja/reference/grant-usage-on-schema-pgroonga.html)を参照してください。

```sql
CREATE EXTENSION pgroonga;
GRANT USAGE ON SCHEMA pgroonga TO blog_user;
```


スーパーユーザー権限が必要なのはここまでです。`blog_user`で接続しなおします。

```text
% psql --user blog_user --host 192.168.0.16 blog
```


テーブルとPGroongaのインデックスを作成します。いくつかデータも投入します。PGroongaのインデックス作成方法・使い方については[PGroongaのチュートリアル](http://pgroonga.github.io/ja/tutorial/)を参照してください。

```sql
CREATE TABLE posts (
  id text PRIMARY KEY,
  title text NOT NULL,
  body text NOT NULL
);
CREATE INDEX posts_full_text_index ON posts USING pgroonga (id, title, body);
INSERT INTO posts VALUES ('2016-03-20-pgroonga',
                          'はじめてのPGroonga',
                          'PGroongaを使いはじめました！');
INSERT INTO posts VALUES ('2016-03-21-pglogical',
                          'pglogicalにトライ',
                          'pglogicalを試しています。PGroongaと一緒に使えるかな？');
INSERT INTO posts VALUES ('2016-03-22-pgroonga-and-pglogical',
                          'PGroongaとpglogical',
                          'pglogicalとPGroongaを一緒に使えました！');
```


インデックスを使って「一緒」が含まれる投稿を全文検索してみます。

```sql
SET enable_seqscan = off;
SELECT body, pgroonga.score(posts) FROM posts WHERE body %% '一緒';
--                          body                          | score 
-- -------------------------------------------------------+-------
--  pglogicalを試しています。PGroongaと一緒に使えるかな？ |     1
--  pglogicalとPGroongaを一緒に使えました！               |     1
-- (2 行)
```


うまく動いていますね。

それでは、`blog`データベースをレプリケーション対象にしましょう。

まず、ノードとして登録します。これは1度だけやる処理です。詳細は[pglogicalのドキュメント（英語）](http://2ndquadrant.com/en/resources/pglogical/pglogical-docs/)を参照してください。

```sql
SELECT pglogical.create_node(
  node_name := 'master',
  dsn := 'host=192.168.0.16 port=5432 dbname=blog'
);
```


現在の`blog`データベース（の`public`スキーマ）内のすべてのテーブルをレプリケーション対象にします。

```sql
SELECT pglogical.replication_set_add_all_tables('default', ARRAY['public']);
```


新しくテーブルを追加したときはこの処理を再度実行する必要があることに注意してください。そうしないとレプリケーション対象になりません。これはハマりポイントなので最後に改めて言及します。

これでマスターのセットアップは完了です。

#### スレーブノードのセットアップ

スレーブノードのセットアップは基本的なセットアップはマスターノードと同じですが、省略せずに説明します。

PostgreSQLをパッケージでインストールします。

```text
% sudo rpm -ivh http://yum.postgresql.org/9.5/redhat/rhel-$(rpm -qf --queryformat="%{VERSION}" /etc/redhat-release)-$(rpm -qf --queryformat="%{ARCH}" /etc/redhat-release)/pgdg-centos95-9.5-2.noarch.rpm
% sudo -H yum install -y postgresql95-server
% sudo -H /usr/pgsql-9.5/bin/postgresql95-setup initdb
% sudo -H systemctl enable postgresql-9.5
```


pglogicalをパッケージでインストールします。詳細は[pglogicalのインストールドキュメント（英語）](http://2ndquadrant.com/en/resources/pglogical/pglogical-installation-instructions/)を参照してください。

```text
% sudo -H yum install -y postgresql95-contrib
% sudo -H yum install -y http://packages.2ndquadrant.com/pglogical/yum-repo-rpms/pglogical-rhel-1.0-1.noarch.rpm
% sudo -H yum install -y postgresql95-pglogical
```


PGroongaをパッケージでインストールします。詳細は[PGroongaのインストールドキュメント](http://pgroonga.github.io/ja/install/centos.html#install-on-7)を参照してください。

```text
% sudo -H yum install -y http://packages.groonga.org/centos/groonga-release-1.1.0-1.noarch.rpm
% sudo -H yum install -y postgresql95-pgroonga
```


これで必要なパッケージはすべてインストールできたので設定をします。

まずは`postgresql.conf`を設定します。

外部からの接続を受け付けるようにするため、`listen_address`を`*`にします。

/var/lib/pgsql/9.5/data/postgresql.conf:

```text
listen_addresses = '*'
```


続いてpglogical用の設定をします。詳細は[pglogicalのドキュメント（英語）](http://2ndquadrant.com/en/resources/pglogical/pglogical-docs/)を参照してください。

次の項目を設定します。

<table>
  <thead>
    <tr>
      <th>項目</th>
      <th>値</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>max_replication_slots</code></td>
      <td><code>1</code></td>
    </tr>
    <tr>
      <td><code>shared_preload_libraries</code></td>
      <td><code>'pglogical'</code></td>
    </tr>
  </tbody>
</table>


pglogicalでは複数のマスターノードからレプリケーションすることもできます。その場合は、`max_replication_slots`と`max_worker_processes`をマスターノード数以上に設定する必要があります。

今回は`max_worker_processes`はデフォルト（`8`）から変更しません。理由は、今回の例ではマスターノードが1つなのでデフォルトの`8`で十分だからです。

具体的には次のように設定します。

/var/lib/pgsql/9.5/data/postgresql.conf:

```text
wal_replication_slots = 1

shared_preload_libraries = 'pglogical'
```


続いて`pg_hba.conf`を設定します。なお、この設定は`pglogical`の設定ではなく一般的な設定です。自分の利用方法に合わせて調整してください。ただし、データベース操作用のユーザー名はマスターノードと合わせておいた方がデフォルト設定を使えるため便利です。

次の設定を追加します。

  * 後で作成するデータベース操作用のユーザー（`blog_user`）がローカルネットワークから`blog`データベースへパスワード接続することを許可

/var/lib/pgsql/9.5/data/postgresql.conf:

```text
host blog blog_user 192.168.0.0/24 md5
```


これで起動前の設定は完了したのでPostgreSQLを起動します。

```text
% sudo -H systemctl start postgresql-9.5
```


データベースを使用するユーザー`blog_user`を作成します。

```text
% sudo -u postgres -H createuser blog_user --pwprompt
```


レプリケーション初期化時のデータ同期時に必要になる（マスターノードで`GRANT ... TO blog_replication`しているため）ので`blog_replication`ユーザーを作成します。ログインするわけではないのでログイン不可にします。

```text
% sudo -u postgres -H createuser blog_replication --no-login
```


`blog`データベースを作成します。`blog_user`をオーナーにします。

```text
% sudo -u postgres -H createdb --owner blog_user blog
```


`blog`データベースをセットアップします。これはスーパーユーザーで実行する必要があります。

```text
% sudo -u postgres -H psql blog
```


データベースにpglogicalをインストールします。詳細は[pglogicalのドキュメント（英語）](http://2ndquadrant.com/en/resources/pglogical/pglogical-docs/)を参照してください。

```sql
CREATE EXTENSION pglogical;
GRANT USAGE ON SCHEMA pglogical TO blog_user;
```


PGroongaもインストールします。詳細は[PGroongaのドキュメント](http://pgroonga.github.io/ja/reference/grant-usage-on-schema-pgroonga.html)を参照してください。

```sql
CREATE EXTENSION pgroonga;
GRANT USAGE ON SCHEMA pgroonga TO blog_user;
```


スーパーユーザー権限が必要なのはここまでです。`blog_user`で接続しなおします。接続先のIPアドレスは捜査対象のスレーブノードのIPアドレスになっているか確認してください。

```text
% psql --user blog_user --host 192.168.0.17 blog
```


まず、ノードとして登録します。これは1度だけやる処理です。スレーブノードごと`node_name`を変えてください。以下は詳細は[pglogicalのドキュメント（英語）](http://2ndquadrant.com/en/resources/pglogical/pglogical-docs/)を参照してください。

なお、ここで設定した接続情報はレプリケーション開始時にデータベースの内容を同期するときに使われます。その際、スーパーユーザー権限が必要になるのでスーパーユーザーの接続情報を指定してください。以下の設定ではUNIXドメインソケット経由で`postgres`ユーザーで接続します。（`pglogical.create_subscription`のオプションで同期を無効にできます。その場合はスーパーユーザー権限は必要ありません。）

```sql
SELECT pglogical.create_node(
  node_name := 'slave1',
  dsn := 'dbname=blog'
);
```


レプリケーションを開始します。これも1度だけやる処理です。`subscription_name`はスレーブごとに違う値にします。`host`はマスターノードのIPアドレス（またはホスト名）にします。

```sql
SELECT pglogical.create_subscription(
    subscription_name := 'subscription1',
    provider_dsn := 'host=192.168.0.16 port=5432 dbname=blog user=blog_replication password=replication_password'
);
```


レプリケーションを開始するとデータを同期するので`posts`テーブルができてデータが入っています。

```sql
SELECT * FROM posts;
--                 id                 |        title        |                      
--    body                          
-- -----------------------------------+---------------------+----------------------
-- ---------------------------------
--  2016-03-20-pgroonga               | はじめてのPGroonga  | PGroongaを使いはじめ
-- した！
--  2016-03-21-pglogical              | pglogicalにトライ   | pglogicalを試していま
-- す。PGroongaと一緒に使えるかな？
--  2016-03-22-pgroonga-and-pglogical | PGroongaとpglogical | pglogicalとPGroongaを
-- 一緒に使えました！
-- (3 行)
```


もちろん、PGroongaを使った高速日本語全文検索も動きます。

```sql
SET enable_seqscan = off;
SELECT body, pgroonga.score(posts) FROM posts WHERE body %% '一緒';
--                          body                          | score 
-- -------------------------------------------------------+-------
--  pglogicalを試しています。PGroongaと一緒に使えるかな？ |     1
--  pglogicalとPGroongaを一緒に使えました！               |     1
-- (2 行)
```


マスターノードでデータを追加するとスレーブノードから参照できます。

マスターノードで実行：

```sql
INSERT INTO posts VALUES ('2016-03-23-pgroonga-postgresql-96',
                          'PostgreSQL 9.6でPGroonga',
                          'PGroongaはPostgreSQL 9.6と一緒でも使えた！');
```


スレーブノードで実行：

```sql
SET enable_seqscan = off;
SELECT body, pgroonga.score(posts) FROM posts WHERE body %% '一緒';
--                          body                          | score 
-- -------------------------------------------------------+-------
--  pglogicalを試しています。PGroongaと一緒に使えるかな？ |     0
--  pglogicalとPGroongaを一緒に使えました！               |     0
--  PGroongaはPostgreSQL 9.6と一緒でも使えた！            |     0
-- (3 行)
```


どちらのスレーブノードでも新しく追加したレコードがヒットします。

### まとめ

pglogicalとPGroongaを使ったレプリケーション対応の高速日本語全文検索可能なPostgreSQLクラスターの作り方を説明しました。

[pglogicalのドキュメントの「4. Limitations and Restrictions」](http://2ndquadrant.com/en/resources/pglogical/pglogical-docs/)にある通り、いくつか制限はありますがレコードの追加・更新・削除をしてもスレーブノードで検索できるという基本的なことは実現可能です。制限は自分のユースケースでクリティカルなものか確認し、pglogicalの使用を検討してください。なお、制限とは、たとえば、DDL（`CREATE TABLE`など）はレプリケーションされない、`PRIMARY KEY`がないと更新・削除がレプリケーションされない、などです。

DDLがレプリケーションされないことが運用に与える影響について少し補足します。

DDLがレプリケーションされないということは`CREATE TABLE`がレプリケーションされないということです。つまり、マスターノードでテーブルを作成したらスレーブノードでも同じテーブルを定義する必要があるということです。さらに、テーブルを定義するだけではレプリケーションされないなので、テーブルを作ったらレプリケーションの設定も更新する必要があります。具体的にはマスターノードで次のSQLを実行します。（pglogicalのドキュメントではトリガーでこの作業を自動化する方法を紹介しています。）

```sql
SELECT pglogical.replication_set_add_all_tables('default', ARRAY['public']);
```


次の順序で操作した場合はこれだけでOKです。

  1. マスターノードでテーブル作成

  1. スレーブノードでテーブル作成

  1. マスターノードで`pglogical.replication_set_add_all_tables`を実行

  1. マスターノードでデータ追加

しかし、次のようにデータを追加してから`pglogical.replication_set_add_all_tables`を実行した場合はもう1つやることがあります。

  1. マスターノードでテーブル作成

  1. スレーブノードでテーブル作成

  1. マスターノードでデータ追加

  1. マスターノードで`pglogical.replication_set_add_all_tables`を実行

次のようにスレーブノードで`pglogical.alter_subscription_resynchronize_table`を実行します。

```sql
SELECT pglogical.alter_subscription_resynchronize_table(
    subscription_name := 'subscription1',
    relation := 'posts'
);
```


これでデータが同期されます。

それでは、pglogicalとPGroongaで高速日本語全文検索を実現してください。
