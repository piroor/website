---
tags:
- milter-manager
title: 第09回 まっちゃ445勉強会の資料公開
---
[第09回 まっちゃ445勉強会](http://matcha445.techtalk.jp/saturday-workshop/9th-workshop)で使用した資料を公開しました。
<!--more-->


  * [milter manager - milterに重みをおいて](/archives/matcha445-9/)

今回は、Ruby関連で話すときのように自由な感じで話したのですが、楽しんでもらえた方もいたようでよかったです。

### 内容

話した内容はmilter managerのことというより、milter managerがベースとしているmilterのことです。milter managerについては、最後に少し触れた程度です。

プログラムを見てもらえればわかりますが、送信側の対策、経路情報を利用する対策、メッセージの内容を利用する対策と網羅的な内容でした。そのなかで、手法そのものではなく、これらの手法を活用するためのツールであるmilter managerは異色と言えます[^0]。

milter managerと同じくmilterも手法そのものではありません。milterは手法を実現するための仕組みの1つです。セッションの順番が経路情報を利用する対策のセッションとメッセージの内容を利用する対策のセッションの間という切り替えのタイミング（で、さらにおやつの後）ということもあり、[umqさんのイントロダクション](http://slashdot.jp/~umq/journal/486282)の一部をもう少し詳しく取り上げて、勉強会の話題に上がっている手法をmilterを使って実現できるよ、という流れになっています。全体を再確認しつつ、適度な中休みになったのならmilter managerセッションの役割は果たせたかと思います。

### 感想

個別ではなく全体的に見てですが、みんな楽しそうでいいなぁ、集まれてよかったなぁ、というのが率直なところです。また、同じような機会ができればいいなぁと思いますので、実現したときはまたよろしくお願いします。

### おまけ

勉強会で使ったプレゼンテーションツールRabbitの機能を紹介しておきます。

  * PDFにうさぎとかめ
  * Rabbit Hole

#### PDFにうさぎとかめ

[さとうさん](http://d.hatena.ne.jp/stealthinu/)の資料はOpenOffice.orgのImpressで作成してPDF化されていました。少し時間がオーバーしそうということだったので、Rabbitの「うさぎとかめタイマー」をおすすめして実際に使ってもらいました。

RabbitはPDFレンダリング機能もあるので、他のプレゼンテーションツールで資料を作成しPDFに出力すれば、Rabbitで表示することができます。Rabbitは読み込んだPDFのスライドの上に「うさぎとかめタイマー」を表示するので、PDF側には特に何もする必要はありません。

「うさぎとかめタイマー」が何かについては、ぜひ自分の目で確認してください。

#### Rabbit Hole

SMTPのやりとりのデモをするときにスライドの真ん中を開けて、後ろにあるターミナル上でtelnet（と[Mutt](https://ja.wikipedia.org/wiki/Mutt)[^1]）を使いました。この穴のことを「Rabbit Hole」と呼んでいます。名前の由来はもちろん[不思議の国のアリス](http://www.genpaku.org/alice01/alice01j.html)です。

ちなみに、Rabbitには[アリス画像](http://www.cozmixng.org/repos/rabbit/trunk/data/rabbit/image/rabbit-images/mini-alice.png)も用意されているので、アリスとうさぎを追いかけさせることもできます。

[^0]: そのためあまりmilter managerを前に出しすぎない内容になっています。自由な感じで話せたのはそのおかげだったりします。:-)

[^1]: Muttが好きなら[Sup](http://sup.rubyforge.org/)も気に入るかもしれません。
