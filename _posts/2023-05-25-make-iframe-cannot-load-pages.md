---
title: 'ウェブサイトのクリックジャッキング攻撃対策: iframeによる読み込みを防止する方法'
author: daipom
tags:
  - security
---

こんにちは。普段使っているウェブサイトやメールなどの仕組みをもっと分かるようになりたい福田です。

このたび、会社のセキュリティ対策の一環として、`iframe`要素からこのウェブサイトを読み込むことができないようにしました。
私はこういった内容に詳しくないのですが、良い勉強の機会だと思って詳しい人に教えてもらいながらやってみました。

このウェブサイトの公開にはApacheを使っているので、`.htaccess`ファイルを使って設定を行いました。

この記事では、私のようにこういった内容に興味はあるけれど詳しくない、という人向けに今回行った内容を紹介します。

<!--more-->

### `iframe`を使ってみる

クリックジャッキング攻撃をイメージするためにも、まずは`iframe`を使ってみましょう。

次のような`html`ファイルを作成します。

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
</head>
<body>
    <iframe src="https://www.clear-code.com"></iframe>
</body>
</html>
```

`body`以外はスニペットです。適当にhtmlの体裁を整えればいいです。

`body`に`iframe`を足しています。
`src="https://www.clear-code.com"`とすることで、`iframe`にこのウェブサイトを読み込ませています。

このファイルをブラウザーで開くと、動作を確認することができます。
既定のアプリで開くコマンドが便利です。

Ubuntu

```console
$ xdg-open hoge.html
```

Windows PowerShell

```console
// iiコマンドはInvoke-Itemコマンドの標準のエイリアスです
$ ii hoge.html
```

macOS

```console
$ open hoge.html
```

ブラウザーで開くと、この対応をする以前では、次のようにクリアコードのウェブサイトが`iframe`の中で読み込まれていました。

<div style="border: solid;">
  <img src="{% link /images/blog/make-iframe-cannot-load-pages/iframe.png %}" alt="iframe" title="iframe">
</div>

### クリックジャッキング攻撃とは

このように`iframe`で読み込むことができる場合は、クリックジャッキング攻撃に悪用される可能性があります。
クリックジャッキング攻撃については、次のウェブサイトの説明が分かりやすいです。

* [独立行政法人情報処理推進機構 安全なウェブサイトの作り方 - 1.9 クリックジャッキング](https://www.ipa.go.jp/security/vuln/websecurity-HTML-1_9.html)

今回のケースでは、悪意のある人が作った悪意のあるウェブページ上に`iframe`を使ってクリアコードのウェブサイトを埋め込んで悪用する、という可能性があります。

クリアコードのウェブサイトではクリックジャッキングをされて困るような機能はないとは思いますが、念の為に対策をすることになったわけです。

### `.htaccess`ファイルを使って`iframe`から読み込めなくする

`iframe`から読み込めなくするには、HTTPレスポンスヘッダーに`X-Frame-Options: DENY`を出力します。

このウェブサイトの公開にはApacheを使っているので、方法は2つあります。

* A: Apacheの設定を変更する
  * メリット: パフォーマンスへの影響がBと比べて少ない
  * デメリット: 管理者権限が必要
* B: `.htaccess`ファイルを使う
  * メリット: 一般ユーザー権限でできる
  * デメリット: パフォーマンスへの影響がAと比べて大きい

今回は、私が管理者権限を持っていないので、Bの方法で行います。

Apacheでは`.htaccess`ファイルを使うことで、様々な設定を各ディレクトリー単位で反映させることができます。
今回はウェブサイト全体に適用する必要があるので、トップレベルの`.htaccess`を編集します。

トップレベルの`.htaccess`に次の行を追加しました。

```
Header set X-Frame-Options DENY
```

実際の修正コミットはこちらです。

* https://gitlab.com/clear-code/website/-/commit/fa28a0677c4c47bae7e0f1425cf6b655eeb15141

この修正がデプロイされた後、再度冒頭の方法で`iframe`から読み込もうとしてみました[^deploy]。
すると、次のように読み込むことができなくなったことを確認できました。

<div style="border: solid;">
  <img src="{% link /images/blog/make-iframe-cannot-load-pages/iframe-restricted.png %}" alt="iframe-restricted" title="iframe-restricted">
</div>

以上で対応完了です。

[^deploy]: [jekyll](http://jekyllrb-ja.github.io/)を使ってローカル環境でウェブサイトをジェネレートできますが、それではApacheが動作しません。そのため、`jekyll`を使ってローカルで`.htaccess`の動作を確認することはできません。

### まとめ

そういえば、ククログでもスライドを記事に埋め込むのに`iframe`を使うことがあります。
`iframe`自体は便利な機能なのでしょうが、他のウェブサイトに埋め込まれることを想定していないウェブサイトでは、
念の為にこの対応をしておくのが良さそうです。

今回私は、`iframe`の簡単な使い方や、Apacheの`.htaccess`がこういうケースで使えることを学ぶことができました。
こういう問題に気がついたら、さっと修正できるようになっていきたいです。

クリアコードではこのように知見や成果を公開することを重視しています。
クリアコードで働くことに興味を持たれた方は、[クリアコードの採用情報]({% link recruitment/index.md %})をぜひご覧ください。
