---
title: 'July Tech Festa 2021 winter：PostgreSQLで高速・高機能な日本語全文検索 #JTF2021w'
author: komainu8
tags:
- presentation
---

今週の日曜日 1/24(日) に開催される [July Tech Festa 2021 winter](https://techfesta.connpass.com/event/193966/)に「PostgreSQLで高速・高機能な日本語全文検索」という題名で、PGroongaの紹介をします。

<!--more-->
<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/komainu8/july-tech-festa-2021-winter/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/komainu8/july-tech-festa-2021-winter/" title="PostgreSQLで高速・高機能な日本語全文検索">PostgreSQLで高速・高機能な日本語全文検索</a>
  </div>
</div>

関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/komainu8/july-tech-festa-2021-winter/)

  * [リポジトリー](https://github.com/komainu8/rabbit-slide-komainu8-july-tech-festa-2021-winter)

### 内容

PostgreSQLで日本語の全文検索をするには、LIKE演算子を使って実現できますが、インデックスを使用しないため、データ量が多い場合には低速です。英文については、インデックスを使用した全文検索が可能ですが、日本語には対応していません。PGroonga(ぴーじーるんが)は、全言語対応の高速な全文検索機能を PostgreSQL で使えるようにする拡張で、 安定して高速で、かつ高機能（同義語、表記ゆれや異字体への対応、類似文書検索などが使えます）です。この発表では、PGroongaの速さや便利さを紹介します。

### まとめ

PostgreSQLで日本語の全文検索機能の実装を検討している人や、日本語の全文検索に興味がある人向けの内容にしています。
あまり全文検索に詳しくないけど、興味があるという方は、是非参加してみてください！
