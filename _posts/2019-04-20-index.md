---
tags:
- ruby
- presentation
title: 'RubyKaigi 2019 - Better CSV processing with Ruby 2.6 #rubykaigi'
---
[RubyKaigi 2019](https://rubykaigi.org/2019)の2日目に[Better CSV processing with Ruby 2.6](https://rubykaigi.org/2019/presentations/ktou.html)という話をした須藤です。今年もクリアコードは[シルバースポンサー](https://rubykaigi.org/2019/sponsors#sponsor-24)としてRubyKaigiを応援しました。
<!--more-->


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2019/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2019/" title="Better CSV processing with Ruby 2.6">Better CSV processing with Ruby 2.6</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/rubykaigi-2019/)

  * [スライド（SlideShare）](https://slideshare.net/kou/rubykaigi-2019csv)

  * [リポジトリー](https://gitlab.com/ktou/rabbit-slide-kou-rubykaigi-2019)

### 内容

この1年、 @284km と一緒に[csv](https://github.com/ruby/csv)を改良していたのでその成果を自慢しました。せっかく2人で話をするので、時間を分割してそれぞれ話をするのではなく、ずっと掛け合いをしながら2人で話をしました。楽しんでもらえたでしょうか？

Ruby 2.6.3に入っているcsvはどんなケースでもRuby 2.5のcsvより高速になっています。この成果を使うためにぜひ最新のRubyにアップグレードしてください。

最後にも少し触れましたが、まだまだcsvやその周辺に改良すべきことがあります。今回の話でcsvの開発に興味がでてきた人は一緒に改良してきましょう。その気になった人は[Red Data ToolsのGitter](https://gitter.im/red-data-tools/ja)に書き込んでください。どうやって進めるか相談しましょう。

RubyKaigi 2019でまわりの人たちと相談してstrscanのメンテナンスを引き取ることにしたのでそのあたりの改良もできます。

### RubyData Workshop

発表の後、[RubyData Workshop](https://rubykaigi.org/2019/presentations/mrkn_workshop.html)でもRed Data Toolsの開発に参加する人を募りました。すでに数人[Red Data ToolsのGitter](https://gitter.im/red-data-tools/ja)に書き込んでいる人もいます。やったね！

### まとめ

RubyKaigi 2019で最近のcsvの開発の成果を自慢しました。今回は2人での発表だったのでやいのやいのした発表をしました。

Red Data Toolsの仲間が増えたのでよいRubyKaigiでした。
