---
tags:
- fluentd
title: Fluent Bit入門 - 軽量高速なログ収集ツール
author: fujimotos
---

![Fluent Bit Logo]({% link /images/blog/20210518_0.png %})[^1]

Fluent Bitとは軽量高速なログ収集ツールです。ElasticsearchやApache Kafkaなどの様々なサービスにログデータを転送できます。

* [プロジェクト公式サイト](https://fluentbit.io)
* [GitHubレポジトリ](https://github.com/fluent/fluent-bit)

全体がCで実装されているのが特徴で、Rubyベースの[Fluentd](https://www.fluentd.org/)の兄弟プロジェクトという位置づけです。

この記事では、Fluent Bitに関する基本的な質問に答えて、具体的に使い始めるための最初のステップについて解説します。

<!--more-->

[^1]: ["Fluent Bit Logo"](https://github.com/fluent/fluent-bit/blob/master/fluentbit_logo.png) by The Fluent Bit Authors, used under [Apache License v2.0](https://github.com/fluent/fluent-bit/blob/master/LICENSE).

### Fluent Bitのよくある質問 (FAQ)

#### [質問1] Fluent Bitはどこで使われていますか？

現在はKubernetesのようなコンテナ環境で広く利用されています。

* 利用例1: [AWS FireLens](https://aws.amazon.com/about-aws/whats-new/2019/11/aws-launches-firelens-log-router-for-amazon-ecs-and-aws-fargate/)
* 利用例2: [Google Cloud Platform Ops Agent](https://github.com/GoogleCloudPlatform/ops-agent)

いずれもクラウドプラットフォームが提供しているログの収集・解析サービスですが、転送エージェントにFluent Bitが採用されています。

また、その他の主な導入企業については、[ロゴの一覧がFluent Bitの公式サイトに掲載されています。](https://github.com/fluent/fluent-bit#fluent-bit-in-production)

#### [質問2] 誰が開発していますか？

Fluent Bit自体は[Cloud Native Computing Foundation](https://www.cncf.io/)の傘下プロジェクトで、様々なベンダーが開発に参加しています。

 * [Fluent Bitプロジェクトのメンテナ一覧](https://github.com/fluent/fluent-bit/blob/master/MAINTAINERS.md)
 * [GitHubのコントリビューターリスト](https://github.com/fluent/fluent-bit/graphs/contributors)

クリアコード社員の藤本（この記事の筆者）もメンテナの一人です。開発者は欧米からアジアまで世界的に分散していて、日本だと、2016年から開発に参加されている開発者として [@nokute78](https://github.com/nokute78) さんがいらっしゃいます。

#### [質問3] いつから開発されているプロジェクトですか？

Gitの履歴をたどると、[2014年から開発が始まっています。](https://github.com/fluent/fluent-bit/commit/b327996bf)本格的に開発がスタートしたのは2015年です。

 * 2018年にv1.0.0がリリースされ、本記事を執筆している2021年5月時点ではv1.7.5が最新版です。
 * 現在は、概ね3-6ヶ月に一度、新しい機能やプラグインを追加したバージョン更新がリリースされています。

#### [質問4] Fluent Bitを使うメリットは何ですか？

Fluent Bitを使うメリットとしては以下があります。

 * **システムフットプリントが小さい**
    * Fluent Bitは静的にリンクされたバイナリなので、実行ファイル一つを配置すれば転送を始められます。
    * 対して、FluentdはRuby本体を含めた一連のセットアップがどうしても必要となり、この手軽さの点でFluent Bitに分があります。
 * **データ処理が高速**
    * 全体がCで記述されているので、大量のデータ処理についてはFluent Bitに速度メリットがあります。
    * この点についは[弊社の過去記事に計測結果があります。]({{ "/services/fluent-bit.html" | relative_url }})
 * **その他**
    * SQLライクな言語で動的にログを操作する[ストリームプロセッシング](https://docs.fluentbit.io/manual/stream-processing/getting-started/)という機能があります。
    * 組み込み環境向けに[Yoctoイメージが提供されています。](https://docs.fluentbit.io/manual/installation/yocto-embedded-linux)

#### [質問5] どうやったら使い始められますか？

各種のディストリビューション向けのパッケージが用意されています。具体的にはドキュメントを参照ください。

* [Linux向けのインストールパッケージ](https://docs.fluentbit.io/manual/installation/linux)
* [Windows向けのインストールパッケージ](https://docs.fluentbit.io/manual/installation/windows)

なお、システムインストールする目的でなければ、自分でビルドすることも（比較的簡単に）可能です。詳細は次の節を参照ください。

### Fluent Bitをソースコードからビルドする

開発者が「インストール前に、ちょっと触って感覚をつかんでみたい」という場合は、ソースからコンパイルしてみることをおすすめします。
手順は特に難しくはなく、Linuxの動いている環境があれば、10分ほどでセットアップからビルドまで完了させられます。

#### Ubuntu/Debianの場合

まずはビルドに必要なツールをインストールします。

```console
$ sudo apt install cmake git build-essentials flex bison
```

Fluent Bitをクローンして、最新の安定版をチェックアウトします。

```console
$ git clone https://github.com/fluent/fluent-bit
$ cd fluent-bit/build
$ git checkout v1.7.5
```

CMakeでMakefileを生成して、makeします。

```console
$ cmake ..
$ make
```

マシンの性能によりますが、ビルドは1-2分で終わります。この記事作成時の実測だと、AzureのB1msインスタンス（最も安価なクラスのインスタンス）で、2分19秒で完了しました。

```
real    2m19.578s
user    1m57.200s
sys     0m18.846s
```

`./bin/`配下にFluent Bitのバイナリが生成されていますので、次のコマンドで動作を確認下さい。

```console
$ ./bin/fluent-bit -i dummy -o stdout
...
[0] dummy.0: [1621305552.596679813, {"message"=>"dummy"}]
[1] dummy.0: [1621305553.596759306, {"message"=>"dummy"}]
```

#### CentOS/RHELの場合

最初のステップでインストールするパッケージが異なる他は、基本的にUbuntu/Debianと同じです。

以下の手順はCentOS 8.2で動作を確認しています。まずはビルドに必要なツールをインストールします。

```console
$ sudo dnf install cmake git flex bison
$ sudo dnf groupinstall "Devlopment Tools"
```

Fluent Bitをクローンして、最新の安定版をチェックアウトします。

```console
$ git clone https://github.com/fluent/fluent-bit
$ cd fluent-bit/build
$ git checkout v1.7.5
```

CMakeでMakefileを生成して、makeします。

```console
$ cmake ..
$ make
```

ビルドはAzureのB1msインスタンスで、1分58秒で完了しました。

```
real    1m58.384s
user    1m33.585s
sys     0m20.255s
```

`./bin/`配下にFluent Bitのバイナリが生成されていますので、次のコマンドで動作を確認下さい。

```console
$ ./bin/fluent-bit -i dummy -o stdout
...
[0] dummy.0: [1621307478.470085499, {"message"=>"dummy"}]
[1] dummy.0: [1621307479.470049466, {"message"=>"dummy"}]
```

### 次のステップ

この記事では、Fluent Bitの概要を説明しました。Fluent Bitについてもっと知りたい方は、以下のリンクを参照ください。

 * コンテナ環境での利用（とくになぜFluent Bitがk8s環境で有用なのか）については、CNCFの[「Introduction to fluentbit」](https://www.youtube.com/watch?v=3ELc1helke4)に良い解説があります。

 * 開発に関するノウハウについては[公式ドキュメントの開発者向け記事](https://docs.fluentbit.io/manual/development/developer-guide)に有用な情報があります。

 * また、クリアコードでは[Fluentd/Fluent Bitのサポートサービス]({{ "/services/fluentd.html" | relative_url }})を提供しています。また、[Fluentd/Fluent Bitに携わりたい開発者も募集しています]({{ "/recruitment/" | relative_url }})。こちらの[お問い合わせフォーム]({{ "/contact/" | relative_url }})からご連絡ください。
