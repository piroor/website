---
tags: 
- embedded
title: Mac OS XのCocoa版GTK+で日本語入力を行うためのgtkimmodule（GtkIMCocoa）の開発
---
Mac OS X[^0]には、Cocoaと呼ばれるネイティブアプリケーション構築のためのAPIがあります。
<!--more-->


通常のアプリケーションはこのCocoaを使って実装されていますが、その他にUnix系OSではメジャーなGUIツールキットであるGTK+を使うこともできます。GTK+が[Mac OS X向けに移植されている](https://www.gtk.org/docs/installations/macos/)ので、GTK+で実装されたアプリケーションをMac OS Xでも使うことができるのです。

ただし、現在のMac OS X版GTK+には、日本語入力の点で課題があります。

そこで、Mac OS X版GTK+で日本語入力を行う上での問題点と、その解決のために新たに開発中のGTK+ immodule（GtkIMCocoa）について紹介します。

### GTK+における日本語入力の仕組み

GTK+におけるテキスト入力の機能は、immodule（input method module。インプットメソッドモジュール。）と呼ばれるプラグインとして実装するようになっています。

プラグイン構造でインプットメソッドのモジュールを個別に追加できる仕組みにすることで、多種多様なインプットメソッドを動的に切り替えて利用できるようになっています。実際に、GTK+アプリケーションのテキスト入力画面で右クリックすると、[入力メソッド]メニューからインプットメソッドを簡単に切り替えることができます。

なお、通常immoduleはGTK+とインプットメソッドの仲介をするものなので、事前にインプットメソッドのインストールが必要です。例えば、im-ximであればXIMサーバーと呼ばれるソフトウェアをあらかじめインストールする必要があります。

以下の図はアプリケーションとインプットメソッドの関係を表しています。

![アプリケーションとインプットメソッド]({{ "/images/blog/20130314_0.png" | relative_url }} "アプリケーションとインプットメソッド")

### Mac OS XにおけるGTK+の日本語入力事情

Mac OS X上で動くGTK+の日本語入力の現状について、まずはMac OS X特有の事情から説明します。

Mac OS XではGTK+のバイナリーが2種類あります。

  * X Window Systemに依存したGTK+
  * Cocoaにのみ依存したGTK+

X Window System版GTK+の場合、アプリケーションでの日本語入力は、前述のようにXIMと呼ばれるインプットメソッドをGTK+から利用することで実現しています。

一方、Cocoa版GTK+では対応するimmoduleがないため、日本語入力を行うことができません。

そのため、日本語入力をまともに使うには、X Window System版GTK+を使うしか選択肢はありませんでした。

しかし、この制約もMac OS X Mountain Lionがリリースされて以降、さらに厳しいものになりました。

なぜなら、Mountain Lionでは、そもそもAppleによるX Window Systemの実装であるX11.appがインストールされないようになってしまったためです。それだけではなく、X11.appがインストールされていた過去のバージョンを使用していても、Mountain LionにアップグレードするとX11.appが削除されてしまいます[^1]。もともとX Window SystemだけではなくXIMサーバーもインストールする必要があったことを考えると、簡単に日本語入力の環境を整えられるとはとても言えない状況です。

このような状況下で今後Cocoa版GTK+を使ったアプリケーションが増えてきたとき、日本語入力が使えないというのは致命的です。

しかし、Cocoa版GTK+でも日本語入力ができるようになれば、非標準となってしまったX Window Systemのパッケージをわざわざインストールする手間もなくなりますし、完成度が向上してくれば、これまでMac OS X版を提供していなかったGTK+アプリケーションでも、パッケージが提供されるようになるかもしれません[^2]。

### Mac OS X版GTK+ immoduleの開発状況

前述のように、現在のGTK+のソースコードには、Mac OS X用のimmoduleは含まれていません。一方、Sylpheedの開発者である山本博之氏による試験的な実装は存在していました（以下、im-quartz）。

  * [GTK+ immodule for Mac OS X を試作してみた](http://sylpheed.sraoss.jp/diary/?date=20110328)
  * [GTK+ 2.24.0 に対するパッチ](http://sylpheed.sraoss.jp/sylpheed/macosx/gtk+-2.24.0-macosx-textinputclient_ja-test1.patch)

山本氏自身が上記記事内で指摘されておられるように、この実装は設計上、大きな問題を抱えています。そのため、このままではGTK+本体にマージされるものにはならないだろう、という懸念がありました。

最大の懸念点は、テキスト処理がimmodule内で完結していないという点です。GTK+の設計思想では、テキスト入力の処理はimmodule内で完結しているべきなのですが、im-quartzでは、テキスト入力処理の大半が描画レイヤーであるGDKの側で実装されています。そのため、せっかくのモジュール構造であるのにも関わらず、GDKに対するパッチも必要となってしまいます。ただし、これはあえてそういう設計にしているというわけではなく、Cocoaのテキスト入力APIがビューのAPIと癒着していることから、そうせざるを得なかった、というのが真相のようです。

### im-quartzに対する改善アプローチ

Mac OS X上でGTK+の設計思想に沿った形のimmoduleを実装するにはどうすればよいのかを検討しました。その検討過程には紆余曲折があったのですが、最終的に以下のような設計にするとうまくいくのではないか、という結論に至りました。

  * immoduleは、テキスト入力専用に独自のNSView（CocoaネイティブのViewオブジェクト）をもつ。
  * immoduleは、受け取ったキーイベントを上記の独自NSViewにフォーワードし、独自NSViewでのテキスト処理結果を受け取ってGTK+のウィジェットに返す。
  * 独自NSViewはユーザーの目に触れる形で表示されることはなく、裏方で上記処理を行う。

### 本アプローチによる成果

上記の改善方針に基づいて、新しいimmodule「GtkIMCocoa」の開発を開始しました。ソースコードは[GitHub](https://github.com/ashie/gtkimcocoa)にて公開しています。

基本的な機能は既に完了しており、この改善アプローチでもテキスト入力処理が正しく機能することを確認しています。

![Mac OS X版GTK+で日本語を入力している様子]({{ "/images/blog/20130314_1.png" | relative_url }} "Mac OS X版GTK+で日本語を入力している様子")

また、当初の目論見どおりテキスト入力処理をimmodule内で完結させることができるようになったため、GTK+本体に対するパッチが不要となりました。

このことにより、以下のようなメリットが得られます。

  * 異なる複数のレイヤーに分断されていたテキスト入力処理の実装が統合され、コードの見通しがよくなった。
  * テキスト入力を必要とするビューにのみテキスト入力処理が実装されるようになった。
  * 単独でのパッケージ配布が可能となり、既にビルド済みのGTK+に対して多言語入力機能を追加することも可能となった。

現時点のGtkIMCocoaは、バージョン0.0.0として以下で公開しています。

  * [GtkIMCocoa 0.0.0](http://sourceforge.jp/projects/imime/releases/?package_id=13602)

最終的にはGTK+本体に取り込まれるのが理想ですが、それまでの間は上記サイトにて定期的にパッケージを公開していく予定です。

### GtkIMCocoaの導入方法について

GtkIMCocoaのインストール方法について解説します。

#### GTK+ 3のインストール

まず、使用するGTK+について説明します。

現在のGtkIMCocoaは、GTK+ 3に対してのみ動作を確認しています。[GTK+](http://www.gtk.org/download/macos.php)のサイトではMac OS X版のバイナリも配布されていますが、im-quartz（あるいは後述する改良版）を試したい場合はGTK+側にも手を入れる必要があるので、ここでは[jhbuild](https://live.gnome.org/Jhbuild)を使用してソースコードからGTK+ 3をビルドする方法を紹介します。

ここで対象とするMac OS Xのバージョンは、Mountain Lionのみです。他のバージョンのMac OS Xで使用したい場合は、[Building GTK-OSX](https://live.gnome.org/GTK%2B/OSX/Building)を参照してください。

##### 1. 開発環境の準備

  * Xcodeをインストールします。Mountain Lionの場合は、App Storeから無料でインストールすることができます。
  * Xcodeのメニューから「Xcode」→「Preferences...」で設定画面を開き、「Downloads」→「Components」画面で「Command Line Tools」をインストールします。
  * MacPortsを使用している場合は、PATHからMacPortsのパスを外してください。MacPortsの一部のコマンドを使用すると、ビルドに失敗することがあるようです。

##### 2. gtk-osx-build-setup.shの入手および実行

jhbuildをMac OS X用にセットアップするためのスクリプトgtk-osx-build-setup.shを入手します。

{% raw %}
```
$ curl -s -O https://git.gnome.org/browse/gtk-osx/plain/gtk-osx-build-setup.sh
```
{% endraw %}

このスクリプトをエディタで開き、下記の行

{% raw %}
```
BASEURL="http://git.gnome.org/browse/gtk-osx/plain/"
```
{% endraw %}

の`"http"`を`"https"`に変更します。

{% raw %}
```
BASEURL="https://git.gnome.org/browse/gtk-osx/plain/"
```
{% endraw %}

変更を保存し、スクリプトを実行します。

{% raw %}
```
$ sh gtk-osx-build-setup.sh
```
{% endraw %}

##### 3. jhbuild shell起動

適切な環境変数をセットするために、jhbuildのシェルを起動します。

{% raw %}
```
$ ~/.local/bin/jhbuild shell
```
{% endraw %}

##### 4. GTK+ 3のビルド

以下のコマンドでGTK+ 3をビルドできます。

{% raw %}
```
$ ~/.local/bin/jhbuild bootstrap
$ ~/.local/bin/jhbuild build meta-gtk-osx-bootstrap
$ ~/.local/bin/jhbuild build meta-gtk-osx-gtk3
```
{% endraw %}

デフォルトの設定のまま実行した場合、GTK+は`~/gtk/inst`以下にインストールされます。

##### 5. 動作確認

gtk3-demoを起動して、GTK+ 3が正しくインストールされたことを確認します。

{% raw %}
```
$ gtk3-demo
```
{% endraw %}

#### GtkIMCocoaのインストール

次にGtkIMCocoaのインストール方法を説明します。

##### 1. ソースコードの入手

GitHubからGtkIMCocoaのソースコードをcloneします。

{% raw %}
```
$ git clone git://github.com/ashie/gtkimcocoa.git
```
{% endraw %}

あるいは、[リリース版のGtkIMCocoaソースパッケージ](http://sourceforge.jp/projects/imime/releases/?package_id=13602)をダウンロードし、 以下のコマンドで任意のディレクトリにソースパッケージを展開します。

{% raw %}
```
$ tar xvzf gtkimcocoa-0.0.0.tar.gz
```
{% endraw %}

##### 2. GtkIMCocoaのビルド

jhbuildシェル上で下記コマンドを実行して、GtkIMCocoaをビルドします。

{% raw %}
```
$ cd gtkimcocoa
$ ./autogen.sh
$ ./configure
$ make
$ make install
```
{% endraw %}

##### 3. immodules.cacheの更新

以下のコマンドで、GTK+ 3のimmoduleの設定を更新します。

{% raw %}
```
$ gtk-query-immodules-3.0 > ~/gtk/inst/lib/gtk-3.0/3.0.0/immodules.cache
```
{% endraw %}

##### 4. 動作確認

gtk3-demoを起動し、「Text Widget」→「Hypertext」などを開いて日本語入力できることを確認します。

{% raw %}
```
$ gtk3-demo
```
{% endraw %}

### 今後の課題

実は、GtkIMCocoaの開発開始と同時期に、uimプロジェクト等で知られるek.kato氏もim-quartzの改善パッチを作成されていたことが後から発覚しました。

  * [Bug 694273 - Patch to support NSTextInputClient in text widgets](https://bugzilla.gnome.org/show_bug.cgi?id=694273)

GtkIMCocoaではうまく動作していない日本語入力システム（Google日本語入力）が、ek.kato版ではうまく動作しているなど、安定性の面ではこちらの方が進んでいるようです。一方で、基本設計の問題はオリジナルのim-quartzをそのまま受け継いでしまっているようです。このため、両実装の統合が必要になるでしょう。

また、日本語だけにとらわれず、あらゆるインプットメソッドとの組み合わせての検証を行い、完成度を上げていく必要があります。

### まとめ

Mac OS XのCocoa版GTK+で日本語入力を行う上での問題点とその解決のために新たに開発したGTK+ immodule（GtkIMCocoa）の実装と今後について紹介しました。

[GtkIMCocoa](https://github.com/ashie/gtkimcocoa)はまだ開発を開始したばかりのプロジェクトであり、まだまだ改善すべき余地が残されています。

そこで、Cocoa版GTK+で日本語入力をまともに使えるようにしたいという、一緒に開発してくれる人を募集しています。また、開発を手伝うというのが難しくても、GtkIMCocoaを実際に使ってみて問題があればバグレポートしてくれる人も募集しています。

なお、GtkIMCocoaの開発については先日[このブログで紹介したインターンシップ制度](/blog/2013/2/19.html)の題材としても挙げていますので、挑戦されたい方は是非ともご応募ください。

[^0]:  現在は「OS X」が正式名称のようですが、ここではX Window Systemとの混乱を避けるために「Mac OS X」と表記します。

[^1]: 別途[XQuartz](http://xquartz.macosforge.org/landing/)のインストールが促されます。

[^2]: Unix系OSの老舗メールクライアントである[Sylpheed](http://sylpheed.sraoss.jp/ja/)のMac OS X版が登場することを期待して止みません。
