---
title: Software Design12月号のDebian Hot Topicsでインタビュー記事が掲載されます
tags:
  - debian
---
しばらく前に[Debian Developerになった林](https://bits.debian.org/2020/11/new-developers-2020-10.html)です。
最近は日本からDebian Developerになった人がいないということで、新Debian Developerへのインタビューを受ける機会がありました。
2020年11月18日発売 Software Design12月号のDebian Hot Topicsのコーナーでちょっととりあげてもらっています。
<!--more-->


<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">Software Design12月号は明日発売です！（11月18日）<br>特別付録は、ちょうぜつエンジニアめもりーちゃんステッカーです。表紙はくまみねさんの現場猫さんとコラボ！<a href="https://twitter.com/hashtag/%E7%8C%AB%E9%9B%91%E8%AA%8C%E3%81%98%E3%82%83%E3%81%AA%E3%81%84%E3%81%9E%E3%82%88?src=hash&amp;ref_src=twsrc%5Etfw">#猫雑誌じゃないぞよ</a><br>特集1はDocker、特集2はAWS！<a href="https://t.co/xGcqO5S5fT">https://t.co/xGcqO5S5fT</a> <a href="https://t.co/VwzhFx82du">pic.twitter.com/VwzhFx82du</a></p>&mdash; SoftwareDesign (@gihyosd) <a href="https://twitter.com/gihyosd/status/1328512522031284224?ref_src=twsrc%5Etfw">November 17, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


[Software Design12月号の目次](https://gihyo.jp/magazine/SD/archive/2020/202012)にはそれらしい記述はないですけども、掲載されているはずです。
Debianを使い始めたきっかけとか、Debian Developerになって変わったこととかを語っています。

Debian MaintainerやDebian Developerになるための記事を以前書いたので、あわせて参考にしてみてください。

  * [Debian Maintainerになるには](2018830)

  * [Debian Developerになるには](2020928)

Debianを好きな人は、今週末 11/21(土) にオンラインの[東京エリア・関西合同Debian勉強会](https://debianjp.connpass.com/event/194059/)が開催されるので、ぜひ参加してみてください。
