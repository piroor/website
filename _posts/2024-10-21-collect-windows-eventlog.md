---
title: "いざというときに備えてWindowsのイベントログを収集・保存しておく方法"
author: kenhys
tags:
  - fluentd
---

システム管理者にとって、セキュリティインシデント発生時など、原因究明・問題解決のために必要な情報をすばやく収集・分析する必要があります。

Windowsにおいては、イベントログが監視や監査という観点からは重要なログといえるでしょう。
Windowsイベントログにもいくつか種類があり、セキュリティ監査の観点からはアカウントの
ログオン等の情報が得られるので、Windowsのセキュリティイベントログが保存されていると有用です。[^recommended-policy]

[^recommended-policy]: Windowsのセキュリティイベントログの内容は、グループポリシーの監査ポリシーや、フォルダの監査設定によってもかわってきます。推奨される監査ポリシーは[監査ポリシーの推奨事項](https://learn.microsoft.com/ja-jp/windows-server/identity/ad-ds/plan/security-best-practices/audit-policy-recommendations) を参照してください。


そこで、まだそこまでしくみを整備できていない人のために、今回はいざというときに備えてWindowsのセキュリティイベントログを収集・保存しておく方法を紹介します。

<!--more-->

### Windowsのセキュリティイベントログを収集・保存する前提条件

今回は次の前提をもとに、ログを収集・保存するものとします。

* 日常的にWindowsのセキュリティイベントログを収集・保存したい(インシデント発生後にログを収集するのではない)
* 収集したWindowsのセキュリティイベントログはクラウドサービス(Amazon S3)に保存する

### Windowsのセキュリティイベントログを収集する環境をセットアップする

日常的にログを収集するという観点からは、ログを収集するためのアプリケーションを都度起動するのではなく、
サービスのように常駐するプログラムを利用して随時収集する方式が適しています。

そこで、今回はFluent Packageというソフトウェアを用いてログを収集する例を示します。

Fluent Packageはログ収集のためのソフトウェアである[Fluentd](https://www.fluentd.org)を簡単に使えるようにしたものです。
動作に必要なランタイムやプラグインをまとめてインストールし、バックグラウンドサービスとして動作させることができるようにパッケージ化しています。

Fluent Packageは https://td-agent-package-browser.herokuapp.com/lts/5/windows からインストーラーをダウンロードできます。
本記事執筆時点ではv5.0.4が最新です。

インストール方法の詳細は、[`fluent-package v5`のセクション](https://docs.fluentd.org/installation/install-by-msi#fluent-package-v5)を参照してください。
既定の選択肢を選んでいくだけでインストールは完了します。

### Windowsのセキュリティイベントログをクラウドサービス(Amazon S3)に保存する

すでに述べたように、Windowsの監査目的であれば、Windowsのセキュリティイベントログを保持しておくのが適切です。
セキュリティイベントログには、アカウントへのログオンの可否等が記録されているからです。
また、ファイルアクセスの可否など不審な挙動も把握できます。

Windowsのセキュリティイベントログは既定で出力されるものもありますが、[グループポリシーにて予め用意されている監査ポリシーを適宜選択することで、組織において高度な監査ポリシー設定を構成](https://learn.microsoft.com/ja-jp/defender-for-identity/deploy/configure-windows-event-collection)したり、ファイルやフォルダ単位で監査設定を変更してログを出すようにすることもできます。

個別の監査ポリシーについてまでは言及がありませんが、[監査ポリシーの推奨事項](https://learn.microsoft.com/ja-jp/windows-server/identity/ad-ds/plan/security-best-practices/audit-policy-recommendations)が公開されているので、組織の実情に応じて適宜取捨選択するとよいでしょう。

では、Fluent Packageでどのように実現するとよいでしょうか。
収集対象はWindowsのセキュリティイベントログとし、ローカルに保存するのではなく、クラウドサービスに
アーカイブを保存する設定をしてみましょう。

次の内容のファイルを`c:\opt\fluent\etc\fluent\fluentd.conf`として保存します。

```xml
<source>
  @type windows_eventlog2
  @id windows_eventlog2
  channels security
  tag eventlog.security
  parse_description
  <storage>
    path c:\opt\fluent\eventlog2.pos
  </storage>
</source>

<filter eventlog.security>
  @type record_transformer
  <record>
    hostname "#{Socket.gethostname}"
  </record>
</filter>

<match eventlog.security>>
  @type s3
  aws_key_id YOUR_AWS_KEY_ID
  aws_sec_key YOUR_AWS_SEC_KEY
  s3_bucket YOUR_S3_BUCKET_NAME
  s3_region YOUR_S3_REGION
  path logs/${tag}/%Y/%m/%d/
  <buffer tag,time>
    @type file
    path /opt/fluent/buffer/s3
    timekey 3600 # 1 hour partition
    timekey_wait 10m
    timekey_use_utc true
  </buffer>
</match>
```

`YOUR_AWS_KEY_ID`や、`YOUR_AWS_SEC_KEY`、`YOUR_S3_BUCKET_NAME`は適宜ご利用環境に合わせて、書き換えてください。[^block-public-access]

windows_eventlog2では`parse_description`を指定しておくことで、`Description`と`EventData`を自動的にパースさせることができます。
イベントIDによってパースされたあとのフィールド名が若干変わってくる点に注意してください。

[^block-public-access]: 既定では、[2023年4月28日以降に作成されたAmazon S3バケットは非公開](https://docs.aws.amazon.com/prescriptive-guidance/latest/aws-startup-security-baseline/acct-08.html)となっています。

あらかじめ、Amazon S3のアクセス権限まわりは設定してあるものとします。最低限認証なしでアクセスできないようにします。)

このようにすることで、1時間ごとでgz形式で圧縮されたアーカイブがAmazon S3に保存できるようになります。

![スクリーンショット: Amazon S3バケット]({% link /images/blog/collect-windows-eventlog/s3-bucket-objects.png %})

なお、`YOUR_S3_BUCKET_NAME`を作成する際には、バケットのオブジェクトロックを有効にしておくとよいでしょう。

<div class="callout secondary">

オブジェクトロックを利用するためには、Amazon S3のバケットを作成するときに、バージョニングもあわせて有効にする必要があります。

![スクリーンショット: オブジェクトバージョニング]({% link /images/blog/collect-windows-eventlog/s3-bucket-versioning.png %})

バケットのオブジェクトロックを有効にすることで、不用意なイベントログの削除を防ぐことができます。

![スクリーンショット: オブジェクトロック]({% link /images/blog/collect-windows-eventlog/s3-bucket-objectlock.png %})

各ユーザーへの適切な権限の付与に関しては、[Amazon S3 での IAM の機能](https://docs.aws.amazon.com/ja_jp/AmazonS3/latest/userguide/security_iam_service-with-iam.html)等を参照してください。本記事では詳細を割愛します。[^avvs]

[^avvs]: AWSの権限設定まわりはそれだけでひとつの記事にできるくらいなので、本記事ではそこまで踏み込みません。

</div>

### 実際に収集できるWindowsイベントログの情報

実際にこの設定でどのような情報が収集できるか、次の3つのケースについて紹介します。

* ログオンの場合
* ログオン失敗の場合
* ログオフの場合

#### ログインの場合

ログオン時には次のような情報が取得できます。

```text
2024-10-11T10:38:54+00:00	test.eventlog.security	{
    "ProviderName": "Microsoft-Windows-Security-Auditing",
    "ProviderGUID": "",
    "EventID": "4624",
    "Level": "0",
    "Task": "12544",
    "Opcode": "0",
    "Keywords": "0x8020000000000000",
    "TimeCreated": "2024/10/11 10:38:52.63834300",
    "EventRecordID": "144447",
    "ActivityID": "{C3A87DD4-1BC0-0000-637E-A8C3C01BDB01}",
    "RelatedActivityID": "",
    "ProcessID": "828",
    "ThreadID": "4008",
    "Channel": "Security",
    "Computer": "EC2AMAZ-E2RILE2",
    "UserID": "",
    "Version": "2",
    "DescriptionTitle": "An account was successfully logged on.",
    "subject.security_id": "S-1-5-18",
    "subject.account_name": "EC2AMAZ-E2RILE2$",
    "subject.account_domain": "WORKGROUP",
    "subject.logon_id": "0x3E7",
    "logon_information.logon_type": "7",
    "logon_information.restricted_admin_mode": "-",
    "logon_information.virtual_account": "No",
    "logon_information.elevated_token": "Yes",
    "impersonation_level": "Impersonation",
    "new_logon.security_id": "S-1-5-21-3781849529-2443535876-146867212-500",
    "new_logon.account_name": "Administrator",
    "new_logon.account_domain": "EC2AMAZ-E2RILE2",
    "new_logon.logon_id": "0x92508B",
    "new_logon.linked_logon_id": "0x0",
    "new_logon.network_account_name": "-",
    "new_logon.network_account_domain": "-",
    "new_logon.logon_guid": "{00000000-0000-0000-0000-000000000000}",
    "process_information.process_id": "0x8a4",
    "process_information.process_name": "C:\\Windows\\System32\\svchost.exe",
    "network_information.workstation_name": "EC2AMAZ-E2RILE2",
    "network_information.source_network_address": "******",
    "network_information.source_port": "0",
    "detailed_authentication_information.logon_process": "User32",
    "detailed_authentication_information.authentication_package": "Negotiate",
    "detailed_authentication_information.transited_services": "-",
    "detailed_authentication_information.package_name_(ntlm_only)": "-",
    "detailed_authentication_information.key_length": "0",
    "hostname": "EC2AMAZ-E2RILE2"
}
```

主に次の情報から、Administratorに2024/10/11 10:38:52にログオンがあったことを確認できます。

* "EventID": "4624"
* "TimeCreated": "2024/10/11 10:38:52.63834300"
* "DescriptionTitle": "An account was successfully logged on."
* "new_logon.account_name": "Administrator"

#### ログオン失敗の場合

ログオン失敗時には次のような情報が取得できます。

```text
2024-10-11T10:31:38+00:00	test.eventlog.security	{
    "ProviderName": "Microsoft-Windows-Security-Auditing",
    "ProviderGUID": "",
    "EventID": "4625",
    "Level": "0",
    "Task": "12544",
    "Opcode": "0",
    "Keywords": "0x8010000000000000",
    "TimeCreated": "2024/10/11 10:31:37.508171900",
    "EventRecordID": "144439",
    "ActivityID": "{C3A87DD4-1BC0-0000-637E-A8C3C01BDB01}",
    "RelatedActivityID": "",
    "ProcessID": "828",
    "ThreadID": "5008",
    "Channel": "Security",
    "Computer": "EC2AMAZ-E2RILE2",
    "UserID": "",
    "Version": "0",
    "DescriptionTitle": "An account failed to log on.",
    "subject.security_id": "S-1-0-0",
    "subject.account_name": "-",
    "subject.account_domain": "-",
    "subject.logon_id": "0x0",
    "logon_type": "3",
    "account_for_which_logon_failed.security_id": "S-1-0-0",
    "account_for_which_logon_failed.account_name": "Administrator",
    "failure_information.failure_reason": "Unknown user name or bad password.",
    "failure_information.status": "0xC000006D",
    "failure_information.sub_status": "0xC000006A",
    "process_information.caller_process_id": "0x0",
    "process_information.caller_process_name": "-",
    "network_information.workstation_name": "******",
    "network_information.source_network_address": "******",
    "network_information.source_port": "0",
    "detailed_authentication_information.logon_process": "NtLmSsp",
    "detailed_authentication_information.authentication_package": "NTLM",
    "detailed_authentication_information.transited_services": "-",
    "detailed_authentication_information.package_name_(ntlm_only)": "-",
    "detailed_authentication_information.key_length": "0",
    "hostname": "EC2AMAZ-E2RILE2"
}
```

主に次の情報から、Administratorに2024/10/11 10:31:37にパスワード不一致によるログオン失敗があったことを確認できます。

* "EventID": "4625"
* "TimeCreated": "2024/10/11 10:31:37.508171900"
* "DescriptionTitle": "An account failed to log on."
* "account_for_which_logon_failed.account_name": "Administrator"
* "failure_information.failure_reason": "Unknown user name or bad password."

#### ログオフの場合

ログオフ時には次のような情報が取得できます。

```text
2024-10-11T10:22:42+00:00	test.eventlog.security	{
    "ProviderName": "Microsoft-Windows-Security-Auditing",
    "ProviderGUID": "",
    "EventID": "4634",
    "Level": "0",
    "Task": "12545",
    "Opcode": "0",
    "Keywords": "0x8020000000000000",
    "TimeCreated": "2024/10/11 10:22:41.111590900",
    "EventRecordID": "144438",
    "ActivityID": "",
    "RelatedActivityID": "",
    "ProcessID": "828",
    "ThreadID": "5008",
    "Channel": "Security",
    "Computer": "EC2AMAZ-E2RILE2",
    "UserID": "",
    "Version": "0",
    "DescriptionTitle": "An account was logged off.",
    "subject.security_id": "S-1-5-21-3781849529-2443535876-146867212-500",
    "subject.account_name": "Administrator",
    "subject.account_domain": "EC2AMAZ-E2RILE2",
    "subject.logon_id": "0x87B050",
    "logon_type": "3",
    "hostname": "EC2AMAZ-E2RILE2"
}
```

主に次の情報から、Administratorが2024/10/11 10:22:41にログオフしたことを確認できます。

* "EventID": "4634"
* "TimeCreated": "2024/10/11 10:22:41.111590900"
* "DescriptionTitle": "An account was logged off."
* "subject.account_name": "Administrator"

### おわりに

今回はいざというときにそなえてWindowsのセキュリティイベントログを収集・保存しておく方法を紹介しました。
どのアカウントがいつログオン/ログオフしたのかや、不審なログオン失敗などを後で確認することができるようになります。

本記事では定期的に収集・保存できるしくみを確立することでなにかあったときのための備えとなるようなWindowsのセキュリティイベントログの保存のしかたの一例を説明しました。
この記事では言及していませんが、[Fluentdのフィルタ](https://docs.fluentd.org/filter)というしくみをあわせてを導入することで、不必要なログを除いてAWS S3へ保存されるデータを
少なくするということもできます。

保存するしくみが整えられたら、次のステップとしてはAmazon S3にためておいたログを元に実際に問題が発生していないか確認したり・アラートをあげたりするなど、より活用していきたいところですが、これについては別の機会に紹介したいと思います。

クリアコードは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})を行っています。
Fluent Packageのご利用方法についてもサポートいたしますので、詳しくは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})をご覧いただき、[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にお問い合わせください。
