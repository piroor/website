---
tags: []
title: OSSの開発に参加したいあなたへ
---
なかなか文章を書く時間を作れていない須藤です。
<!--more-->


去年の末にgihyo.jpに[OSS（オープンソースソフトウェア）の開発に参加したいあなたへ](http://gihyo.jp/dev/column/newyear/2018/start-oss-development)という記事を書いていて、今年の始めに公開されました。2年ほど前から[OSS Gate](https://oss-gate.github.io/)という「OSSの開発に参加する人を継続的に増やす取り組み」をしているのですが、そこでの知見をまとめました。

まわりにOSSの開発に参加したい人がいたら教えてあげてください。
