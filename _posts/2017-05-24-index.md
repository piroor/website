---
tags:
- fluentd
title: Fluentd v0.14のEventTimeに関する話
---
### はじめに

Fluentd v0.14ではログをmsgpackでエンコードし、新たに時間をForwardプロトコルで送る際に時間をEventTimeへエンコードして送信することができるようになりました。
このエンコード形式を用いて時間をForwardプロトコルで送るようにすると、秒よりもさらに細かな精度でログのやりとりができるようになります。
Fluent Loggerでログを送る際に、一秒間に２つ以上のログが発生する環境で秒精度までのログ転送を行った場合、Fluentdが扱うログの順番が送り先で発生した順ではなくなることがあります。
そのため、ログの順番をより正確に時刻でソートするために考えられたv0.14で新たにログの時刻の形式として秒精度以下も扱えるEventTimeが追加されました。
<!--more-->


### EventTime

Forwardプロトコルのv1の仕様にEventTimeについての解説があります。

仕様を見ると、[EventTimeはmsgpackの拡張型として表される](https://github.com/fluent/fluentd/wiki/Forward-Protocol-Specification-v1#eventtime-ext-format)ことがわかります。
また、fixext8とext8のどちらの場合でも秒とナノ秒は32bit integerで表し、msgpackへエンコードする必要があります。
fixext8形式は実行時に長さを取る必要がないので、構造体を直にEventTimeへエンコードする使い方をすると実行時のことを考えなくてもよくなります。

[fruently](https://github.com/cosmo0920/fruently)では[fixext8を使ったEventTime対応](https://github.com/cosmo0920/fruently/blob/1bc727b9cf9ba411bc2473eccd0a1415e1385282/src/event_time.rs#L31-L37)を行いました。

fixext8を用いたEventTime拡張型の場合もext8を用いたEventTime拡張型も秒とナノ秒を表す32bit Integerをエンコードする際にはBig Endianでエンコードする必要があることに注意してください。

#### fixext8

```
+-------+----+----+----+----+----+----+----+----+----+
|     1 |  2 |  3 |  4 |  5 |  6 |  7 |  8 |  9 | 10 |
+-------+----+----+----+----+----+----+----+----+----+
|    D7 | 00 | second from epoch |     nanosecond    |
+-------+----+----+----+----+----+----+----+----+----+
|fixext8|type| 32bits integer BE | 32bits integer BE |
+-------+----+----+----+----+----+----+----+----+----+
```


#### ext8

```
+--------+----+----+----+----+----+----+----+----+----+----+
|      1 |  2 |  3 |  4 |  5 |  6 |  7 |  8 |  9 | 10 | 11 |
+--------+----+----+----+----+----+----+----+----+----+----+
|     C7 | 08 | 00 | second from epoch |     nanosecond    |
+--------+----+----+----+----+----+----+----+----+----+----+
|   ext8 | len|type| 32bits integer BE | 32bits integer BE |
+--------+----+----+----+----+----+----+----+----+----+----+
```


### 例

#### Unix epoch時間(v0.12互換)

EventTimeなしでFluent Loggerを動かす場合は、秒精度までのログしか送ることができません。Unix epochで表現が可能な精度の時刻がログに含まれます。

```
2017-05-17 17:29:57.000000000 +0900 test: {"name":"fruently"}
```


#### EventTime(v0.14から)

EventTime形式にエンコードして時間をログに含めた際には、処理系がサポートする秒以下の精度の時間も含めてログに含めることができます。

```
2017-05-17 17:29:57.587226000 +0900 test: {"name":"fruently"}
```


### まとめ

Fluent LoggerのEventTime対応をする際に参照する必要になったEventTimeの仕様を解説しました。
EventTimeを用いてエンコードされた時刻はこれまでのものよりもさらに精度の高い時刻情報を持てるようになります。
1秒に1つ以上のログをFluent Loggerに送る必要がある際にFluent LoggerがEventTime対応をしているかどうかを調べて、未対応であればバグ報告やプルリクエストをしてみるのはいかがでしょうか。
