---
tags:
- groonga
- presentation
title: 'PGConf.ASIA 2017 - PGroonga 2 – Make PostgreSQL rich full text search system
  backend! #pgconfasia'
---
[PGConf.ASIA 2017](http://www.pgconf.asia/JA/2017/)で[RUM](https://github.com/postgrespro/rum)の存在を知った須藤です。RUMはGINと違って完全転置索引にできるので全文検索用途によさそう。（Groongaは元から完全転置索引にできるのでずっと前からよかった。）
<!--more-->


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/pgconf-asia-2017/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/pgconf-asia-2017/" title="PGroonga 2 – Make PostgreSQL rich full text search system backend!">PGroonga 2 – Make PostgreSQL rich full text search system backend!</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/pgconf-asia-2017/)

  * [スライド（SlideShare）](https://slideshare.net/kou/pgconf-asia-2017)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-pgconf-asia-2017)

### 内容

[PostgreSQL Conference Japan 2017での内容]({% post_url 2017-11-07-index %})にPGroonga 1.0.0からPGroonga 2へのアップグレード関連の話を盛り込んだ内容になっています。なお、PostgreSQL Conference Japan 2017での内容は次の昨日の実現方法を紹介でした。

  * 高速全文検索

  * それっぽい順でのソート

  * 検索結果表示画面で検索キーワードをハイライト

  * 検索結果表示画面で検索キーワード周辺テキストだけを表示

  * オートコンプリート（検索キーワードを少し入力したら補完する機能）

  * 類似文書検索（ブログの検索システムなら関連エントリーの表示に使える機能）

  * 同義語展開（表記揺れの吸収とかに使える機能）

これらの機能の実現方法はPostgreSQL Conference Japan 2017用の資料の方が参考にしやすいかもしれません。PGConf.ASIA 2017用の資料は英語（と日本語訳）でまとめていますが、PostgreSQL Conference Japan 2017用の資料は日本語でまとめているからです。

PGroongaを使うと全文検索システムのバックエンドとしてもPostgreSQLを活用できます。ぜひ活用してください！

### まとめ

PGConf.ASIA 2017で、先日リリースしたPGroonga 2を紹介しました。PGroongaも使ってPostgreSQLをどんどん活用してください！もし、PGroonga関連でなにか相談したいことがある場合は[お問い合わせ](/contact/?type=groonga)ください。
