---
tags:
- ruby
title: Ruby 3.4.0のcsv/fiddle/rexml/stringio/strscan/test-unit
author: kou
---
Rubyの開発に参加している須藤です。そろそろRuby 3.4.0がリリースされるので私がメンテナンスしているdefault gem/bundled gemの変更点を簡単に紹介します。

<!--more-->

### 対象gem

紹介するgemは次の通りです。default gemがRubyに組み込まれているgemで、bundled gemがRubyをインストールするときに普通のgemとしてついでにインストールされるgemです。どちらも新しいバージョンを普通のgemとしてインストールすることで、Ruby本体のバージョンを上げなくても新しいバージョンのgemを使えるようになります。

* csv: bundled gem
* fiddle: default gem
* rexml: bundled gem
* stringio: default gem
* strscan: default gem
* test-unit: bundled gem

### csv

csv gemはCSVを扱うgemです。csv gemの変更点はこんな感じです。

[GH-296](https://github.com/ruby/csv/issues/296)

3つの条件が重なったときにパースに失敗する「ことがある」問題を修正しました。（4つの条件とか言ったほうがいいかもしれないけど、もう1つの条件を説明するのが面倒。。。）

[GH-301](https://github.com/ruby/csv/issues/301)

`CSV.open`がデフォルトでBOMを自動検出するようになりました。ただし！Windowsでは自動検出しません。これは[Ruby本体のWindowsでのBOMの検出機能がなんか変](https://bugs.ruby-lang.org/issues/20526)だからです。気が向いたら直そうかと思っていましたが、普段Windowsを使っていないこともあり全然気が向かなかったのでなにもしていません。だれか直して。

なお、CSVにBOMが入る原因のほぼすべてはExcelなはずです。Excelが出力するCSVにはBOMが入っているはずなんです。なので、Windowsでこそこの機能があった方が便利になるはずなのですが、前述の通り無効になっています。Excelで作られたCSVを違う環境で扱うときはこれで便利になるはずです。

[GH-300](https://github.com/ruby/csv/issues/300)

`CSV.open`が`StringIO`を受け付けるようになりました。普通は文字列か`IO`を渡すと思うので、`StringIO`を受け付ける必要性はあまりないと思うのですが、`IO`を受け付けるなら`StringIO`も受け付けていいよねーということで受け付けるようになりました。

[GH-313](https://github.com/ruby/csv/issues/313)

いくつか組み込みで値を変換する機能（たとえば、`"1"`を`1`にする機能）があるのですが、時刻へ変換する機能が増えました。日付と日付＋時刻（`datetime`って日本語でなんて言うの？）へ変換する機能は前からあったので、時刻を変換する機能もあっていいよねということです。

[GH-319](https://github.com/ruby/csv/issues/319)

`CSV::TSV`クラスが増えました。`CSV`クラスと同じように使えるのですが、デフォルト設定がTSV（tab-separated values）用になっています。つまり、カラムの区切りがタブになっています。

`CSV.open("xxx.tsv", col_sep: "\t")`じゃなくて`CSV::TSV.open("xxx.tsv")`と書けるようになったということです。

[GH-311](https://github.com/ruby/csv/issues/311)
[GH-312](https://github.com/ruby/csv/issues/312)

ちょっと速くなりました。

### fiddle

fiddle gemは拡張ライブラリーを書かなくてもCで書かれた機能を呼び出せるようにするgemです。fiddle gemの変更点はこんな感じです。

[GH-147](https://github.com/ruby/fiddle/issues/147)

JRubyのFiddle実装を取り込みました。今後、Fiddleがdefault gemからbundled gemになるということを見越しての変更です。なお、strscanも同じように数年前にJRuby実装を取り込んでいます。

JRubyにはffi gemの実装が組み込まれているのですが、それの上にFiddleのAPIを実装しています。

[GH-149](https://github.com/ruby/fiddle/issues/149)

いろいろすったもんだありましたが、TruffleRubyのFiddle実装も取り込みました。といっても、既存のTruffleRuby内にあるFiddle実装を取り込んだのではなく、既存のTruffleRuby実装を捨てて、JRuby実装をTruffleRubyでも使えるようにしました。TruffleRubyにもffi gemの実装が組み込まれているので、同じ実装を使い回せました。

[GH-139](https://github.com/ruby/fiddle/issues/139)

Ractorに対応しました。

本当は構造体サポート（[GH-114](https://github.com/ruby/fiddle/issues/114)）も入れたかったのですが、間に合いませんでした。。。だれか実装したい？

### rexml

rexml gemはピュアRubyのXMLパーサーです。rexml gemの変更点はこんな感じです。

大きな変更の1つ目はやはり[@naitoh](https://github.com/naitoh)による高速化もろもろですね！[#RubyKaigi 2024 LTで「Improved REXML XML parsing performance using StringScanner」というタイトルで発表しました。](https://naitoh.hatenablog.com/entry/2024/05/20/232115)も見てね。これを機にメンテナーにもなってもらいました。（[GH-225](https://github.com/ruby/rexml/issues/225)）

大きな変更の2つ目はたくさんの脆弱性の対応ですね。。。原因は大きく2つありました。REXMLは正規表現を多用しているのですが、それに起因するReDoSが1つ。大きなXMLをできるだけ少ないリソースで処理できるようにするためのストリーム処理に起因するDoSがもう1つです。

前者は[@makenowjust](https://github.com/makenowjust)がRuby 3.2以降に入れた改善で発生しなくなっているのですが、Ruby 3.1はまだEOLになっていないので、Ruby 3.1用にREXML側を改良して対応しました。

後者は[@tompng](https://github.com/tompng)が[GH-186](https://github.com/ruby/rexml/pull/186)で抜本的な対策を入れてくれて直りました。なお、@tompngはRubyコミッターになる予定です。（[Misc #20946](https://bugs.ruby-lang.org/issues/20946)）

他にもいろいろあるのですが、今年はこの2つが大きすぎました。

### stringio

stringio gemは文字列を`IO`っぽくつかえるようにするためのgemです。stringio gemの変更点はこんな感じです。

JRuby実装がよりいい感じになりました。JRuby実装がstringio gemに取り込まれてからCRuby用実装と同じテストを使うようになったのですが、いくつかのテストは省略していました。そういうのがちょっとずつ減っていっています。

CRuby実装はちょろちょろっとした改良が入っています。

### strscan

strscan gemは文字列を高速に解析するためのgemです。strscan gemの変更点はこんな感じです。

[GH-72](https://github.com/ruby/strscan/issues/72)

REXMLの高速化はstrscanを使うようにしたことによるものが大きいのですが、@naitohはstrscanの改善も合わせて進めています。これはREXMLを高速にするために`StringScanner#captures`のAPIを変更しています。

@naitohによる改良は他にもありますが省略します。

[GH-113](https://github.com/ruby/strscan/issues/113)

整数値に特化した解析メソッド`StringScanner#scan_integer`を追加しました。`scanner.scan(/[+-]\d+/).to_i`とかでも整数値を解析できましたが、それより速いです。

strscan gemもJRuby実装を取り込んでいるので、JRubyでもこれらの改良を使えます。しかし、TruffleRuby実装は取り込んでいないので、TruffleRubyではこれらの改良は使えません。TruffleRubyでもstrscan gemをインストールできますが、なにもしないgemになっています。私は、それはユーザーがうれしくないんじゃないかと思うので話はしてみたのですが、そっちの方がTruffleRuby的にはうれしいということで今の形になっています。

### test-unit

test-unit gemは単体テスト用のgemです。test-unit gemの変更点はこんな感じです。

`--gc-stress`オプションを追加しました。テスト実行中のみ`GC.stress`を有効にします。GC関連のバグのときに便利です。

[GH-237](https://github.com/test-unit/test-unit/issues/237)
[GH-262](https://github.com/test-unit/test-unit/issues/262)

Ruby 3.4向けの変更が入っています。Ruby 3系は高い互換性を維持したまま開発が進んでいますが、ちょいちょい非互換があります。そういうやつにはすでに対応してあるので、Ruby 3.4でもいい感じに動きます。

[GH-253](https://github.com/test-unit/test-unit/issues/253)

`--report-slow-tests`オプションを追加しました。遅いテストを遅い順に表示してくれます。この順に高速化していけば効率よくテスト実行時間を高速化できます。

[GH-235](https://github.com/test-unit/test-unit/issues/235)

`Thread`を使った並列実行をサポートしました。詳細は[test-unitで並列テスト実行]({% post_url 2024-11-29-test-unit-parallel %})を参照してください。

### おまけ：rdoc

RDocはメンテンナンスしていませんが、Ruby 3.4.0に組み込まれるRDocには[Red Data Tools：RDocとRubyGemsを疎結合にしたい！]({% post_url 2024-09-11-red-data-tools-rdoc-rubygems-loose-coupling %})が入っていることをアピールしておきます。

### まとめ

そろそろRuby 3.4.0がリリースされているので、私がメンテナンスしているdefault gem/bundled gemの変更点を紹介しました。普通のgemとしてもリリースしてあります。つまり、今でも試せる状態になっています。もし、なにか問題を見つけたら教えてください。3.4.0リリース前に直せれば3.4.0に組み込まれるgemはその問題が直った状態にできます。
