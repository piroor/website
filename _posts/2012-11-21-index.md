---
tags: []
title: CentOS 5でCentOS 6用のzshのSRPMをビルドする方法
---
快適なコマンドラインライフを送るためにはシェルのことをおろそかにできません。シェルは昔からあるツールですが、今でも改良されています。そのため、数年前にリリースされたシェルよりも最近リリースされたシェルの方が便利です。
<!--more-->


さて、CentOS 5は今でもメンテナンスされているOSなので、CentOS 5で作業することがあります。しかし、CentOS 5の標準yumリポジトリにあるzshは4.2.6と古いバージョンです。[zshの更新履歴](http://zsh.sourceforge.net/News/)を見ると、4.2.6は2005-12-05にリリースされたバージョンで、実に7年前です。マルチバイト文字のサポートは4.3.xから始まっているので、CentOS 5ではzsh上で日本語を扱うことができません[^0]。

CentOS 6の標準yumリポジトリにあるzshは4.3.10で2009-06-01にリリースされたものです。最新[^1]ではありませんが、日本語も使えるバージョンです。CentOS 5で作業するときも、少なくともこれくらいの新しいバージョンは使いたいところです。このくらいのバージョンになると[おすすめzsh設定]({% post_url 2011-09-05-index %})も使えます。

ということで、CentOS 5でCentOS 6用のzshのSRPMをビルドしてCentOS 5でzsh 4.3.10を使うための方法を説明します。

### ビルド

SRPMを一般ユーザー権限でビルドするためには~/.rpmmacrosなどを用意しないといけませんが、少し面倒です。これを自動化してくれるツールがあります。それがrpmdev-setuptreeコマンドです。

このコマンドはrpmdevtoolsパッケージの中に含まれているのですが、rpmdevtoolsパッケージはCentOS 5では提供されていません。そのため、[EPEL](http://fedoraproject.org/wiki/EPEL)にあるパッケージを使います[^2]。

それでは、EPELを使えるようにします。ただし、今回はrpmdevtoolsパッケージだけを使いたいので、EPELはデフォルトでは無効にしておきます。

{% raw %}
```
% wget http://ftp.iij.ad.jp/pub/linux/fedora/epel/5/i386/epel-release-5-4.noarch.rpm
% sudo -H rpm -Uvh epel-release-5-4.noarch.rpm
% sudo -H sed -i'' -e 's/enabled=1/enabled=0/' /etc/yum.repos.d/epel.repo
```
{% endraw %}

まず、~/.rpmmacrosなどを用意します。

{% raw %}
```
% sudo -H yum install --enablerepo=epel -y rpmdevtools
% rpmdev-setuptree
```
{% endraw %}

CentOS 6用のzshのSRPMをダウンロードします。

{% raw %}
```
% wget http://vault.centos.org/6.3/os/Source/SPackages/zsh-4.3.10-5.el6.src.rpm
```
{% endraw %}

ビルドに必要なパッケージをインストールします。yum-utilsパッケージに含まれているyum-builddepコマンドが便利です。

{% raw %}
```
% sudo -H yum install -y yum-utils
% sudo -H yum-builddep zsh-4.3.10-5.el6.src.rpm
```
{% endraw %}

SRPMをビルドします。本当は`rpmbuild --rebuild zsh-4.3.10-5.el6.src.rpm`でビルドしたいところですが、CentOS 5のrpmbuildコマンドではCentOS 6用のSRPMのチェックサムを確認できない[^3]ので、不本意ながら`--nomd5`付きでSRPMを展開してからビルドします。

{% raw %}
```
% rpm --nomd5 -i zsh-4.3.10-5.el6.src.rpm
% rpmbuild -bb ~/rpmbuild/SPECS/zsh.spec
```
{% endraw %}

これで、~/rpmbuild/RPMS/x86_64/zsh-4.3.10-5.x86_64.rpmにRPMができます。

### インストール

ビルドしたPRMをインストールします。

{% raw %}
```
% sudo -H rpm -Uvh ~/rpmbuild/RPMS/x86_64/zsh-4.3.10-5.x86_64.rpm
```
{% endraw %}

ログインシェルにする場合はchshで変更します。ログインシェルを変更する場合はうまくシェルが起動しなかった場合のために、別のターミナルを開いたままでログインし直すと安全です。うまくログインし直せなかったら別のターミナルに開いたままのシェルでchshして元のシェルに戻せます。このように別のターミナルを開いたまま動作確認をするのは、PAMやsudoの設定を変更するときと同じですね。

### まとめ

CentOS 5でCentOS 6用のRPMをビルドする方法を説明しました。具体例としてzshを用いました。zshを用いたのは、実際にCentOS 5のzshで作業するのがとてもつらくて、ビルドの必要性があったからです。

[^0]: CentOS 5で提供されているbash 3.2では日本語を使えます。[bashの更新履歴](http://tiswww.case.edu/php/chet/bash/NEWS)を見ると、2.05bから使えるようになったようです。

[^1]: 現在は2012-07-24にリリースされた5.0.0が最新です。

[^2]: サーバーには必要なもの以外インストールしたくない、という場合は別マシンでビルドしてzshのRPMをコピーしてインストールするとよいでしょう。

[^3]: 「cpio: MD5 sum mismatch」というメッセージがでてエラーになります。
