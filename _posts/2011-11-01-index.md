---
tags: []
title: 'お知らせ: OSC2011.DBでgroongaストレージエンジンを紹介'
---
今度の土曜日11/5に開催される[OSC2011.DB](http://www.ospn.jp/osc2011.db/)の10:35からのセッション「[OSSDB MySQL](https://www.ospn.jp/osc2011.db/modules/eguide/event.php?eid=15)」に少しおじゃまして[groongaストレージエンジン](http://mroonga.github.com/)を紹介します。groongaストレージエンジンがイベントなどで紹介されるのは昨年の[全文検索エンジンgroongaを囲む夕べ #1](http://atnd.org/events/9234)以来のはずなので約1年ぶりになります。その間に初のメジャーリリースである1.0.0がリリースされるなど、だいぶ成長しています。そのため、話題はたくさんあるのですが、その中から特に注目すべきところを選り抜いて紹介します。
<!--more-->


groongaストレージエンジンについては1ヶ月後の[全文検索エンジンgroongaを囲む夕べ 2](http://atnd.org/events/20446)でも詳しく紹介されますが、一足早く知りたい人はぜひ参加してください。（参加費用は無料ですが参加登録が必須なので注意してください。）
