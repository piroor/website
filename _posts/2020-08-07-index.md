---
tags:
- mozilla
title: Thunderbird 78に未対応のアドオンのThunderbird 78対応の現状と課題について
author: piro_or
---
Mozillaサポート業務に従事している結城です。
<!--more-->


去る7月17日、Thunderbird 78がリリースされました。この記事では、[Flex Confirm Mail](https://addons.thunderbird.net/thunderbird/addon/flex-confirm-mail/)のThunderbird 78対応版開発作業[^0]を通じて得られた知見を元に、「アドオンのThunderbird 78対応化」が可能かどうかの大まかな判断基準や、参考となる情報をご紹介します。

### Thunderbird 78における従来型アドオン対応終了の経緯

Thunderbird 78では従来形式のアドオン（以下、「XULアドオン」と表記します）が一切動作しなくなり、WebExtensionsと呼ばれるAPIセット[^1]に基づく新形式のアドオン（以下、「WebExtensionsアドオン）のみが動作するようになりました。これは、Firefox 57において行われた、XULアドオンの廃止によるWebExtensionsアドオンへの一元化に相当する変化となります。

Thunderbirdは、対応するバージョン番号のFirefoxがベースになっているため本来であれば、XULアドオンはThunderbird 60で動作しなくなるところでした。しかし、ThunderbirdのWebExtensions APIは開発リソースの不足のために機能が揃っておらず、Thunderbird 60やThunderbird 68の時点でXULアドオンに対応しなくなると、代替手段がなくなってしまう状況でした。そのため、Firefoxでは無効化された機能を復活させたり、廃止された機能を代替する独自の互換レイヤーを実装したりして、XULアドオンの延命措置が図られていました。

今回のThunderbird 78では、WebExtensions APIの実装がある程度の完成度に達したため、XULアドオンの延命に終止符が打たれ、ようやく満を持してWebExtensionsアドオンへの全面移行が行われたということになります。

### XULアドオンとWebExtensionsアドオンの相違点

WebExtensionsアドオンは、エンドユーザーからはXULアドオンと区別が付かなくても、開発者側視点ではまったくの別物です。
[![（FirefoxにおけるXULアドオンとWebExtensionsアドオンの差異を示した図。XULアドオンはFirefoxの内部にUIからバックエンドまで深く癒着しているのに対し、WebExtensionsアドオンはサンドボックス内に公開されたAPIのみに依存するため、疎結合となっている。）]({{ "/images/blog/20200807_0.png" | relative_url }} "（FirefoxにおけるXULアドオンとWebExtensionsアドオンの差異を示した図。XULアドオンはFirefoxの内部にUIからバックエンドまで深く癒着しているのに対し、WebExtensionsアドオンはサンドボックス内に公開されたAPIのみに依存するため、疎結合となっている。）")]({{ "/images/blog/20200807_0.png" | relative_url }})
こちらはFirefoxにおけるXULアドオンとWebExtensionsアドオンの差異を図示したものですが、この説明はThunderbirdにもそのままあてはまります。

XULアドオンは、実態としては*Thunderbirdに対して動的に適用されるパッチ*に近い存在でした。そのため、Thunderbirdの動作を自由自在に書き換えることができ、非常に自由度が高いのが特徴でした。その反面、パッチを当てる対象となるThunderbirdの内部実装に対して極めて密結合となるため、Thunderbird側の変更の影響を受けやすく、Thunderbirdの更新の度にどこかしらの部分が動作しなくなりやすい性質がありました。

これに対しWebExtensionsアドオンは、サンドボックス化された環境の中で動作し、WebExtensions APIとして用意されたインターフェースのみを通じてThunderbirdと相互に通信する、*独立した小型のソフトウェア*と言えます。Web開発者の視点では実質的には、Thunderbirdというバックエンドと通信するためのWebExtensions APIというアダプターを使った、*小規模なWebアプリ（のフロントエンド）* とも言えるでしょう。Thunderbirdの内部実装に対しては疎結合となるため、Thunderbirdの更新で動かなくなるリスクは大幅に減じられました。その一方で、APIが用意されていないことは一切行えず、自由度の幅は大きく制約されています。

このような違いがあるため、XULアドオンからWebExtensionsアドオンへの更新作業（WebExtensions化）は、特にXULアドオンとして適切に設計されていた物ほど、「Thunderbird 78に対応するための小規模な改修」と言うよりも、「*既存のアドオンの動作をリファレンスとして、それと同等の結果を得られるようなソフトウェアを新たに開発する*」と言った方が実態に近い、[極めて大がかりな作業](https://piro.sakura.ne.jp/latest/blosxom.cgi/mozilla/extension/treestyletab/2017-10-03_migration-we-ja.htm)となります[^2]。

そのため、[いくつかの著名アドオンについて、WebExtensions化を支援するクラウドファンディングが行われた事例もあります](https://forest.watch.impress.co.jp/docs/serial/yajiuma/1231765.html)。このクラウドファンディングは目標金額を達成したようですが、本稿執筆時点で各アドオンのWebExtensions版は未リリースであることからも、上記の作業の大変さを窺い知れます。

### WebExtensionsアドオン開発の注意点

「アドオンのWebExtensions化」の成功に必要なポイントは、

  * したいことを実現するのに必要な機能がWebExtensions APIで提供されているかどうか、WebExtensionsの仕組み上で許可されているか

  * WebExtensions APIが用意されていない部分をどのように実装するか

の2点に集約されます。

#### WebExtensions APIの傾向

[FirefoxのWebExtensions APIの情報はMDNに集約されています](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions)が、ThunderbirdのWebExtensions APIの情報は、OSS向けの技術文書ホスティングサイトであるRead The Docsに[Thunderbird WebExtension APIs](https://thunderbird-webextensions.readthedocs.io/en/78/)として集約されています。実際のアドオン開発にあたっては、導入部はMDNのチュートリアルを参照しつつ適宜Thunderbird向けに読み替えて、要領が掴めてきたらThunderbirdに固有のAPIをThunderbird用のドキュメントで調べる、という要領になるでしょう。

全体的な傾向としては、以下の事が言えます。

  * Thunderbirdの各ウィンドウの内容には、ブラウザの「タブ」としてアクセスする。

    * メール表示ウィンドウと編集ウィンドウにはタブは存在しないが、「タブが1つだけ開かれているウィンドウ」として扱われる。

  * ツールバーボタンは1アドオンにつき、メール表示画面用に最大1つ、メール編集画面用に最大1つまで登録できる。

  * 表示中・編集中のメールの本文は「コンテンツ」として扱われ、WebExtensions APIで言うところの[コンテンツスクリプト](https://developer.mozilla.org/ja/docs/Mozilla/Add-ons/WebExtensions/Content_scripts)で操作する。

    * 受信メールの表示時に「添付ファイルの一覧」を取得する方法はなく、[マルチパートの各パート](https://thunderbird-webextensions.readthedocs.io/en/78/messages.html#messages-messagepart)を解析する必要がある。

  * アドレス帳やメールフォルダなどへのアクセス方法は、WebExtensionsでのブックマークの操作に似ている。

  * Thunderbird上で発生したイベント・行われた操作は、原則として事後的に通知されるため、アドオンからのキャンセルはできない。

    * ただし、唯一[`compose.onBeforeSend`](https://thunderbird-webextensions.readthedocs.io/en/78/compose.html#onbeforesend-tab-details)のみ「メールが送信される前」に通知され、リスナーの戻り値で操作をキャンセルできる（WebExtensionsの[`webRequest.onBeforeRequest`](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/webRequest/onBeforeRequest)と同様）。

APIの一部はFirefoxと共通で、詳細はMDNを参照するように誘導されている場合があります。ただ、Firefoxでは行えるはずのことがThunderbirdの同名のAPIでは行えない部分もいくつかあります。筆者自身、Thunderbird用のWebExtensionsアドオンの開発中にいくつかそのようなトラブルに遭遇し、以下の通りバグとして報告しました。

  * [1652469 - Storage changes are not delivered to storage.onChanged listeners in options page](https://bugzilla.mozilla.org/show_bug.cgi?id=1652469)

  * [1652478 - tabs.executeScript() does not work for a tab in a popup type window](https://bugzilla.mozilla.org/show_bug.cgi?id=1652478)

このように、細かい部分ではまだまだ不具合が散見されるため、APIを使う際には若干の注意と工夫が必要です。

なお、[Thunderbirdの内部的な実装にアクセスする実験用API](https://thunderbird-webextensions.readthedocs.io/en/latest/how-to/experiments.html)も存在していますが、このAPIの使用はおすすめできません。[今後のThunderbirdの開発ロードマップ](https://developer.thunderbird.net/planning/roadmap)にも記されているとおり、今後はThunderbird内の古い実装はどんどん廃止されていく予定のため、このAPIに依存するアドオンでは、「Thunderbird本体が更新されたらアドオンが動かなくなる」というXULアドオン時代の問題が継続してしまうからです。

#### APIでカバーされない、ユーザー体験に直接関わる部分

WebExtensions APIは「アドオンを作りやすくする便利機能」ではなく「Thunderbirdをアドオンから操作するための基盤」として設計されています。昨今のWeb APIの仕様と同様、ユーザー体験に直接関わる部分はアプリケーション（アドオン）開発者側で工夫して実現する必要があります。そのような工夫を一般化した物が「ライブラリ」や「ツールキット」です[^3]。

Webページ用としては、昔からjQuery UIなどの独自のUIツールキットがいくつも開発されてきました。WebExtensionsアドオンは実質的には小規模なWebアプリだと述べましたが、このようなWebアプリ用ツールキットをWebExtensionsアドオンでも使うことができます。

ただ、Webページ用のUIツールキットは「Webページの中でダイアログのような物を表示する」のように、良くも悪くもWebページ内で動作が閉じる前提であることが多いです。Webページよりももう少し自由度が高いWebExtensionsのニーズにはマッチしないこともありますし、設定の永続化などWeb APIではなくWebExtensions固有のAPIを使った方が適切な場合も多いです。

このような理由から筆者自身がWebExtensionsアドオン用に開発したライブラリはいくつかあり、このブログでも過去にいくつか使用法をご紹介してきました。

  * [設定画面の提供を容易にするライブラリ：Options.js]({% post_url 2018-07-09-index %})

  * [設定の読み書きを容易にするライブラリ：Configs.js]({% post_url 2018-06-12-index %})

  * [メニューやパネル風のUIを提供するライブラリ：MenuUI.js]({% post_url 2018-05-11-index %})

当社でのThunderbird向けWebExtensionsアドオンの開発過程でも、UIとして任意の内容のダイアログウィンドウを開く場面で工夫や回避策が必要であったことから、[ダイアログ形式のUIの実装を支援するライブラリ](https://gitlab.com/clear-code/webextensions-lib-dialog)を新規に開発しました。

### XULアドオンのWebExtensions化に取り組む際の注意点

既存のXULアドオンをWebExtensions化する際には、*これらの前提を踏まえた上で、前提にフィットするようにアドオンの仕様を見直す*ことが必要となります。

すでに述べたとおり、XULアドオンとWebExtensionsアドオンは前提が大きく異なります。「XULアドオンを最小限の変更でWebExtensions化する」「XULアドオンで実現していた機能をすべてWebExtensionsアドオンに持ち込む」という発想のままで作業に取り組むと、「あれもできない、これもできない」と様々な壁に阻まれて、結局WebExtensions化自体を断念する他なくなってしまいます。

そうではなく、まずは*アドオンの各機能を「その機能は何のために・どういう目的で用意したのか？」にまで立ち返って分析し、「WebExtensions APIの上でその目的を達成する」という発想で設計し直す*ことが肝心です。その上で、最終的なユーザー体験をXULアドオンでの物に近付けるよう工夫するのが、XULアドオンのWebExtensions化では有効なアプローチと言えます。

多機能なXULアドオンほど、WebExtensions APIで実現できる機能とそうでない機能が混在している場合は多いです。そのようなケースでは、*機能ごとに個別のアドオンとして切り出す*のも有効です。「全機能が揃わないのですべて諦める」よりは、「一部の機能だけでもThunderbird 78で使える」方がユーザー体験として望ましいことに、疑いの余地は無いでしょう。

### 当社開発のアドオンの中で、WebExtensions化は不可能と見込まれる物

[当社がThunderbird Add-onsで公開しているThunderbirdアドオン](https://addons.thunderbird.net/ja/thunderbird/user/clearcode-inc/)は多数あります[^4]。この中で、具体的にThunderbird 78対応のための作業が進行しているのは、現時点では[Flex Confirm Mail](https://addons.thunderbird.net/thunderbird/addon/flex-confirm-mail/)のみとなります。

それ以外のアドオンについてWebExtensions化が可能かどうかの予備調査を実施した所、以下のアドオンは必要なAPIが存在しないために、WebExtensions化はできない可能性が高いと分かりました。前述の傾向と併せて、「どのようなアドオンであればThunderbrid 78に対応できて、どのようなアドオンは対応できないか」を見積もる際の参考にしていただければ幸いです。

  * [Addressbooks Default Search](https://addons.thunderbird.net/thunderbird/addon/addressbooks-default-search/)：ThunderbirdのUIをアドオンからは直接操作できないため。

  * [Always Default Client](https://addons.thunderbird.net/thunderbird/addon/always-default-client/)：アドオンからはThunderbirdの設定を変更できないため。

  * [Attachemnt Encoding Detector](https://addons.thunderbird.net/thunderbird/addon/attachemnt-encoding-detecto/)：Thunderbirdが添付ファイルの内容を表示する前の処理に介入することはできないため。

  * [Auto Password Registerer](https://addons.thunderbird.net/thunderbird/addon/auto-password-registerer/)：アドオンからはパスワードマネージャへアクセスできないため。

  * [Clear IMAP Local Cache](https://addons.thunderbird.net/thunderbird/addon/clear-imap-local-cache/)：アドオンからはThunderbirdのプロファイル内のファイルを直接操作できないため。

  * [Customizable LDAP AddressBook Auto Complete](https://addons.thunderbird.net/thunderbird/addon/customizable-ldap-addressbook-/)：アドオンからはメールアドレスのオートコンプリート処理に介入できないため。

  * [Delete Only Empty Folder](https://addons.thunderbird.net/thunderbird/addon/delete-only-empty-folder/)：アドオンからは「操作が行われる直前に介入し、操作をキャンセルする」ということができないため。（事後的な介入は可能）

  * [Disable about:config](https://addons.thunderbird.net/thunderbird/addon/disable-aboutconfig/)：ThunderbirdのUIをアドオンからは制御できないため。

  * [Disable About Something](https://addons.thunderbird.net/thunderbird/addon/disable-about-something/)：ThunderbirdのUIをアドオンからは制御できないため。

  * [Do Not Save Password](https://addons.thunderbird.net/thunderbird/addon/do-not-save-password/)：ThunderbirdのUIをアドオンからは制御できず、パスワードマネージャへもアクセスできないため。

  * [リンク先ファイルの送信禁止](https://addons.thunderbird.net/thunderbird/addon/dont-send-linked-files/)：ThunderbirdのUIをアドオンからは制御できないため。（ただし、添付されようとしているファイルを即座に削除する事は可能と考えられる。）

  * [Force Authentication at Startup](https://addons.thunderbird.net/thunderbird/addon/force-auth-at-startup/)：ThunderbirdのUIをアドオンからは制御できないため。

  * [Force Hide Message Pane](https://addons.thunderbird.net/thunderbird/addon/force-hide-message-pane/)：ThunderbirdのUIをアドオンからは制御できないため。

  * [globalChrome.css](https://addons.thunderbird.net/thunderbird/addon/globalchromecss/)：ThunderbirdのUIをアドオンからは制御できないため。

  * [Hide Option Pane](https://addons.thunderbird.net/thunderbird/addon/hide-option-pane/)：ThunderbirdのUIをアドオンからは制御できないため。

  * [MIME Type「application/applefile」による問題の回避](https://addons.thunderbird.net/thunderbird/addon/no-applicationapplefile/)：Thunderbirdの内部的なデータベースにアドオンからアクセスできないため。

  * [Only Minor Update](https://addons.thunderbird.net/thunderbird/addon/only-minor-update/)：自動更新処理にはアドオンから介入できないため。

  * [Periodic Memory Usage Dumper](https://addons.thunderbird.net/thunderbird/addon/periodic-memory-usage-dumper/)：詳細なメモリ消費状況を取得するAPIが存在しないため。

  * [Set Default Columns](https://addons.thunderbird.net/thunderbird/addon/set-default-columns/)：ThunderbirdのUIをアドオンからは制御できないため。

  * [システムモニター (System Monitor)](https://addons.thunderbird.net/thunderbird/addon/system-monitor/)：ウィンドウ内に常時表示できるツールバーボタンが1つだけ且つ固定サイズで、十分な広さの表示領域を確保できないため。

  * [UI Text Overrider](https://addons.thunderbird.net/thunderbird/addon/ui-text-overrider/)：ThunderbirdのUIをアドオンからは制御できないため。

  * [UxU - UnitTest.XUL](https://addons.thunderbird.net/thunderbird/addon/uxu-unittestxul/)：各種のデバッグ用APIにアドオンからはアクセスできないため。

なお、WebExtensions化の可否以前のこととして、以下のアドオンは必要性がなくなったため、今後の更新は行われません。

  * [Edit Message Encoding Fallback](https://addons.thunderbird.net/thunderbird/addon/edit-message-encoding-fallback/)：回避対象のThunderbirdの不具合が既にクローズされているため。

  * [Force Addon Status](https://addons.thunderbird.net/thunderbird/addon/force-addon-status/)：相当する機能がThunderbird本体のポリシー設定に含まれるため。

### まとめ

以上、Thunderbird用のXULアドオンをThunderbird 78以降に対応させる上で必要となる情報をまとめてみました。

Firefox 57でのXULアドオン廃止は、Firefoxユーザーに未曾有の大混乱を引き起こしました。現時点ではThunderbirdではまだ深刻な混乱は発生していませんが、Thunderbird 68.12のサポート終了のタイミングでThunderbird 78への自動更新が始まるため、その時には何らかの対応が必要となります[^5]。

当社では、業務上で重要度の高いアドオン（当社製でない物も含む）のThunderbird 78対応作業も含む、Thunderbirdの法人利用者向けの有償サポートを提供しております。全社的に使用中のアドオンがThunderbird 78で動作しないなどのトラブルでお困りの情シス担当者さまがいらっしゃいましたら、[お問い合わせフォーム](/contact/)よりご連絡下さい。

また当社では、このような「OSSの改善に関わりつつ、それを仕事とすること」に関心のある方の[エンジニア採用](https://www.clear-code.com/recruitment/)を行っています。日常的にOSSを使用していて、自分でもOSSに関わること自体を仕事にしてみたいという方は、ぜひお問い合わせいただければ幸いです。

[^0]: 現在も進行中で、作業は未完了です。

[^1]: ドキュメントによっては「MailExtensions」という表記も見られますが、多くのドキュメントではFirefoxと同じ「WebExtensions」と表記されているため、この記事ではこちらに統一することにします。

[^2]: XULアドオンで使用されていたコードの一部をWebExtensionsアドオンで流用できる場合はありますが、作業方針としてはあくまで「新規開発」と捉える方が実態に即しています。ただ、XULアドオンらしくない・一般的なWebアプリの作法で作られていたXULアドオンは、流用できるコードの量が多くなるとは言えます。

[^3]: XULアドオンで言えば「OS標準のダイアログと同様に振る舞うダイアログ」や「文字・数値の入力が可能なドロップダウンリスト」などがツールキットの領域にあたります。

[^4]: 法人サポート契約の中で必要が生じてご依頼を頂き作成した物を一般公開しているケースについては、まだ作業のご依頼を頂いていないため現時点でThunderbird 68に未対応の状態の物もあります。

[^5]: 自動更新を停止して古いバージョンを使い続けるユーザーも少なくないですが、安全性の観点からはお薦めできません。
