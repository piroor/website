---
tags: []
title: Rubyで定義したメソッドの使用例をYARD用のドキュメントとして書く方法
---
### はじめに

YARDというRuby用のドキュメンテーションツールがあります。[APIのドキュメントの記述方法は大きく2種類あります]({% post_url 2011-05-05-index %})が、YARDはコードにコメントとしてドキュメントを埋め込む形式を採用しています。専用の記法を使って構造化された読みやすいドキュメントを書けることが[類似ツールであるRDocとの大きな違い]({% post_url 2011-05-11-index %})です。
<!--more-->


今回は「Rubyで定義したメソッドの使用例を示す」ドキュメントのYARD流の書き方を紹介します。

なぜ使用例の書き方を説明するかというと、使用例を1つ示すだけで使い方をぐっとわかりやすく説明することができるからです。もちろん、引数や戻り値などメソッドについての情報も必要ですが、それらは断片的な情報なため、そこから全体像をイメージするにはもうひとステップ必要になります。一方、使用例は詳細を示すことには不向きですが、どんな状況で使うのか、どのように準備して使うのかといった前後関係も含めた全体像を示すことができます。

ライブラリーを初めて使うとき、最初にサンプルコードを動かして動作を確認した経験があるはずです。動作を確認し、サンプルコードの中の値を少しずつ変更して、実際に使うコードまで改良していったこともあるでしょう。このように、実際のコードで示された使い方はユーザーにとって有用な情報となります。

ユーザーではなく、ドキュメントを書く開発者の立場からも考えてみましょう。使用例を書くことで自分の作ったライブラリーのAPIが使いやすいかどうかを確認することができます。これは、[よいソフトウェアを書くことに役立つ]({% post_url 2011-10-27-index %})ことです。

このように、ドキュメントに使用例を書くことはユーザーにも開発者にもメリットがあります。

それでは、YARDで使用例を書く方法を次の順で説明します。

  1. YARDで使用例を書くために使う`@example`タグの使用例を紹介

  1. YARDで使用例を書くために使う`@example`タグについて説明

  1. Rubyのコードに実際に`@example`タグを使って使用例を書き、その出力を確認


### `@example`タグの使用例

最初に使用例があるとわかりやすくなると説明したので、ここでも最初に`@example`タグの使用例を示します。詳細は後から説明します。

`@example`タグの同じ行に書いている「extract ages over 40」が使用例のタイトルで、それ以降のインデントされたコードが使用例になります。

{% raw %}
```ruby
# @example extract ages over 40
#   overages = extract_overage([17, 43, 40, 56], 40)
#   puts("overages: #{overages.join(", ")}") #=> "overages: 43, 56"
def extract_overage(ages, limit_age=20)
  ages.select do |number|
    number > limit_age
  end
end
```
{% endraw %}

どんな風に書くか雰囲気をつかめたでしょうか。

### `@example`タグについて

それでは、`@example`タグについて説明します[^0]。`@example`タグは、ドキュメント対象であるメソッドの使用例となるコードを示すために使います。後述する`yardoc`コマンドを使って生成するHTMLのリファレンスマニュアルでは、コードが使用例だとわかるように整形されます。

`@example`タグの書式は次の通りです。

{% raw %}
```
@example 使用例のタイトル
  使用例のコード
```
{% endraw %}

「使用例のタイトル」と「使用例のコード」に書く内容を説明します。

「使用例のタイトル」には、使用例を一言で示すような説明を書きます。`@example`タグは複数個同時に使用できるため、それぞれの例が区別しやすくなるような説明になっているか確認してください。「この使用例が注目していることはなんだろう」と考えるとよいタイトルをつけられるはずです。

使用例のタイトルは省略可能ですが、できるだけ書くようにしましょう。タイトルをつけられない使用例は何に注目しているかが散漫になっている可能性が高いです。そのような使用例はユーザーにとってわかりにくいものです。「何に注目しているか」を忘れずにタイトルがつけられる使用例を書いてください。

「使用例のコード」には、使用例として示したいコードを書きます。コードは`@example`タグの次の行以降に書きます。インデントすることを忘れないでください。インデントすることで、そのブロックにあるコードが使用例のコードであることを示します。

### 使用例を書くRubyのコード

次に、実際に使用例を書くRubyのコードを示します。

`@example`タグで使用例を書くコード（以降、「サンプルコード」と呼びます）は次の通りです。

{% raw %}
```ruby
# Generates Array containing numbers exceeding limit_age. Numbers is members of ages.
# For example, extract_overage([17, 43, 40, 56], 40) #=> [43, 56]
#
# @param ages [Array] numbers checked if they exceeded from limit_age.
# @param limit_age [Integer] limit_age limit used to extract
#   bigger ages than it.
# @return [Array] Returns Array containing numbers exceeding limit_age.
def extract_overage(ages, limit_age=20)
  ages.select do |number|
    number > limit_age
  end
end
```
{% endraw %}

この状態のサンプルコードからHTMLのリファレンスマニュアルを生成してみましょう。これは、使用例を書いたサンプルコードから生成したリファレンスマニュアルと後で比較するためです。

HTMLのリファレンスマニュアルを作成するには、YARDに付属する`yardoc`コマンドを使用します。サンプルコードをexample.rbというファイルに保存して、次のコマンドを実行してください。

{% raw %}
```
% yardoc example.rb
```
{% endraw %}

`yardoc`コマンドを実行すると、次のようなリファレンスマニュアルができます[^1]。

![使用例なしのリファレンスマニュアル]({{ "/images/blog/20130919_0.png" | relative_url }} "使用例なしのリファレンスマニュアル")

### `@example`タグの使い方

いよいよ、`@example`タグで例を書いていきます。

今回は使用例のタイトルに「extract ages over 40」を使い、使用例のコードは次のコードにします。

{% raw %}
```ruby
overages = extract_overage([17, 43, 40, 56], 40)
puts("overages: #{overages.join(", ")}") #=> "overages: 43, 56"
```
{% endraw %}

では、実際にサンプルコードのコメントに使用例を書きましょう。使用例の1つはすでにメソッドの全体の説明の中に「`extract_overage([17, 43, 40, 56], 40) #=> [43, 56]`」と書かれていました。その例を`@example`タグで書き直します。

{% raw %}
```ruby
# Generates Array containing numbers exceeding limit_age. Numbers is
# members of ages.
#
# @example extract ages over 40
#   overages = extract_overage([17, 43, 40, 56], 40)
#   puts("overages: #{overages.join(", ")}") #=> "overages: 43, 56"
#
#
# @param ages [Array] numbers checked if they exceeded from limit_age.
# @param limit_age [Integer] limit_age limit used to extract
#   bigger ages than it.
# @return [Array] Returns Array containing numbers exceeding limit_age.
def extract_overage(ages, limit_age=20)
end
</code></pre>
```
{% endraw %}

コメント内で`@example`タグを書く位置はどこでもよいのですが、メソッド全体の説明の直後、引数や戻り値の説明の前に書くことをオススメします。これは、ドキュメントを読む側の視点で考えるとわかります。引数や戻り値の説明の前に使用例を見ておくと、その後の引数や戻り値の説明を理解しやすくなるからです。使用例を頭に入れてから引数や戻り値の説明を見ると、使用例で使われていた引数や戻り値に説明を引きあわせて考えることができるため、使用例を見ていない状態よりも理解しやすくなります。

使用例を書いたサンプルコードから`yardoc`コマンドを使ってリファレンスマニュアルを生成します。生成したリファレンスマニュアルは次のようになります。

![使用例ありのリファレンスマニュアル]({{ "/images/blog/20130919_1.png" | relative_url }} "使用例ありのリファレンスマニュアル")

HTMLのリファレンスマニュアルを見ると、赤枠の中に使用例のコードが表示されていることがわかります。

### まとめ

YARDでは`@example`タグを使って使用例を書けることを実例を示しながら説明しました。また、HTMLのリファレンスマニュアルではどのように使用例が整形されるかも示しました。

今回は、YARDの書き方シリーズの4回目として`@example`タグを使った使用例の書き方を紹介しました。使用例を書くことで、実装したメソッドを実際にどう使えばよいのかをわかりやすく伝えることができます。今後、ドキュメントを書くときは、ユーザーに使いやすさを伝えるために使用例も書いてみてください。

これまでも次の通りYARDの書き方を説明しました。こちらも合わせて確認してください。

  * [Rubyの拡張ライブラリにYARD用のドキュメントを書く方法]({% post_url 2012-10-02-index %})
  * [Rubyで定義したメソッドの引数についてのドキュメントをYARD用に書く方法]({% post_url 2013-04-03-index %})
  * [Rubyで定義したメソッドに戻り値のYARD用ドキュメントを書く方法]({% post_url 2013-04-18-index %})

[^0]: タグというのはYARD専用の記法の1つで、コードのメタデータを記述するために使用します。[詳細]({% post_url 2013-04-03-index %})。

[^1]: カレントディレクトリにできた`doc/top-level-namespace.html`がこのリファレンスマニュアルに相当します。
