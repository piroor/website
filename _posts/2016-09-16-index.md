---
tags:
- ruby
- presentation
title: 'RubyKaigi2016: Ruby Reference Manual 2016 Autumn #rubykaigi'
---
2016年9月8日から10日にかけて開催された[RubyKaigi 2016](http://rubykaigi.org/2016/)で「Rubyリファレンスマニュアル(るりま)に関わる人を増やしたい！」という話をしました。
<!--more-->


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/okkez/rubykaigi2016/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/okkez/rubykaigi2016/" title="Ruby Reference Manual 2016 Autumn">Ruby Reference Manual 2016 Autumn</a>
  </div>
</div>


  * [スライド(Rabbit Slide Show)](https://slide.rabbit-shocker.org/authors/okkez/rubykaigi2016/)

  * [スライド(SlideShare)](http://www.slideshare.net/okkez/rubykaigi2016)

話の流れは以下の通りです。

  * 「るりま」と「るびま」の違いについて

  * 簡単に歴史をおさらい

    * 10周年だった

  * これまでのコントリビューター数を紹介

  * これからやりたいことを紹介

    * Ruby2.4.0 対応

    * EPUB

    * RDoc

### 「るりま」と「るびま」の違いについて

「るりま」と「[るびま](http://magazine.rubyist.net/)」の違いについて確認しました。

一文字違いですが、どちらも[一般社団法人 日本Rubyの会](http://ruby-no-kai.org/)のサポートを受けて活動しています。

### 簡単に歴史をおさらい

  * 2006-08-27 にプロジェクトが始まりました

    * 初期は青木さんの個人のサーバーでホスティングされていました

    * その後、いつだったか忘れましたが、日本Rubyの会のサーバーでホスティングされるようになりました

  * 2011-09-01 メンテナンスフェイズに移行しました

    * 最低限のドキュメントを書くのに約5年かかりました

  * 2013-05-30 RubyKaigi2013で発表しました

  * 2013-06-02 GitHub にリポジトリを移行しました

  * Rubyの新しいバージョンがリリースされるたびにドキュメントの更新をしています

  * 2016-09-09 RubyKaigi2016 で発表しました

### これまでのコントリビューター数を紹介

延べ100人以上の人が貢献してくれています！RubyKaigi2016での発表後に新たにプルリクエストを送ってくれた人もいるので、これからも多くの人が協力してくれることを期待しています。

### これからやりたいことを紹介

Ruby2.4.0対応やEPUB生成等、やりたいことを話しました。

初日のパーティで[YassLab](http://yasslab.jp/ja/)の安川さんと話したところ、色々と参考になるお話を聞くことができました。

  * EPUB生成をやるなら、[kmuto/review: Re:VIEW is flexible document format/conversion system](https://github.com/kmuto/review)を使ったらいいですよ

    * EPUBの仕様は複雑なので、EPUB対応を主にやっているツールを使うべき

  * Ruby本体が更新されたときの更新通知はYassLabさんで開発しているツールが使えるかもしれない

  * ドキュメント翻訳のシステム化とお金の話

また、発表後に質問しに来てくれた人が数人いて、興味を持って話を聞きにきてくれた人がいたことを嬉しく思いました。

以下のようなフィードバックをもらいました。

  * 具体的にやりたいことややって欲しいことをリスト化すると貢献したい人がやってくれるのでは？

  * (修正が必要な場所を探すのはできないけど)翻訳だけならできると言っていた人がいたので、翻訳して欲しいものリストがあるといいのでは？

フィードバックを受けて https://github.com/rurema/doctree/issues と https://github.com/rurema/bitclust/issues に issue を登録していくことにしました。

発表の中や、会場で話をするのを忘れていたのは、ゆるく相談する場として[Gitter](https://gitter.im/)を用意したり、[commit-email.info - Commit Email as a Service](http://www.commit-email.info/)で差分付きのコミットメールを送ってもらうようにしたりするのはどうかということでした。

RubyKaigiで発表することで新しい人にるりまを知ってもらうことができたり、普段会えない人に話を聞くことができたりして、とても有意義な時間を過ごすことができました。
