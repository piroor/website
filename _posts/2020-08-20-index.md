---
tags: []
title: D-Feetを使ってDBusインターフェースの変更に追従するには
---
### はじめに

ソフトウェアをメンテナンスしていると、バージョンアップにともないAPI等が変更になることがあります。
今回は、アプリケーションが対応しているDBusのインターフェースの変更に追従するやりかたを紹介します。
<!--more-->


### DBusのインターフェースの変更に気づいたきっかけ

問題に気づいたきっかけは、[#955899 growl-for-linux: Depends on deprecated dbus-glib](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=955899)というバグ報告でした。

バグ報告で言及されている[growl-for-linux](https://github.com/mattn/growl-for-linux)は、Growl Notification Transport Protocolを使う通知アプリケーションです。
アプリケーションで使っているライブラリーであるdbus-glibが非推奨になっているというのが報告されていました。

growl-for-linuxは[Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox)のプラグインがあり、音楽を再生するときにgrowl-for-linux経由で通知できます。
バグを修正するためにdbus-glibからGDBusへ移行する際に、このプラグインが動作しなくなっていることがわかりました。
その原因がDBusのインターフェースの変更だったのです。

### D-Feetを使って問題を特定する

D-FeetはDBusのデバッガでシステムバスやセッションバスの情報を表示できます。

動作しなくなっていたプラグインは次の条件を前提としていました。

[参考：rhythmbox.c](https://github.com/mattn/growl-for-linux/blob/master/subscribe/rhythmbox/rhythmbox.c#L182)

  * セッションバスのバス名は org.gnome.Rhythmbox

  * Rhythmboxのオブジェクトパスは /org/gnome/Rhythmbox/Shell と /org/gnome/Rhythmbox/Player

  * オブジェクトパス /org/gnome/Rhythmbox/Player の getPlayingUri で再生中の曲のパスを取得する

  * オブジェクトパス /org/gnome/Rhythmbox/Shell の getSongProperties で再生中の曲のプロパティを取得する

一方、D-FeetでRhythmbox（バージョンは3.4.4）をみてみると次のことがわかりました。

![d-feetでセッションバスを確認しているスクリーンショット]({{ "/images/blog/20200820_0.png" | relative_url }} "d-feetでセッションバスを確認しているスクリーンショット")

  * セッションバスのバス名は org.gnome.Rhythmbox3 （Rhythmbox3に変わっている）

  * Rhythmboxのオブジェクトパスは /org/mpris/MediaPlayer2 [^0]

  * オブジェクトパス /org/mpris/MediaPlayer2 の PlaybackStatus プロパティで再生状態を取得する

  * オブジェクトパス /org/mpris/MediaPlayer2 の Metadata プロパティで曲のメタ情報を取得する [^1]

したがって、上記のとおりD-Busのインターフェースに対してアクセスしている箇所を移植することで動作するようになりました。

修正内容は [Use GDBus instead of deprecated dbus-glib](https://github.com/mattn/growl-for-linux/pull/84) としてupstreamにもフィードバックしています。

### まとめ

今回は、アプリケーションが対応しているDBusのインターフェースの変更に追従するやりかたを紹介しました。[^2]
もし、DBusのインターフェースの変更に対応することになったら参考にしてみてください。

[^0]: 従来の /org/gnome/Rhythmbox/Shell と /org/gnome/Rhythmbox/Player に相当するのは /org/gnome/Rhythmbox3 ではなく /org/mpris/MediaPlayer2

[^1]: xesam:title や xesam:artist、 xesam:album などが取得できる

[^2]: dbus-glibからGDBusへの移行については別の機会があれば説明することにします。
