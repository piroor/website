---
tags: []
title: RZ/G1向けのfluent-bitのYoctoレシピを作成しました
---
### はじめに

[2017年7月6日の記事]({% post_url 2017-07-06-index %})で紹介した通り、クリアコードは組み込みLinux向けにMozilla Firefoxを移植するプロジェクト[Gecko Embedded](http://gecko-embedded.org/)を[WebDINO Japan（旧Mozilla Japan）](https://www.webdino.org/)様と共同で立ち上げ、開発を進めております。[Yocto](https://www.yoctoproject.org/)を使用してFirefoxをビルドしたりハードウェアクセラレーションを有効化する際のノウハウを蓄積して公開することで、同じ問題に悩む開発者の助けになることを目指しています。
<!--more-->


この記事では、Gecko Embedded本体ではなく周辺のミドルウェアの[Fluent Bit](http://fluentbit.io/)を移植した話を書きます。
Fluent Bitは、センサなどの組み込み機器向けのデータコレクタで、Fluentdより軽量です。

### 現在のステータス

#### ターゲットボード

1月時点では、fluent-bitのビルド及び動作は以下のボードで確認しています。

  * [Renesas RZ/G1E Starter Kit](https://elinux.org/RZ-G/Boards/SK-RZG1E)

  * [iWave RainboW-G20D Q7](https://github.com/webdino/meta-browser/wiki/Build-RainboW-G20D-Q7-Yocto-2.0)

  * [R-Car Gen3](https://github.com/webdino/meta-browser/wiki/Build-RCar-Gen3-Yocto2.1)

  * Raspberry Pi 3 Model B

### ビルド方法

レシピはGitHubにて公開しています。https://github.com/clear-code/meta-fluent

Yoctoに組み込むには、meta-fluentを `git clone` したのち、(bitbakeのビルドディレクトリ)/conf/local.confへ

```
IMAGE_INSTALL_append = " fluent-bit "
```


(bitbakeのビルドディレクトリ)/conf/bblayers.confへ

```
BBLAYEYS += " ${TOPDIR}/../meta-fluent "
```


をそれぞれ追加し、bitbakeを実行します。

また、fluent-bit本体にもYoctoレシピがありましたがx86_64向けのビルドしかされていないようだったため、armv7hまたはaarch64向けの[クロスビルドが通らないことをIssue](https://github.com/fluent/fluent-bit/issues/219)にしました。

このIssueは[このpull request](https://github.com/fluent/fluent-bit/pull/243)によって解決済みです。

### 動作確認

fluent-bitを上記の設定ででビルドした場合、`fluent-bit` コマンドが起動イメージにインストールされます。

```bash
$ fluent-bit -i dummy -o stdout
Fluent-Bit v0.12.10
Copyright (C) Treasure Data

[2017/12/27 14:04:42] [ info] [engine] started
```


となれば動作確認は完了です。
設定ファイルの書き方やどのようなプラグインがあるかは[Fluent Bitの公式サイトのドキュメント](http://fluentbit.io/documentation/)を参照してください。

### まとめ

fluent-bitをarmv7hとaarch64がターゲットのYoctoレシピに載せ、RZ/Gシリーズのボードに載せた話を紹介しました。
ARMなどの組み込み向けボードはPCに比べてリソースの制約がありますが、fluent-bitではFluentdよりも軽量のため、リソース不足により動作が遅くなることは少ないでしょう。
