---
tags:
- milter-manager
title: milterでtaRgrey
---
[milter-greylistでtaRgrey]({% post_url 2009-09-07-index %})でmilter-greylistにtarpitの機能が取り込まれたことを紹介しました。当時はまだ取り込まれただけでリリースはされていなかったのですが、先日、開発版として[milter-greylist 4.3.4がリリース](http://tech.groups.yahoo.com/group/milter-greylist/message/5299)されました。
<!--more-->


興味のある方が簡単に試せるようにDebian GNU/Linux lenny i386用とUbuntu 8.04 LTS Hardy Heron i386用のパッケージを作成しました。

  * [Debian用](http://www.clear-code.com/~kou/debian/milter-greylist_4.3.4_i386.deb)
  * [Ubuntu用](http://www.clear-code.com/~kou/ubuntu/milter-greylist_4.3.4_i386.deb)

それでは、milterだけで[taRgrey](http://k2net.hakuba.jp/targrey/)を実現する方法を説明します。milter-greylist単体で実現すると設定が煩雑になるので、milter managerとmilter-greylistを連携させます。

### 設定

[milter managerのUbuntu用インストールドキュメント](http://milter-manager.sourceforge.net/reference/ja/install-to-ubuntu.html)通りにインストールされているものとします。Debian用のインストールドキュメントはまだHTMLで公開していないので、[下書き](https://milter-manager.svn.sourceforge.net/svnroot/milter-manager/milter-manager/trunk/doc/install-to-debian.rd.ja)を参考にしてください[^0]。

ただし、milter managerは1.3.1（開発版）以降を利用してください。1.3.1のパッケージは以下にあります。

Ubuntu 8.04 LTS Hardy Heron用apt-line:

{% raw %}
```
deb http://milter-manager.sourceforge.net/ubuntu/ hardy universe
deb-src http://milter-manager.sourceforge.net/ubuntu/ hardy universe
```
{% endraw %}

Debian GNU/Linux lenny用apt-line:

{% raw %}
```
deb http://milter-manager.sourceforge.net/debian/ lenny main
deb-src http://milter-manager.sourceforge.net/debian/ lenny main
```
{% endraw %}

設定は以下のように変更します。

  * Postfix: milterとのやりとりのタイムアウト時間を150秒にする
  * milter-greylist: 125秒のtarpitに耐えた接続をホワイトリストに入れる

milter managerの設定を変更することがない点に注意してください。milter managerは環境にあわせて適切なデフォルト値を使うので、システム管理の手間を減少することができます。

Postfixは/etc/postfix/main.cfに以下の行を追加してください。

/etc/postfix/main.cf:

{% raw %}
```
milter_command_timeout = 150
```
{% endraw %}

設定を反映するために再起動します。

{% raw %}
```
% sudo /etc/init.d/postfix restart
```
{% endraw %}

milter-greylistは/etc/milter-greylist/greylist.confを以下のように変更します。

変更前:

{% raw %}
```
racl greylist default
```
{% endraw %}

変更後:

{% raw %}
```
racl whitelist tarpit 125s
racl greylist default
```
{% endraw %}

125秒のtarpitに耐えたらホワイトリスト、耐えられなかったらgreylistで救済という動作が自然に書けています。

設定を反映するために再起動します。

{% raw %}
```
% sudo /etc/init.d/milter-greylist restart
```
{% endraw %}

最後に、milter-managerに設定の変更を検出させるために再起動します。設定の変更は必要ありません。

{% raw %}
```
% sudo /etc/init.d/milter-manager restart
```
{% endraw %}

milter-managerの設定内容を確認すると、milter-greylistのタイムアウト時間が135秒となっているのがわかります。

{% raw %}
```
% sudo /usr/sbin/milter-manager -u milter-manager --show-config
...
define_milter("milter-greylist") do |milter|
  ...
  milter.writing_timeout = 135.0
  milter.reading_timeout = 135.0
  ...
end
...
```
{% endraw %}

135秒は「デフォルトのタイムアウト時間10秒」と「milter-greylistのtarpit時間125秒」を足し合わせた時間です。これはmilter-managerが自動で検出します。

これで、milterによるtaRgrey環境が完成です。

### まとめ

tarpit対応のmilter-greylistがリリース（ただし開発版）されたので、milter managerとmilter-greylistを組み合わせてtaRgreyを実現する方法を紹介しました。

milter managerもmilter-greylistもどちらも開発版を使用しているので、まだ本番環境への投入は早いですが、Debianパッケージがあるので比較的簡単に試せるようになっています。検証など興味のある方は試してみてはいかがでしょうか。問題があった場合は、それを報告し、安定版リリース時には問題が解決されているようにしたいですね。

[^0]: 次のリリースで公開する予定
