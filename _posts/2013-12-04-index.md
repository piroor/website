---
tags:
- presentation
title: 全文検索エンジンGroongaを囲む夕べ 4での発表資料
---
2013年11月29日（いい肉の日）に[全文検索エンジンGroongaを囲む夕べ 4](http://atnd.org/events/43461)が開催されました。会場の提供から運営、懇親会の飲み物や肉メインのおいしい食べ物の提供まで、[DeNA](http://dena.com/)さんが全面的にバックアップしてくれました。とても感謝しています。
<!--more-->


平日の17時と早い時間からのスタートにも関わらず、130名くらいも参加[^0]してくれました。4時間という長い時間のイベントだったので、みなさんだいぶ疲れたのではないでしょうか。DeNAさんが用意してくれた懇親会の飲み物と食べ物をよりおいしく感じたかもしれませんね。

今年も[Ustream配信](http://www.ustream.tv/channel/groonga-night)は[すずきさん](https://twitter.com/suzuki)が協力してくれました。ありがとうございます。遠方で参加できない人などの役に立っていたようでよかったです。録画してあり、今でも観れるので当日参加できなかった方はご利用ください。

イベントの内容はTwitter上にも流していました。[Togetterまとめ](http://togetter.com/li/598598)で当日のTwitter上での情報を確認できるのでこちらも合わせてご利用ください。Twitterでの情報提供は[@hyoshihara04さん](https://twitter.com/hyoshihara04)が手伝ってくれました。

軽妙な語り口でスムーズな進行をしてくれたのは今年も[坂井さん](https://twitter.com/sakaik)です。坂井さんの司会はUstreamの録画でお楽しみください。

さて、今回のイベントではクリアコードのメンバー3人の発表がありました。参加できなかった人でもわかるように、それぞれの発表について説明します。発表内容は次のとおりです。

  * Groonga族2013：Groonga関連プロダクトの概要と最新情報の紹介
  * Groongaビジネスパートナー募集：Groongaでのビジネスを一緒に盛り上げようというお誘い
  * Groongaを支える取り組みの紹介：Groongaをもっと便利に使えるようにするための取り組みを紹介

### Groonga族2013

まず、Groonga関連プロダクトの概要と最新情報の紹介した「Groonga族2013」について説明します。発表資料は次の場所にあります。どちらも同じものです。

  * [SlideShare](https://slideshare.net/kou/groonga-family2013)
  * [Rabbit Slide Show](https://slide.rabbit-shocker.org/authors/kou/groonga-night-4/)

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/groonga-night-4/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen>
  </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/groonga-night-4/" title="Groonga族2013">Groonga族2013</a>
  </div>
</div>


#### 概要

この発表では次の3つについて話しました。

  * Groonga関連プロダクトの公式表記が先頭大文字で「Groonga」や「Mroonga」になったこと。
  * Groongaそれ自体と他の関連プロダクトの関係。
  * Groonga関連プロダクトのここ1年での改良点を紹介。

##### 公式表記の変更について

公式表記について補足します。これまでは、主に「groonga」（すべて小文字）、たまに[^1]「Groonga」（先頭大文字）という表記を使っていましたが、今後は「Groonga」（先頭大文字）という表記のみを使っていきます。

このように表記を統一する理由は、世界進出を視野にいれているからです。これまで英語圏の人とやりとりしていた経験から、英語圏では先頭大文字表記の方がなじみがあることがわかりました。今後、世界でGroongaについて話題にするときによりなじみやすくするために先頭大文字で統一することにしました。今後は、ユーザーのみなさんもGroongaの世界進出を後押しするためにこの表記を使ってくださいとお願いしました。

表記の変更については[Groonga公式サイトでのアナウンス](http://groonga.org/ja/blog/2013/10/30/use-capitalized-notation.html)も参考にしてください。

##### プロダクトの関係について

Groongaそれ自体と他の関連プロダクトの関係については、図入りで説明しました。図があったほうがわかりやすいので、発表資料を参照してください。

##### ここ1年の改良点について

現在、Groonga関連のイベントは年に1回いい肉の日に開催されている「全文検索エンジンGroongaを囲む夕べ」のみです。そのため、前回のイベントからこの1年でGroonga関連プロダクトが成長しているかの概要を紹介しました。紹介した内容は次のとおりです。

  * Groonga
    * Fedora公式パッケージになった。CentOS 7くらいではCentOS 7にも入るだろう。
    * mrubyの組み込みを始めた。ただし、まだ実験的な機能。
  * Mroonga
    * Windowsサポートを始めた。
    * MariaDBバンドル作業が進行中。
  * Rroonga
    * 最新のGroongaのフル機能を使える。（継続）
    * [64bit版Windows用バイナリ]({% post_url 2013-10-16-index %})の提供を始めた。

もう少し粒度を小さくするともっとたくさんありますが、時間の関係でこのくらいにとどめました。詳細は[Groongaの更新履歴ページ](http://groonga.org/ja/docs/news.html)や[Mroongaの更新履歴ページ](http://mroonga.org/ja/docs/news.html)を確認してください。

##### まとめ

「Groonga族2013」では次の3点について話しました。

  * Groonga関連プロダクトの公式表記が先頭大文字で「Groonga」や「Mroonga」になったこと。
  * Groongaそれ自体と他の関連プロダクトの関係。
  * Groonga関連プロダクトのここ1年での改良点を紹介。

### Groongaビジネスパートナー募集

次に、Groongaでのビジネスを一緒に盛り上げようという「Groongaでビジネスしませんか？ -Groongaビジネスパートナー構想のご紹介」発表について説明します。

<div class="slideshare">
  <iframe src="http://www.slideshare.net/slideshow/embed_code/28796968" width="427" height="356" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC;border-width:1px 1px 0;margin-bottom:5px" allowfullscreen>
  </iframe>
  <div style="margin-bottom:5px">
    <strong><a href="https://www.slideshare.net/ShinichiroMinami/201311groonga-night-28796968" title="2013.11.29 Groongaでビジネスしませんか？" target="_blank">2013.11.29 Groongaでビジネスしませんか？</a></strong> from <strong><a href="http://www.slideshare.net/ShinichiroMinami" target="_blank">Shinichiro Minami</a></strong>
  </div>
</div>


#### 概要

Groongaの普及にともなって、Groongaを使ってみたい、Groongaをとことん使いこなしたいという企業からの問い合わせが増えてきました。また、Groongaを使ったサービスのアイデアももらうようになりました。しかし、現在の体制ではこのような要望に十分に応えられていません。Groongaの更なる普及には、Groongaを使ったシステム開発やGroongaのサポートに対応する体制を強化する必要があります。

その方法としてGroongaに関連したサービスを提供する企業が共同でPR活動を行ったり、一緒にサービスを開発する体制を構築する予定です。サポートを提供する企業が多く存在することによってユーザの選択肢が増加し、さらにそれぞれの企業が連携してより大きなサービスを提供することが可能となることを期待しています。

これはクリアコードでのいくつかの経験を参考にしています。ひとつは、Mozilla Japanの[サポートパートナー](http://www.mozilla.jp/business/support/)に参加して、Mozilla Firefox、Thunderbirdの企業導入を行ってきた経験です。サポートパートナーが連携して1つの案件を獲得することもありますし、サポートパートナーが1つの案件で競合することもあります。いずれも複数のサポート企業が存在することによってできることです。Groongaに関しても複数のサポート企業がユーザから見やすいかたちで存在することが重要です。

もうひとつはクリアコードで案件を請けられない時に信頼できる企業を紹介してきた経験です。MozillaのサポートやRubyによる開発などクリアコードに仕事の依頼をもらっても、リソースがないことが理由で請けられないケースがあります。このような時に、せっかく問い合わせしてもらったので、なにかお役に立てないか他の企業を紹介することがあります。具体的には一緒に仕事をした経験があり、また一緒に仕事をしたいと思う企業を優先して紹介します。そして、紹介したことによってビジネスが生まれれば、問い合わせした企業、クリアコード、紹介した企業の3社の間に信頼関係ができます。Groongaをキーワードにこのような信頼の連鎖を作っていきたい考えです。

Groongaビジネスパートナー構想を検討するにあたって、クリアコードの経験だけでなく、参加を検討される方々の経験やアイデアを参考にしていきます。そのため、12月からGroongaでのビジネスに興味のある方からお話を伺っていきます。興味のある方はぜひminami@clear-code.comまでご連絡ください。

#### 発表者の感想

素敵な会場での発表で、非常に緊張してしまい、伝えきれなかったところが多々ありました。しかし、発表の後、何人かから声をかけていただき、Groongaビジネスパートナー構想への参加を検討していただけることになりました。うれしい限りです。来年2月9日の発足に向けてしっかり進めて行きたいと思います。ありがとうございました。

### Groongaを支える取り組みの紹介

最後に、Groongaをもっと便利に使えるようにするための取り組みを紹介をした「Groongaを支える取り組みの紹介」について説明します。

<div class="slideshare">
  <iframe src="http://www.slideshare.net/slideshow/embed_code/28866174?rel=0" width="427" height="356" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC;border-width:1px 1px 0;margin-bottom:5px" allowfullscreen>
  </iframe>
  <div style="margin-bottom:5px">
    <strong><a href="https://www.slideshare.net/kenhys/groonga-night-4-28866174" title="Groongaを支える取り組み" target="_blank">Groongaを支える取り組み</a></strong> from <strong><a href="http://www.slideshare.net/kenhys" target="_blank">kenhys</a></strong>
  </div>
</div>


#### 概要

この発表では次のことについて話しました。

  * この1年でやってきたGroongaの利用事例やノウハウを共有する取り組み
  * Groongaを使っていて困った時の助けの求め方
  * Groongaプロジェクトへの貢献のお誘い

##### 利用事例やノウハウを共有する取り組み

利用事例やノウハウを共有するために、今年は次の2つに取り組みました。

  * [週刊Groonga](http://qiita.com/groonga)
  * [隔週連載Groonga](http://gihyo.jp/dev/clip/01/groonga)

週刊Groongaは毎週木曜日にQiitaにGroonga、Mroonga、Rroonga関連のちょっとした便利情報をまとめているものです。Mroonga関連の記事が人気です。

記事を公開したら[@groonga](https://twitter.com/groonga)で告知しています。フォローして最新記事を確認してみてください。

隔週連載Groongaはgihyo.jpさんで4月から9月まで約半年（ほぼ）隔週で続けた連載です。Groonga関連プロダクトの事例紹介とその事例紹介に関連する技術情報を提供しています。今でもすべての記事を読めるのでチェックしてみてください。なお、今回のイベントレポートもgihyo.jpさんに載る予定です。楽しみですね。

##### 困った時の助けの求め方

Groongaはこつこつと開発を続けてだいぶよいソフトウェアになってきました。しかし、困ることもあるでしょう。そんなときは、Twitterにつぶやいたり[groonga-devメーリングリスト](http://lists.sourceforge.jp/mailman/listinfo/groonga-dev)で質問したりしてみてください。

Groonga開発者は「Groonga」や「Mroonga」などのキーワードで困っている人がいないかをチェックしています。Twitterでつぶやくと開発者の人から助けてもらえるかもしれません。

Twitterでは確実に返事をもらえるわけではありませんが、メーリングリストでは確実に返事をもらえます。バグ報告や機能要望だけではなく、どうやったらうまく使えるか、といった質問も受けつけています。

メーリングリストというと敷居が高いかもしれませんが、groonga-devメーリングリストは[技術系メーリングリストで質問するときのパターン・ランゲージ](http://www.hyuki.com/writing/techask.html)や[真・技術系メーリングリスト FAQ](http://www.geocities.co.jp/SiliconValley/5656/)から想起されるような殺伐とした場所ではありません。[過去のメール](http://sourceforge.jp/projects/groonga/lists/archive/dev/)を見ればわかるはずです。「[メーリングリストに投稿したらすぐに直してもらえた](https://twitter.com/KanakoNakai/status/406340303990177792)」という利用者の声もあります。お気軽にご利用ください。

##### Groongaプロジェクトへの貢献のお誘い

Groongaをよりよくしていくために、ユーザーのみなさんの協力を待っています。次のようなことにピンときたら、ぜひ協力してください。

  * [週刊Groongaの翻訳](http://groonga.org/ja/blog/2013/07/22/qiita-translation.html)
    * Groongaの世界進出を推進するために、英語圏へもGroongaの有用な情報を届けたい。週刊Groongaという日本語のコンテンツがあるので、それを翻訳して、Groongaの世界進出を後押しして欲しい。
    * 難易度：元の文章はあるので英語の読み書きができれば簡単
  * [コマンドのドキュメントのフォーマットを統一](http://groonga.org/ja/blog/2013/08/12/reference-command-documentation.html)
    * 現在は、[Groongaのコマンドのドキュメント](http://groonga.org/ja/docs/reference/command.html)の表記が統一されていない。統一するとユーザーがドキュメントを読みやすくなるため、Groongaを使いやすくなる。
    * 難易度：簡単（テキストの整形だけ）
  * [「groonga」表記を「Groonga」表記へ統一](http://groonga.org/ja/blog/2013/10/30/use-capitalized-notation.html)
    * 公式表記は「Groonga」と表明しているのにドキュメント中には小文字表記が多く残っていて、公式表記の普及に悪影響がある。
    * 難易度：簡単（テキストの修正だけ）
  * [Groonga Advent Calendar 2013](http://qiita.com/advent-calendar/2013/groonga)
    * 参照できるGroonga関連情報が増えるので、Groonga普及に役立つ。
    * 難易度：Groonga関連プロダクトをすでに使っていれば簡単

Groongaに貢献したいと思っているけど何をすればいいかわからなかった、という方はぜひ参考にしてください。

### まとめ

毎年いい肉の日に開催しているGroongaイベントでの発表内容を紹介しました。

冒頭にも書いたとおり、DeNAさんには本当にお世話になりました。きっかけを作ってくれた[@sunaotさん](https://twitter.com/sunaot)と[@ryopekoさん](https://twitter.com/ryopeko)、ありがとうございました。[パーフェクトRuby]({% post_url 2013-08-19-index %})が縁でした。

[^0]: 参加登録者が約160名なのでの約8割。ATNDでイベントにしては参加率が高い方ではないでしょうか。

[^1]: 例えば英語の文章で先頭にくるときだけ。
