---
tags:
  - apache-arrow
  - ruby
  - presentation
title: '名古屋Ruby会議03：Apache ArrowのRubyバインディングをGObject Introspectionで #nagoyark03'
---
2017年2月11日に[名古屋Ruby会議03](http://regional.rubykaigi.org/nagoya03/)が[大須演芸場](http://www.osuengei.nagoya/)で開催されました。ここで[咳さんの並列処理啓蒙活動話](http://d.hatena.ne.jp/m_seki/20170212)の前座の1人として話してきました。内容は、[Apache Arrow](http://arrow.apache.org/)と[GObject Introspection](https://wiki.gnome.org/action/show/Projects/GObjectIntrospection)と[Rroonga](http://ranguba.org/ja/#about-rroonga)を活用すれば自然言語のデータ分析の一部でRubyを活用できるよ！、です。
<!--more-->


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/nagoya-rubykaigi-03/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/nagoya-rubykaigi-03/" title="Apache ArrowのRubyバインディングをGObject Introspectionで">Apache ArrowのRubyバインディングをGObject Introspectionで</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/nagoya-rubykaigi-03/)

  * [スライド（SlideShare）](https://slideshare.net/kou/nagoyarubykaigi03)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-nagoya-rubykaigi-03)

リポジトリーには今回のスライド内で使ったスクリプトも入っています。

### 内容

当初は以下にいろいろまとめていた通り、Apache Arrow・GObject Introspectionはどんな特徴でどういう仕組みでそれを実現しているかといったことも説明するつもりでした。

  * [名古屋Ruby会議03：Apache ArrowのRubyバインディング（1） #nagoyark03]({% post_url 2017-01-16-index %})

  * [名古屋Ruby会議03：Apache ArrowのRubyバインディング（2） #nagoyark03]({% post_url 2017-01-20-index %})

  * [名古屋Ruby会議03：Apache ArrowのRubyバインディング（3） #nagoyark03]({% post_url 2017-01-23-index %})

  * [名古屋Ruby会議03：Apache ArrowのRubyバインディング（4） #nagoyark03]({% post_url 2017-01-25-index %})

  * [名古屋Ruby会議03：Apache ArrowのRubyバインディング（5） #nagoyark03]({% post_url 2017-01-31-index %})

  * [名古屋Ruby会議03：Apache ArrowのRubyバインディング（6） #nagoyark03]({% post_url 2017-02-01-index %})

ただ、内容をまとめていく過程で、特徴や仕組みの説明が多いと大須演芸場で話すには勢いが足りないと判断しました。その結果、詳細をもろもろ省略したストーリーベースの内容にしました。そのため、↑の内容はほぼ使っていません。

ストーリーベースの内容にしたことで「あぁ、たしかにデータ分析の一部でRubyを使えるかも」という雰囲気は伝わりやすくなったはず（どうだったでしょうか？）ですが、詳細の説明を省略したので詳細が気になったまま終わった人もいるかもしれません。そんな人たち向けに詳細がわかる追加情報を紹介します。

### Apache ArrowとApache Parquet

まず、Apache Arrowと[Apache Parquet](https://parquet.apache.org/)の連携についてです。両者はどちらもカラムストアのデータについて扱いますが、Apache Arrowはメモリー上での扱い、Aapache Parquetはストレージ上での扱いという違いがあります。この違いのためにトレードオフのバランスが変わっています。詳細は[The future of column-oriented data processing with Arrow and Parquet](http://www.slideshare.net/julienledem/data-eng-conf-ny-nov-2016-parquet-arrow)を見てください。2016年11月にニューヨークで開催された[DataEngConf NYC](https://www.meetup.com/ja-JP/NYC-Data-Engineering/events/234756754/)でのApache Parquetの作者の発表資料です。

ざっくりとまとめると次の通りです。

  * Apache Parquetはストレージ上でのカラムストアのデータの扱いなので、次の傾向がある。

    * CPUを活用するよりもI/Oを減らすほうが重要（たとえば、データを圧縮するとI/Oは減る一方CPU負荷は増えてしまうが、I/Oを減らしたいので圧縮をがんばる）

    * シーケンシャルアクセスが多い

  * Apache Arrowはメモリー上でのカラムストアのデータの扱いなので、次の傾向がある。

    * I/Oを減らすよりもCPUを活用するほうが重要（たとえば、CPUキャッシュミスが少なくなるようなデータ配置にする）

    * シーケンシャルアクセスもあるしランダムアクセスもある

データの配置や高速化の工夫なども前述の発表資料で説明しているので興味のある人は発表資料も確認してください。

Apache ArrowとApache Parquetは連携できます。具体的に言うと、[Apache ParquetのC++実装](https://github.com/apache/parquet-cpp)にはApache Arrowのデータを読み書きできる機能があります。これを使うとApache ParquetのデータをApache Arrowのデータとして扱うことができます。

### GObject Introspection

GObject Introspectionに関する情報は次の記事を参考にしてください。

  * [名古屋Ruby会議03：Apache ArrowのRubyバインディング（4） #nagoyark03]({% post_url 2017-01-25-index %})

  * [名古屋Ruby会議03：Apache ArrowのRubyバインディング（5） #nagoyark03]({% post_url 2017-01-31-index %})

  * [名古屋Ruby会議03：Apache ArrowのRubyバインディング（6） #nagoyark03]({% post_url 2017-02-01-index %})

GObject Introspectionに対応するとバインディングを書かなくても[CライブラリーのテストをRubyで書ける](https://github.com/kou/arrow-glib/tree/master/test)ようになります。Cライブラリーの開発を捗らせるためにGObject Introspectionに対応させるというのもアリです。（これも当初は話したかったけど省略した話題です。）

GObject IntrospectionのRubyバインディングであるgobject-introspection gemについてはここで少し補足します。

話の中で、`GI.load`した後、よりRubyっぽいAPIになるように一手間かけるとグッと使いやすくなると説明しました。当日は次のようにエイリアスを使う方法だけを紹介したのですが、別の方法もあってそれを紹介することをすっかり忘れていました。

```ruby
require "gi"
Arrow = GI.load("Arrow")
class Arrow::Array
  def [](i)
    get_value(i)
  end
end
```


実は`GObjectIntrospection::Loader`には定義するメソッド名を変える機能があります。上述のケースではエイリアスを作るのではなく、最初から`get_value`を`[]`として定義するとよいです。

```ruby
class Arrow::Loader < GObjectIntrospection::Loader
  private
  def rubyish_method_name(function_info, options={})
    # 引数が1つで、メソッド名がget_valueならメソッド名を[]にする
    if function_info.n_in_args == 1 and function_info.name == "get_value"
      "[]"
    else
      super
    end
  end
end
```


このように`GObjectIntrospection::Loader`をカスタマイズするやり方には次のメリットがあります。

  * 余計なメソッドを増やさない（今回のケースでは`get_value`）

  * 新しく同じパターンのメソッドが増えてもエイリアスを追加する必要がない（たとえば、`Arrow::Array`以外に`get_value(i)`なメソッドが増えてもバインディングを変更する必要がない）

この実装は[Red Arrow](https://github.com/kou/red-arrow/)（RArrowから名前を変更、由来はRubyは赤いからというのと西武新宿線の特急列車）にあります。[`open {|io| ...}`を実現する方法](https://github.com/kou/red-arrow/blob/master/lib/arrow/block-openable-applicable.rb)も面白いので、GObject Introspectionが気になってきた方はぜひ実装も見てみてください。

### まとめ

名古屋Ruby会議03で「Apache ArrowとGObject IntrospectionとRroongaを使って自然言語のデータ分析の一部でRubyを活用する」という話をしました。（使い方を間違っていましたが）はじめて小拍子を使ったり、はじめてマクラの後に羽織（？）を脱いだりできて、楽しかったです。貴重な経験になりました。声をかけてもらってありがとうございます。名古屋のみなさん（名古屋外からの参加の方も多かったですが）に楽しんでもらえていたなら、とてもうれしいことです。

今回の話では詳細をもろもろ省略しましたが、そのあたりに興味のある方がいたらぜひお声がけください。

また、クリアコードと一緒にRubyでデータ分析できる環境を整備していきたい！という方はぜひ[お問い合わせ](/contact/?type=ruby)ください。
