---
tags:
- mozilla
title: CPUの使用率を表示するFirefoxアドオンをリリース
---
CPUの使用率をFirefoxのウインドウ右上に表示するアドオンをリリースしました。<del>ダウンロードは[こちらから](http://packages.clear-code.com/xul/extensions/system-monitor/system-monitor-0.1.xpi)どうぞ。</del>[新しいバージョン]({% post_url 2009-10-20-index %})がすでに公開されていますので、そちらを参照して下さい。
<!--more-->


インストールすると、以下のイメージのようにウインドウ右上に直近のCPU使用率がグラフで表示されます。

![CPUモニター]({{ "/images/blog/20091019_0.png" | relative_url }} "CPUモニター")

プラットフォームは、WindowsXP SP1以降、Mac OS X、Linuxをサポートしています。Linuxではlibgtop2を使用していますので、別途インストールしてください。

また、このアドオンではCPU使用率をWebアプリケーションからも使えるようにしてあります。JavaScriptで以下のように書くと、intervalTime間隔ごとにfunctionが呼び出され、aUsageにその時のCPU使用率が渡ってきます。

{% raw %}
```
system.addMonitor("cpu-usage", function(aUsage){}, intervalTime);
```
{% endraw %}

クリアコードでは、このようにWebアプリケーションから各種ハードウェアデバイスにアクセスする機能の開発を行っております。ご用命はinfo@clear-code.comまでお問い合わせください。
