---
tags:
- groonga
- presentation
title: 'Zulip & PGroonga Night - PGroonga & Zulip #zpnight'
---
[Zulip](https://zulip.org/)に[PGroonga](https://pgroonga.github.io/ja/)サポートを実装した須藤です。[PyCon JP 2017](https://pycon.jp/2017/ja/)に参加するためにZulipの開発者の1人であるGregさんが来ていたので、[日本PostgreSQLユーザ会（JPUG）](https://www.postgresql.jp/)さんに主催してもらってZulipとPGroongaのイベント「[Zulip & PGroonga Night](https://jpug.connpass.com/event/62995/)」を開催しました。
<!--more-->


なお、GregさんのPyConJP 2017でのトーク「[Clearer Code at Scale: Static Types at Zulip and Dropbox](https://pycon.jp/2017/ja/proposals/vote/153/)」（[動画](https://www.youtube.com/watch?v=vcWg0tlpnDc)）はPyConJP 2017のベストトークに選ばれました。すごい！

Zulip & PGroonga NightではPGroongaの紹介とZulipの全文検索インデックスの更新方法の紹介をしました。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/zpnight/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/zpnight/" title="MySQL・PostgreSQL上で動かす全文検索エンジン「Groonga」セミナー">MySQL・PostgreSQL上で動かす全文検索エンジン「Groonga」セミナー</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/zpnight/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/zpnight)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-zpnight)

### 背景

ZulipではPostgreSQL標準の[textsearch](https://www.postgresql.jp/document/current/html/textsearch.html)を使って全文検索を実現しています。textsearchは言語特化型のインデックスを作るため、同時に複数の言語をサポートすることができません。また、日本語を含むアジア圏の言語のサポートが不十分なため、日本語を全文検索できないという問題もあります。

そこで、[私はZulipでPGroongaを使えるようにしました](https://github.com/zulip/zulip/pull/700)。PGroongaは言語特化型のインデックスも言語非依存のインデックスも作れます。言語非依存のインデックスを作れば同時に日本語も英語もいい感じに全文検索できます。

### クリアコードがZulipを選んだ理由

私がZulipにPGroongaサポートパッチを送ったのは自分たちが必要だからです。クリアコードではチャットツールとしてZulipを使っています。その前はSkypeを使っていました。お客さんとの連絡でSkypeを使う必要があったため、その流れでなんとなく使っていました。Skypeはあまり活用していませんでした。

クリアコードはフリーソフトウェアを推進したい会社なのでフリーソフトウェアではないSkypeを使っていることをどうにかしたいと考えていました。そこで、いくつかフリーソフトウェアなチャットツールを検討しました。そのうちの1つがZulipでした。Zulipのネックは日本語全文検索できないことでした。ネックがあるので選択肢から外すという考え方もあると思いますが、私たちは、自分たちで日本語全文検索できるようにして使うことにしました。フリーソフトウェアのよいところは自分たちで改良できることだからです。

### Zulipの全文検索インデックスの更新方法

Zulipは書き込み時のレイテンシーを小さくしておくために工夫をしています。チャットアプリケーションでは書き込みがすぐに終わることはよい使い勝手に直結するからです。

書き込み時はデータをPostgreSQLに書き込むだけで、全文検索インデックスの更新は別途バックグラウンドで実行します。このためにトリガーと[`NOTIFY`](https://www.postgresql.jp/document/current/html/sql-notify.html)と[`LISTEN`](https://www.postgresql.jp/document/current/html/sql-listen.html)を使っています。

具体的な実装を簡単に紹介します。興味のある人は[Zulip](https://github.com/zulip/zulip)のコードを見てください。既存のコードから学習することができることもフリーソフトウェアのよいところです。

`zerver_message`がメッセージ（チャットの書き込み）を保存しているテーブルです。この中に全文検索対象のデータを入れるカラムを定義します。メッセージのテキストそのものとは別に定義することがポイントです。`search_tsvector`がそのカラムです。（PGroongaを使うときの実装ではなくtextsearchを使うときの実装です。）

```sql
CREATE TABLE zerver_message (
  rendered_content text,
  -- ... ↓Column for full text search
  search_tsvector tsvector
);
```


この全文検索対象のデータを入れるカラムには全文検索用のインデックスを作ります。（この例ではPGroongaのインデックスではなくtextsearchのインデックスを作っています。）

```sql
CREATE INDEX zerver_message_search_tsvector
  ON zerver_message
  USING gin (search_tsvector);
```


このメッセージ用のテーブルにトリガーを設定します。このトリガーはメッセージが追加・更新されたときに「更新ログ」テーブル（`fts_update_log`テーブル）にメッセージのIDを追加します。

```sql
-- Execute append_to_fts_update_log() on change
CREATE TRIGGER
  zerver_message_update_search_tsvector_async
  BEFORE INSERT OR UPDATE OF rendered_content
    ON zerver_message
  FOR EACH ROW
    EXECUTE PROCEDURE append_to_fts_update_log();
```


メッセージのIDを追加する関数の実装は次のようになります

```sql
-- Insert ID to fts_update_log table
CREATE FUNCTION append_to_fts_update_log()
  RETURNS trigger
  LANGUAGE plpgsql AS $$
    BEGIN
      INSERT INTO fts_update_log (message_id)
        VALUES (NEW.id);
      RETURN NEW;
    END
$$;
```


「更新ログ」テーブルの定義は次の通りです。全文検索インデックスを更新するべきメッセージのIDを入れているだけです。

```sql
-- Keep ID to be updated
CREATE TABLE fts_update_log (
  id SERIAL PRIMARY KEY,
  message_id INTEGER NOT NULL
);
```


これで後から全文検索インデックスを更新するための情報を保存する仕組みができました。通常通りメッセージテーブルを操作するだけで実現できていることがポイントです。こうすることでアプリケーション側をシンプルにしておけます。残りの処理は、後から全文検索インデックスを更新する、です。

この処理のために`NOTIFY`と`LISTEN`を使います。`NOTIFY`は`LISTEN`している接続に通知する仕組みです。`LISTEN`している接続は`NOTIFY`されるまでブロックします。`NOTIFY`と`LISTEN`を組み合わせることで、ポーリングしなくてもイベントが発生したことに気づくことができます。

今回のケースでは「更新ログが増えた」というイベントに気づきたいです。このイベントが来たら全文検索インデックスを更新したいからです。

そのために、「更新ログ」テーブルにトリガーを追加します。「更新ログ」テーブルにレコードが追加されたら`NOTIFY`するトリガーです。

```sql
-- Execute do_notify_fts_update_log() on INSERT
CREATE TRIGGER fts_update_log_notify
  AFTER INSERT ON fts_update_log
  FOR EACH STATEMENT
    EXECUTE PROCEDURE
      do_notify_fts_update_log();
```


`NOTIFY`する関数の実装は次の通りです。この関数を実行すると、`fts_update_log`というイベントを`LISTEN`している接続のブロックが解除されます。

```sql
-- NOTIFY to fts_update_log channel!
CREATE FUNCTION do_notify_fts_update_log()
  RETURNS trigger
  LANGUAGE plpgsql AS $$
    BEGIN
      NOTIFY fts_update_log;
      RETURN NEW;
    END
  $$;
```


全文検索のインデックスを更新するSQLはPythonから発行しています。全文検索のインデックスの更新処理は必要なときだけ（更新ログがあるときだけ）実行したいです。必要がないときも更新処理を実行し続けるとムダにCPUを使ってしまうからです。

必要なときだけ処理を実行するために、`LISTEN`でブロックします。ブロックが解除されたら（`NOTIFY`されたら）必ず更新ログがあるので、処理を実行します。↓には入っていませんが、処理が終わったら次の`NOTIFY`があるまでまたブロックする実装になっています。こうすることで必要なときだけ処理を実行できるためムダにCPUを使わずにすみます。

```python
cursor.execute("LISTEN ftp_update_log") # Wait
cursor.execute("SELECT id, message_id FROM fts_update_log")
ids = []
for (id, message_id) in cursor.fetchall():
  cursor.execute("UPDATE zerver_message SET search_tsvector = "
                   "to_tsvector('zulip.english_us_search', "
                               "rendered_content) "
                 "WHERE id = %s", (message_id,))
  ids.append(id)
cursor.execute("DELETE FROM fts_update_log WHERE id = ANY(%s)",
               (ids,))
```


このような複数プロセスでの待ち合わせを実現するためにRDBMSとは別の仕組みを使うことも多いでしょう。たとえば、[RedisのPub/Sub](https://redis.io/topics/pubsub)を使えるでしょう。別の仕組みを使うと運用が面倒になります。PostgreSQLには`NOTIFY`/`LISTEN`があるので、PostgreSQLを使っていて待ち合わせを実現しなければいけないときは`NOTIFY`/`LISTEN`を使うことを検討してみてください。

### まとめ

ZulipとPGroongaのイベントでZulipとPGroongaの情報を紹介しました。クリアコードはZulipを使っていて、今ではなくてはならないツールになっています。ぜひみなさんもZulipを使ってみてください。

Zulipは基本的に自分たちで運用しますが、運用を任せる選択肢もあります。Zulipの開発チームがクラウドサービスでの提供を進めているのです。オープンソースコミュニティは無料で使えるそうです。興味のある人は[zulipchat.com](https://zulipchat.com/)を確認してください。

PGroongaが気になる人は、11月3日開催の[PostgreSQL Conference Japan 2017](https://www.postgresql.jp/events/jpug-pgcon2017)に来てください。日本PostgreSQLユーザ会（JPUG）が主催のPostgreSQLのカンファレンスです。ここでPGroongaの最新情報を紹介する予定です。
