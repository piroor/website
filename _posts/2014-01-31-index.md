---
tags: []
title: 監視統合ビューアHatoholのセットアップ方法
---
### はじめに

最近、クリアコードでは[Hatohol](https://github.com/project-hatohol/hatohol)というソフトウェアの開発に参加しています。Hatoholは複数の統合監視システムの情報を一括して表示することを可能とするソフトウェアです。現在対応している統合監視システムは[Zabbix](http://www.zabbix.com/)および[Nagios](http://www.nagios.org/)です。他の監視システムに対応することも検討しています。
<!--more-->


なぜこのようなシステムが必要なのかといった背景や、Hatoholの概要については、[ミラクル・リナックス社のサイト](http://www.miraclelinux.com/online-service/labs/lab02)や、OSCでのミラクル・リナックス社の[セミナー資料](http://www.ospn.jp/osc2013-kyoto/pdf/osc2013kyoto_hatohol_lt.pdf)などが詳しいので、今回の記事では割愛します。

Hatoholプロジェクトでは広く開発者を募集していますが、Hatoholを動かす前提として、最低でも一つのZabbixあるいはNagiosを用意しておく必要があり、またHatohol自体をソースからインストールして利用するまでの手順もまだこなれていないため、開発に参加するまでのハードルはやや高いと感じています。そこで今回は、Hatoholをソースからのビルドしてインストールする方法について紹介します。

なお、CentOS 6.4用には[RPMパッケージ](https://github.com/project-hatohol/hatohol-packages)も提供しています。ソースからビルドする気力はないが、とりあえずインストールして使ってみたいという方はこちらを利用する方が簡単でしょう。RPMでのインストール方法については、別途[ドキュメント](https://github.com/project-hatohol/hatohol/blob/master/doc/install/hatohol13.12-centos6.4-ja.md)を用意していますので、そちらを参照して下さい。

### Hatoholの構成

Hatoholは大きく分けて以下の2つの部分で構成されています。

  * 監視情報収集デーモン（C++）
    * 今回の記事では「Hatoholサーバ」と呼称します
  * Web UI （[Django](https://www.djangoproject.com/)アプリケーション）
    * 今回の記事では「Hatoholクライアント」と呼称します

ソースパッケージは一つにまとめられているので両者は一緒にインストールされますが、使用のための設定はそれぞれ施す必要があります。

### 前提条件

  * OS: Ubuntu 12.04
    * Hatoholの公式なサポート対象はCent OS 6.4およびUbuntu 12.04です[^0]。今回はUbuntu 12.04に対するインストール方法を紹介します。
  * Zabbix 2.0以上あるいはNagios 3以上
    * ZabbixあるいはNagiosは別途用意する必要があります[^1]。
    * Zabbixを使用する場合は、Zabbix API経由で監視情報を取得しますので、Zabbix APIにアクセスできるアカウントが必要です。 また、Zabbixフロントエンドのパスは現在のところ/zabbixに決め打ちされています（例: http://192.168.1.10/zabbix/）
    * Nagiosを使用する場合は、[NDOUtils](http://exchange.nagios.org/directory/Addons/Database-Backends/NDOUtils/details)を用いてNagiosのデータをMySQLで管理し、そこから監視情報を取得します。

### Hatoholのインストール

Hatoholのインストール方法は、ソース内の以下のドキュメントにも記されています。

  * [server/README.md](https://github.com/project-hatohol/hatohol/blob/master/server/README.md)
  * [client/README.md](https://github.com/project-hatohol/hatohol/blob/master/client/README.md)

今回は要点のみをかいつまんで紹介します。

#### 依存パッケージのインストール

まず、Hatoholサーバのビルドに必要なパッケージをインストールします。

{% raw %}
```
$ sudo apt-get install \
    git automake g++ libtool gettext \
    libsoup2.4-dev libjson-glib-dev \
    libsqlite3-dev sqlite3 \
    libmysqlclient-dev mysql-server \
    uuid-dev
```
{% endraw %}

次に、Hatoholクライアントの実行に必要なパッケージをインストールします。Djangoの動作に必要なPythonのパッケージはpipでインストールします。

{% raw %}
```
$ sudo apt-get install python-pip python-dev
$ sudo pip install django==1.5.4 mysql-python
```
{% endraw %}

Hatoholクライアントの実行には、他に[Bootstrap](http://getbootstrap.com/)
が必要です。Bootstarpのインストールについては後述します。

#### Hatoholのソースコードの取得

HatoholのソースコードはGitHubで公開しています。以下のコマンドで、GitHubから最新のコードを取得して下さい。

{% raw %}
```
$ git clone https://github.com/project-hatohol/hatohol
```
{% endraw %}

最新のコードではビルドに失敗するなどの問題がある場合は、リリース時点のコードをチェックアウトして試して下さい。

例) リリースタグの確認およびバージョン13.12のチェックアウト

{% raw %}
```
$ git tag
13.12
$ git checkout 13.12
```
{% endraw %}

#### ビルド手順

ソースディレクトリ下で以下の手順を実行してビルド・インストールします。

{% raw %}
```
$ ./autogen.sh
$ ./configure
$ make
$ sudo make install
$ sudo /sbin/ldconfig
```
{% endraw %}

#### Bootstrapのインストール

Hatoholクライアントの実行にはBootstrapが必要ですが、Hatoholのソースには含まれていませんので別途インストールする必要があります。本記事執筆時点でのBootstrapの最新版は3.0.3ですが、Hatoholではまだ3系に追従できておりませんので、[Bootstrap 2.3.2](http://getbootstrap.com/2.3.2/)を取得して下さい[^2]。

取得後、zipアーカイブ内の「css」「img」「js」以下のファイルをそれぞれHatoholクライアントの対応するディレクトリ下にコピーします。

{% raw %}
```
$ unzip bootstrap.zip
$ sudo cp -r bootstrap/* /usr/local/libexec/hatohol/client/static/
```
{% endraw %}

上記ではインストール先である/usr/local以下にコピーしていますが、Hatoholクライアントはソースディレクトリから直接実行することもできます。開発中はこちらから実行する方が手軽でしょう。その場合はソースディレクトリ以下にコピーします。

{% raw %}
```
$ cp -r bootstrap/* /path/to/hatohol/client/static/
```
{% endraw %}

### Hatoholサーバのセットアップおよび実行

Hatoholは以下の2種類のDBを使用します。

  * MySQL: 設定保存用
  * SQLite3: 監視データのキャッシュ用

それぞれ、Hatohol用のDBおよびディレクトリを用意する必要があります。

#### 設定の準備

MySQLへのDB作成および設定の投入は、hatohol-config-db-creatorというコマンドで行うことができます。投入する設定は、テキストファイルで記述します。ソースツリー内の以下のパスにサンプルがありますので、これをコピーして必要な箇所を修正して下さい。

  * [server/tools/hatohol-config.dat.example](https://github.com/project-hatohol/hatohol/blob/master/server/tools/hatohol-config.dat.example)

いくつか設定項目がありますが、ひとまず必要なのは、監視するZabbixあるいはNagiosの設定でしょう。以下に該当行のサンプルを示します。

{% raw %}
```
server: 0, localhost, 127.0.0.1, My machine, 80, 30, 10, Admin, zabbix
```
{% endraw %}

先頭の"server: "は設定カテゴリ名ですので、このまま記述します。以降はカンマ区切りで各設定項目を記述します。各項目の意味は、順に以下の通りです。

  * サーバ種別 (0: Zabbix, 1: Nagios)
  * ホスト名
  * IPアドレス
  * ニックネーム
  * ポート
  * ポーリング間隔（秒）
  * 接続エラー時のリトライ間隔（秒）
  * ユーザー名
  * パスワード
  * DB名（Nagiosのみ）

設定ファイルの準備ができたら、hatohol-config-db-creatorで設定を投入します。

{% raw %}
```
$ hatohol-config-db-creator hatohol-config.dat
```
{% endraw %}

#### キャッシュディレクトリの準備

監視データ（SQLite3のDBファイル）を格納するディレクトリは、デフォルトでは/tmpを使用します。任意のディレクトリに変更することもできますので、変更したい場合はあらかじめディレクトリを作成しておきます。

作成例：

{% raw %}
```
$ sudo mkdir /var/lib/hatohol
```
{% endraw %}

#### Hatoholサーバの実行

以上でHatoholサーバの最低限の準備は完了です。準備が整ったら、以下のコマンドでhatoholサーバを起動します。

{% raw %}
```
$ sudo hatohol
```
{% endraw %}

上記のように何もオプションを指定しない場合、hatoholはデーモンモードで起動します。しかし、開発中の場合はコンソールにログを表示しながら実行するのが便利です[^3]。「--foreground」オプションを付けてhatoholを起動すると、hatoholをフォアグラウンドで実行することができます。

{% raw %}
```
$ sudo hatohol --foreground
```
{% endraw %}

キャッシュディレクトリを変更したい場合は、環境変数HATOHOL_DB_DIRを与えてhatoholを起動します。

{% raw %}
```
$ sudo HATOHOL_DB_DIR=/var/lib/hatohol hatohol --foreground
```
{% endraw %}

より詳細なデバッグログを出力したい場合は、環境変数MLPL_LOGGER_LEVEL=DBGを与えます。

{% raw %}
```
$ sudo MLPL_LOGGER_LEVEL=DBG hatohol --foreground
```
{% endraw %}

#### 動作確認

HatoholサーバはRESTインターフェースを提供していますので、wgetなどで動作を確認することができます。

{% raw %}
```
$ wget http://localhost:33194/hello.html
```
{% endraw %}

ただし、Hatoholサーバ上のリソースを取得するためには認証が必要なため、単純に上記のようなアクセス方法ではエラーが返されます。

{% raw %}
```
{"apiVersion":3,"errorCode":22}
```
{% endraw %}

ひとまずHatoholサーバが動作していることは上記でも確認できているため、ここではこれ以上深入りしません。

### Hatoholクライアントのセットアップおよび実行

#### セットアップ方法

Hatoholクライアントは、自身の設定をHatoholサーバとは別にMySQLに保持します。このDBについても事前に用意しておく必要があります。

{% raw %}
```
$ mysql -u root
> CREATE DATABASE hatohol_client;
> GRANT ALL PRIVILEGES ON hatohol_client.* TO hatohol@localhost IDENTIFIED BY 'hatohol';
```
{% endraw %}

DB作成後、Hatoholクライアントディレクトリ下で以下のコマンドを実行してテーブルを作成します。

{% raw %}
```
$ cd path/to/hatohol/client
$ ./manage.py syncdb
```
{% endraw %}

#### Hatoholクライアントの実行

HatoholクライアントはApacheなどのWebサーバを使用して動作させることを想定していますが、開発中はDjangoの開発サーバを使用するのが手軽です。Hatoholクライアントディレクトリ下で以下のコマンドを実行してWebサーバを起動します。

{% raw %}
```
$ ./manage.py runserver
```
{% endraw %}

#### 動作確認

Webブラウザでhttp://localhost:8000/viewer/にアクセスし、起動していることを確認します。hatohol-config.datでユーザーの設定を特に変更していない場合、初期の管理者アカウントは以下になります。

  * ユーザー名: admin
  * パスワード: hatohol

### まとめ

Hatoholプロジェクトへの参加者が増えてくれることを期待して、Hatoholのインストールおよび実行方法について駆け足で説明しました。Hatoholは開発がはじまってまだ間もないソフトウェアですので、ソフトウェア自体の完成度の面でも、プロジェクト体制の面でも足りていない部分が多々あります。今回の記事をきっかけに、Hatoholに興味を持って協力して頂ける方が一人でも多く現れてくれることを願っています。

Hatoholプロジェクトの現状や、より詳しい情報ついては、また機会を設けて紹介する予定です。

[^0]: ちなみに、筆者の現在の開発環境はUbuntu 13.10ですので、厳密にこのバージョンでなければ動かないというわけではありません。

[^1]: 今回の記事ではこれらのセットアップ方法については扱いませんので、環境が無い場合はWeb上の他の記事などを参照して下さい。

[^2]: ただし、近い将来に3系に移行するはずです。

[^3]: デーモンモードの場合、ログはsyslogに記録されます。
